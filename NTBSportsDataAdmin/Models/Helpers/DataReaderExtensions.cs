﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataReaderExtensions.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The data reader extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System.Data;

namespace NTB.SportsDataAdmin.Application.Models.Helpers
{
    /// <summary>
    /// The data reader extensions.
    /// </summary>
    internal static class DataReaderExtensions
    {
        /// <summary>
        /// The get string or null.
        /// </summary>
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="ordinal">
        /// The ordinal.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetStringOrNull(this IDataReader reader, int ordinal)
        {
            return reader.IsDBNull(ordinal) ? null : reader.GetString(ordinal);
        }

        /// <summary>
        /// The get string or null.
        /// </summary>
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="columnName">
        /// The column name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetStringOrNull(this IDataReader reader, string columnName)
        {
            // Do we have tofind out which columnnumber the column has?
            int columnIndex = reader.GetOrdinal(columnName);
            return reader.IsDBNull(columnIndex) ? null : reader.GetString(columnIndex);
        }
    }
}