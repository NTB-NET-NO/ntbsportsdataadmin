// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MyUrlHelper.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Web.Mvc;

namespace NTB.SportsDataAdmin.Application.Models.Helpers
{
    /// <summary>
    /// The my url helper.
    /// </summary>
    public static class MyUrlHelper
    {
        /// <summary>
        /// The current action.
        /// </summary>
        /// <param name="urlHelper">
        /// The url helper.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string CurrentAction(this UrlHelper urlHelper)
        {
            var routeValueDictionary = urlHelper.RequestContext.RouteData.Values;

            // in case using virtual dirctory 
            var rootUrl = urlHelper.Content("~/");
            return string.Format(
                "{0}{1}/{2}/", rootUrl, routeValueDictionary["controller"], routeValueDictionary["action"]);
        }
    }
}