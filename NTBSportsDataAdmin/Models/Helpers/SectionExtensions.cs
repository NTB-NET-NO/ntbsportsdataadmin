﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SectionExtensions.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The section extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System;
using System.Web.WebPages;

namespace NTB.SportsDataAdmin.Application.Models.Helpers
{
    /// <summary>
    /// The section extensions.
    /// </summary>
    public static class SectionExtensions
    {
        /// <summary>
        /// The _o.
        /// </summary>
        private static readonly object _o = new object();

        /// <summary>
        /// The render section.
        /// </summary>
        /// <param name="page">
        /// The page.
        /// </param>
        /// <param name="sectionName">
        /// The section name.
        /// </param>
        /// <param name="defaultContent">
        /// The default content.
        /// </param>
        /// <returns>
        /// The <see cref="HelperResult"/>.
        /// </returns>
        public static HelperResult RenderSection(
            this WebPageBase page, string sectionName, Func<object, HelperResult> defaultContent)
        {
            if (page.IsSectionDefined(sectionName))
            {
                return page.RenderSection(sectionName);
            }
            else
            {
                return defaultContent(_o);
            }
        }

        /// <summary>
        /// The redefine section.
        /// </summary>
        /// <param name="page">
        /// The page.
        /// </param>
        /// <param name="sectionName">
        /// The section name.
        /// </param>
        /// <returns>
        /// The <see cref="HelperResult"/>.
        /// </returns>
        public static HelperResult RedefineSection(this WebPageBase page, string sectionName)
        {
            return RedefineSection(page, sectionName, defaultContent: null);
        }

        /// <summary>
        /// The redefine section.
        /// </summary>
        /// <param name="page">
        /// The page.
        /// </param>
        /// <param name="sectionName">
        /// The section name.
        /// </param>
        /// <param name="defaultContent">
        /// The default content.
        /// </param>
        /// <returns>
        /// The <see cref="HelperResult"/>.
        /// </returns>
        public static HelperResult RedefineSection(
            this WebPageBase page, string sectionName, Func<object, HelperResult> defaultContent)
        {
            if (page.IsSectionDefined(sectionName))
            {
                page.DefineSection(sectionName, () => page.Write(page.RenderSection(sectionName)));
            }
            else if (defaultContent != null)
            {
                page.DefineSection(sectionName, () => page.Write(defaultContent(_o)));
            }

            return new HelperResult(_ => { });
        }
    }
}