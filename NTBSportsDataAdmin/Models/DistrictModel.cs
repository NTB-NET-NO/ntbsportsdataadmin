﻿using System.Collections.Generic;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Facade.NFF.TournamentAccess;

namespace NTB.SportsDataAdmin.Application.Models
{
    public class DistrictModel
    {
        /// <summary>
        ///     Method to get the districts used.
        ///     If not found in the database, then we will use the services
        /// </summary>
        /// <returns></returns>
        public List<District> GetDistricts()
        {
            DistrictRepository repository = new DistrictRepository();
            List<District> districts = new List<District>(repository.GetAll());

            if (districts.Count == 0)
            {
                districts.Clear();

                TournamentFacade facade = new TournamentFacade();
                
                districts.AddRange(facade.GetDistricts());
            }

            return districts;
        }
    }
}