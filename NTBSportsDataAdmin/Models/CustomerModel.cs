﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerModel.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The customer model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.WebPages;
using System.Xml;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Facade.NFF.TournamentAccess;
using NTB.SportsDataAdmin.Facade.NIF.SportAccess;

namespace NTB.SportsDataAdmin.Application.Models
{
    /// <summary>
    /// The customer model.
    /// </summary>
    public class CustomerModel
    {
        /// <summary>
        /// The get customer.
        /// </summary>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <returns>
        /// The <see cref="NTB.SportsDataAdmin.Domain.Classes.Customer"/> Customer object.
        /// </returns>
        public static Customer GetCustomer(int customerId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("[SportsData_Customer_Get]", sqlConnection)
                                            {
                                                CommandType = CommandType.StoredProcedure
                                            };

                sqlCommand.Parameters.Add(new SqlParameter("CustomerId", customerId));

                sqlConnection.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                
                if (!sqlDataReader.HasRows)
                {
                    return null;
                }

                Customer customer = new Customer();
                while (sqlDataReader.Read())
                {
                    customer.CustomerId = Convert.ToInt32(sqlDataReader["CustomerId"]);
                    customer.Name = sqlDataReader["CustomerName"].ToString();
                    if (sqlDataReader["TtCustomerId"] != DBNull.Value)
                    {
                        customer.TtCustomerId = Convert.ToInt32(sqlDataReader["TtCustomerId"]);
                    }
                }

                return customer;
            }
        }
        
        /// <summary>
        /// The get number of customer jobs.
        /// </summary>
        /// <returns>
        /// The <see cref="NTB.SportsDataAdmin.Domain.Classes.NumberOfCustomers"/>.
        /// </returns>
        public NumberOfCustomers GetNumberOfCustomerJobs()
        {
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("SportsData_GetNumberOfCustomers", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlConnection.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return null;
                }

                while (sqlDataReader.Read())
                {
                    NumberOfCustomers numberOfCustomers = new NumberOfCustomers
                        {
                            Activated = Convert.ToInt32(sqlDataReader["ActivatedCustomers"]), 
                            DeActivated = Convert.ToInt32(sqlDataReader["UnActivatedCustomers"]), 
                            NumberOfTournaments = Convert.ToInt32(sqlDataReader["NumberOfTournaments"])
                        };

                    return numberOfCustomers;
                }

                return null;
            }
        }

        /// <summary>
        /// The get customer tournaments.
        /// </summary>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetCustomerTournaments(int customerId)
        {
            try
            {
                // We are first to get the jobs for the customer
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand("SportsData_GetCustomerJobs", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    sqlConnection.Open();

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }
                    
                    List<CustomerJob> customerJobses = new List<CustomerJob>();
                    while (sqlDataReader.Read())
                    {
                        CustomerJob customerJob = new CustomerJob
                            {
                                CustomerId = customerId,
                                JobId = Convert.ToInt32(sqlDataReader["JobId"]),
                                SportId = Convert.ToInt32(sqlDataReader["SportsId"])
                            };

                        customerJobses.Add(customerJob);
                    }

                    // Creating the Tournament And Id object list
                    List<Tournament> jobTournaments = new List<Tournament>();

                    // Then we are to get the tournament and numbers based on the job id that we got in the previous query
                    foreach (CustomerJob customerJobs in customerJobses)
                    {
                        // Now we have the jobId, and so we can call the command
                        sqlCommand = new SqlCommand("SportsData_GetCustomerTournaments", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };
                        sqlCommand.Parameters.Add(new SqlParameter("@JobId", customerJobs.JobId));

                        sqlConnection.Open();

                        sqlDataReader = sqlCommand.ExecuteReader();

                        if (!sqlDataReader.HasRows)
                        {
                            return null;
                        }

                        while (sqlDataReader.Read())
                        {
                            Tournament jobTournament = new Tournament
                                {
                                    TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]),
                                    TournamentNumber = sqlDataReader["TournamentNumber"].ToString(),
                                    TournamentName = sqlDataReader["TournamentName"].ToString()
                                };

                            jobTournaments.Add(jobTournament);
                        }
                    }

                    // returning the list of tournament Ids and numbers
                    return jobTournaments;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// The get customer foreign id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/> Customer Foreign Id.
        /// </returns>
        public int GetCustomerForeignId(int id)
        {
            try
            {
                // We are first to get the jobs for the customer
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand("SportsData_GetCustomerForeignId", sqlConnection)
                            {
                                CommandType = CommandType.StoredProcedure
                            };

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", id));

                    sqlConnection.Open();

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return 0;
                    }

                    int customerForeignId = 0;
                    while (sqlDataReader.Read())
                    {
                        customerForeignId = Convert.ToInt32(sqlDataReader["CustomerForeignId"]);
                    }

                    return customerForeignId;
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// The get tournaments for all customers.
        /// </summary>
        /// <param name="startDate">
        /// The start Date.
        /// </param>
        /// <param name="endDate">
        /// The end Date.
        /// </param>
        public void GetTournamentsForAllCustomers(string startDate, string endDate)
        {
            // This one has been created specifically for soccer. Needs more work.
            var facade = new TournamentFacade();
            List<Season> seasons = facade.GetSeasons();

            Dictionary<Tournament, List<Customer>> customerTournaments = new Dictionary<Tournament, List<Customer>>();

            DateTime dtStartDate = DateTime.Parse(startDate);
            DateTime dtEndDate = DateTime.Parse(endDate);
            if (seasons.Any())
            {
                var seasonId = (from s in seasons
                                  where dtStartDate.Date >= s.SeasonStartDate.Value.Date
                                  &&
                                        dtEndDate.Date <= s.SeasonEndDate.Value.Date
                                    &&
                                        s.SeasonEndDate.Value.Year == dtEndDate.Year
                                  select s.SeasonId).SingleOrDefault();

                
                if (seasonId > 0)
                {



                    // Now we need to get seasons for the other sports


                    //int seasonId = (from s in seasons
                    //                where s.SeasonStartDate == Convert.ToDateTime(startDate) &&
                    //                      s.SeasonEndDate == Convert.ToDateTime(endDate)
                    //                select s.SeasonId).Single();

                    var tournamentRepository = new TournamentRepository();

                    var tournaments = tournamentRepository.GetTournamentsBySeasonId(seasonId);

                    var customerRepository = new CustomerRepository();
                    foreach (Tournament tournament in tournaments)
                    {
                        var customers = customerRepository.GetCustomersByTournamentId(tournament.TournamentId);
                        tournament.SportId = 16;

                        if (!customers.Any()) continue;
                        customerTournaments.Add(tournament, customers);
                    }

                }
            }

            seasons = null;
            // Now we must get seasons for all the other sports and ... so on.. maybe, right?
            var sportRepository = new SportRepository();
            var sports = sportRepository.GetAll();

            // Only works for team sports...
            foreach (Sport sport in sports.Where(x => x.HasTeamResults == 1 && x.Id != 16))
            {
                // Now we shall get active season for this sport
                var seasonRepository = new SeasonRepository();
                var sportSeasons = seasonRepository.GetSeasonsBySportId(sport.Id);

                // Making sure we have an active season
                var activeSeasons = sportSeasons.Where(x => x.SeasonActive);

                // now we shall get the tournaments for this season
                

                // We must loop because of Bandy / Floorball
                foreach (var activeSeason in activeSeasons)
                {
                    var tournamentRepository = new TournamentRepository();
                    //var sportFacade = new SportFacade();
                    //sportFacade.GetTournamentsBySeasonId(activeSeason.SeasonId);
                    
                    // now that we have all the tournaments
                    var tournaments = tournamentRepository.GetTournamentsBySeasonId(activeSeason.SeasonId);
                    
                    var customerRepository = new CustomerRepository();
                    foreach (var tournament in tournaments)
                    {
                        int? disciplineId = activeSeason.FederationDiscipline;
                        
                        tournament.DisciplineId = (int) disciplineId;
                        
                        var customers = customerRepository.GetCustomersByTournamentId(tournament.TournamentId).Distinct().ToList();

                        if (!customers.Any()) continue;
                        
                        customerTournaments.Add(tournament, customers);
                    }

                }
            }

            if (customerTournaments.Count == 0)
            {
                return;
            }

            // Todo: Move this code into view
            var xmlDocument = new XmlDocument();

            var dec = xmlDocument.CreateXmlDeclaration("1.0", null, null);
            xmlDocument.AppendChild(dec);

            var xmlRoot = xmlDocument.CreateElement("SportsData");
            var xmlRootMeta = xmlDocument.CreateElement("SportsData-Meta");
            xmlRootMeta.SetAttribute("type", "Full");
            xmlRootMeta.SetAttribute("startdate", startDate);
            xmlRootMeta.SetAttribute("enddate", endDate);
            xmlRoot.AppendChild(xmlRootMeta);

            var xmlTournaments = xmlDocument.CreateElement("Tournaments");

            // Getting path from Config-file
            string outputFolder = ConfigurationManager.AppSettings["CustomerTriggerFolder"];

            // Checking that the folder exists (in case I forget when I put the system into production)
            DirectoryInfo directoryInfo = new DirectoryInfo(outputFolder);
            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }

            string todayDate = DateTime.Today.Year.ToString().PadLeft(2, '0')
                               + DateTime.Today.Month.ToString().PadLeft(2, '0')
                               + DateTime.Today.Day.ToString().PadLeft(2, '0');

            string todayTime = DateTime.Now.Hour.ToString().PadLeft(2, '0')
                               + DateTime.Now.Minute.ToString().PadLeft(2, '0')
                               + DateTime.Now.Second.ToString().PadLeft(2, '0');

            // Looping over the dictionary containing TournamentId and the customers subscribing to this tournament
            if (customerTournaments == null)
            {
                return;
            }

            foreach (KeyValuePair<Tournament, List<Customer>> kvp in customerTournaments)
            {
                /*
             * <SportsData>
    <Tournaments>
        <Tournament id="135193" sportId="4" disciplineId="123">
            <Customers>
                <Customer id="35" foreigncustomerid="57518" />
            </Customers>
        </Tournament>
    </Tournaments>
</SportsData>
             */
                int tournamentId = kvp.Key.TournamentId;
                int sportId = kvp.Key.SportId;
                int disciplineId = kvp.Key.DisciplineId;

                XmlElement xmlTournament = xmlDocument.CreateElement("Tournament");
                xmlTournament.SetAttribute("id", tournamentId.ToString());
                xmlTournament.SetAttribute("sportId", sportId.ToString());
                xmlTournament.SetAttribute("disciplineId", disciplineId.ToString());

                XmlElement xmlCustomers = xmlDocument.CreateElement("Customers");

                foreach (Customer customer in kvp.Value)
                {
                    XmlElement xmlCustomer = xmlDocument.CreateElement("Customer");
                    xmlCustomer.SetAttribute("id", customer.CustomerId.ToString());
                    xmlCustomer.SetAttribute("foreigncustomerid", customer.TtCustomerId.ToString());

                    xmlCustomers.AppendChild(xmlCustomer);
                }

                xmlTournament.AppendChild(xmlCustomers);
                xmlTournaments.AppendChild(xmlTournament);
            }

            xmlRoot.AppendChild(xmlTournaments);

            xmlDocument.AppendChild(xmlRoot);

            string filename = "NTB_SportsData_Complete_"
                + todayDate
                + "T"
                + todayTime
                + ".xml";

            xmlDocument.Save(outputFolder + @"\" + filename);
        }
    }
}