// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GeographicModels.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the RegionModels type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Facade.NIF.SportAccess;
using NTB.SportsDataAdmin.Facade.NFF.TournamentAccess;
using log4net;

namespace NTB.SportsDataAdmin.Application.Models
{
    
    /// <summary>
    /// The region models.
    /// </summary>
    public class GeographicModels
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(GeographicModels));

        /// <summary>
        /// Initializes a new instance of the <see cref="GeographicModels"/> class.
        /// </summary>
        public GeographicModels()
        {
             // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        ///     Gets or sets the customer id.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        ///     Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        ///     Gets or sets the season id.
        /// </summary>
        public int SeasonId { get; set; }

        /// <summary>
        ///     Gets or sets the age category id.
        /// </summary>
        public int AgeCategoryId { get; set; }

        /// <summary>
        ///     Gets or sets the job id.
        /// </summary>
        public int JobId { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether use remote data.
        /// </summary>
        public bool UseRemoteData { get; set; }

        /// <summary>
        ///     Gets or sets the federation id
        /// </summary>
        public int FederationId { get; set; }
        

        /// <summary>
        /// The get regions.
        /// </summary>
        /// <returns>
        /// The <see cref="DetailsJobView"/>.
        /// </returns>
        public DetailsJobView GetDetailsJobView()
        {
            try
            {
                // Setting up the Regions Data Access class
                MunicipalityRepository municipalityRepository = new MunicipalityRepository
                    {
                        JobId = JobId
                    };

                // Setting up the District Data Access class
                DistrictRepository districtRepository = new DistrictRepository();

                List<District> districts = new List<District>(districtRepository.GetAll().ToList());

                // Get the list of municipalities
                List<Municipality> municipalities = new List<Municipality>(municipalityRepository.GetAll().ToList());


                var listMunicipalities = new List<Municipality>(GetSelectedMunicipalities(municipalities));

                // We should also get all the tournaments for this sport
                SportFacade facade = new SportFacade();

                List<string> selectedMunicipalities = listMunicipalities.Select(municipality => municipality.MunicipalityId.ToString()).ToList();
                List<Tournament> tournamentByMunicipalities =
                    facade.GetTournamentByMunicipalities(selectedMunicipalities, JobId, FederationId);

                // We should now combine the values we have from the database, so we get the selected ones checked
                TournamentRepository tournamentsRepository = new TournamentRepository
                {
                    JobId = JobId,
                    CustomerId = CustomerId,
                    SportId = SportId
                };

                // Getting the list of tournaments the customer has selected
                List<Tournament> selectedTournaments = tournamentsRepository.GetTournamentByJobId(JobId).ToList();

                foreach (Tournament selectedTournament in from selectedTournament in tournamentByMunicipalities from selected in selectedTournaments where selected.TournamentId == selectedTournament.TournamentId select selectedTournament)
                {
                    selectedTournament.TournamentSelected = true;
                }

                // Creating the object
                return new DetailsJobView
                    {
                        Districts = districts,
                        Municipalities = municipalities,
                        Tournaments = tournamentByMunicipalities,
                        SelectedMunicipalities = listMunicipalities
                    };

                
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                throw new Exception("An error has occured. Please tell IT to check logs");
            }
        }

        private List<Municipality> GetSelectedMunicipalities(List<Municipality> municipalities)
        {
            if (!municipalities.Any())
            {
                throw new Exception("No municipalities in list");
            }

            return municipalities.Where(r => r.Selected).Select(municipality => new Municipality
                {
                    JobId = JobId,
                    DistrictId = municipality.DistrictId, 
                    MuniciaplityName = municipality.MuniciaplityName, 
                    MunicipalityId = municipality.MunicipalityId, 
                    Selected = municipality.Selected
                }).ToList();
        }
    }
}