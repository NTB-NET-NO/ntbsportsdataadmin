﻿using System.Collections.Generic;
using System.Linq;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Facade.NFF.TournamentAccess;
using NTB.SportsDataAdmin.Facade.NIF.SportAccess;

namespace NTB.SportsDataAdmin.Application.Models
{
    public class AgeCategoryModel
    {
        public List<AgeCategory> GetAgeCategories()
        {
            AgeCategoryRepository repository = new AgeCategoryRepository();
            List<AgeCategory> ageCategories = new List<AgeCategory>(repository.GetAll());
            
            if (!ageCategories.Any())
            {
                // We have to use the services
                TournamentFacade tournamentFacade = new TournamentFacade();

                // Getting the data from the facade
                ageCategories = tournamentFacade.GetAgeCategoriesTournament();
            }

            return ageCategories;
        }

        public List<AgeCategory> GetAgeCategoriesBySportId(int sportId)
        {
            AgeCategoryRepository repository = new AgeCategoryRepository();

            
            var agecategories = new List<AgeCategory>(repository.GetAgeCategoryBySportId(sportId));

            if (!agecategories.Any())
            {
                if (sportId == 16)
                {
                    TournamentFacade facade = new TournamentFacade();
                    agecategories = facade.GetAgeCategoriesTournament();
                }
                else
                {
                    var orgRepository = new OrganizationRepository();
                    Organization organization = orgRepository.GetOrganizationBySportId(sportId);
                    SportFacade facade = new SportFacade();
                    agecategories = facade.GetAgeCategories(organization.SportId);
                }
            }

            return agecategories;
        }
    }
}