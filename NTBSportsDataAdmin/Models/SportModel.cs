﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Facade.Mappers.NIF;

namespace NTB.SportsDataAdmin.Application.Models
{
    public class SportModel
    {
        /// <summary>
        /// The get sports.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Sport> GetSports()
        {
            SportRepository sportRepository = new SportRepository();
            List<Sport> sports = new List<Sport>(sportRepository.GetAll());

            if (!sports.Any())
            {
                sports.Clear();

            }
            return sports;

        }

        public Sport GetSportIdBySeasonId(int seasonId)
        {
            var repository = new SportRepository();
            return repository.GetSportIdBySeasonId(seasonId);

        }

        /// <summary>
        ///     Method to activate the sport so we can choose it
        /// </summary>
        /// <param name="id"></param>
        public void Activate(int id)
        {
            var remoteRepository = new FederationRemoteRepository();
            var disciplines = remoteRepository.GetFederationDisciplines(id);
            var federation = remoteRepository.GetFederation(id);
            var sports = new List<Sport>();
            foreach (var bar in disciplines)
            {
                var sport = new Sport
                {
                    Id = bar.ActivityId,
                    Name = bar.ActivityName,
                    OrgId = federation.FederationId,
                    OrgName = federation.FederationName,
                    HasIndividualResult = federation.HasIndividualResults,
                    HasTeamResults = federation.HasTeamResults

                };

                sports.Add(sport);
            }

            var repository = new SportRepository();

            repository.InsertAll(sports);
        }
    }
}