﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Facade.NFF.TournamentAccess;
using NTB.SportsDataAdmin.Facade.NIF.SportAccess;

namespace NTB.SportsDataAdmin.Application.Models
{
    public class MatchModel
    {
        /// <summary>
        ///     A constructor that at the moment does not do much
        /// </summary>
        public MatchModel()
        {

        }

        public List<Match> GetMatches(int tournamentId, int sportId)
        {
            var repostitory = new MatchesRepository();
            var matches = repostitory.GetMatchesByTournamentId(tournamentId);

            if (matches.Any())
            {
                return matches;
            }

            if (sportId == 16)
            {
                var facade = new TournamentFacade();
                return facade.GetMatchesByTournament(tournamentId, false, false, false, false);
            }
            else
            {
                var facade = new SportFacade();
                return facade.GetMatchesByTournamentIdAndSportId(tournamentId, sportId);
            }

        }
    }
}