﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdminToolModels.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the AdminToolModels type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Xml;
using NTB.SportsDataAdmin.Domain.Classes;
using log4net;

namespace NTB.SportsDataAdmin.Application.Models
{
    /// <summary>
    /// The admin tool models.
    /// </summary>
    public class AdminToolModel
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AdminToolModel));

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminToolModel"/> class.
        /// </summary>
        public AdminToolModel()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// The create customers xml.
        /// </summary>
        /// <param name="customers">
        /// The customers.
        /// </param>
        public void CreateCustomersXml(List<Customer> customers)
        {
            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                XmlElement xmlRootElement = xmlDocument.CreateElement("CustomerList");
                xmlDocument.AppendChild(xmlRootElement);

                XmlElement timeStamp = xmlDocument.CreateElement("Timestamp");
                XmlText xmlText =
                    xmlDocument.CreateTextNode(
                        DateTime.Now.ToShortDateString() + @"T" + DateTime.Now.ToShortTimeString());

                timeStamp.AppendChild(xmlText);
                xmlRootElement.AppendChild(timeStamp);

                XmlElement xmlCustomers = xmlDocument.CreateElement("Customers");

                foreach (Customer customer in customers)
                {
                    XmlElement xmlCustomer = xmlDocument.CreateElement("Customer");

                    XmlElement xmlCustomerName = xmlDocument.CreateElement("Name");
                    xmlText = xmlDocument.CreateTextNode(customer.Name);
                    xmlCustomerName.AppendChild(xmlText);

                    XmlElement xmlCustomerNumber = xmlDocument.CreateElement("Number");

                    xmlText = xmlDocument.CreateTextNode(customer.CustomerId.ToString());
                    xmlCustomerNumber.AppendChild(xmlText);

                    XmlElement xmlCustomerForeignId = xmlDocument.CreateElement("ForeignId");
                    xmlText = xmlDocument.CreateTextNode(customer.TtCustomerId.ToString());
                    xmlCustomerForeignId.AppendChild(xmlText);

                    XmlElement xmlCustomerEmail = xmlDocument.CreateElement("Email");
                    xmlText = xmlDocument.CreateTextNode(customer.Email);
                    xmlCustomerEmail.AppendChild(xmlText);

                    xmlCustomer.AppendChild(xmlCustomerName);
                    xmlCustomer.AppendChild(xmlCustomerNumber);
                    xmlCustomer.AppendChild(xmlCustomerForeignId);
                    xmlCustomer.AppendChild(xmlCustomerEmail);

                    xmlCustomers.AppendChild(xmlCustomer);
                }


                xmlRootElement.AppendChild(xmlCustomers);
                string path = ConfigurationManager.AppSettings["OutputFolder"];
                DirectoryInfo directoryInfo = new DirectoryInfo(path);
                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }
                string filename = path + @"\customerlist.xml";
                xmlDocument.Save(filename);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }
    }
}