﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestModel.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The test model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Linq;

namespace NTB.SportsDataAdmin.Application.Models
{
    /// <summary>
    /// The test model.
    /// </summary>
    public class TestModel
    {
        /// <summary>
        /// The home.
        /// </summary>
        private string home = string.Empty;

        /// <summary>
        /// Gets or sets the tests.
        /// </summary>
        public string Tests
        {
            get
            {
                return home;
            }

            set
            {
                string[] something = value.Split('|');

                string hm = something.Aggregate(string.Empty, (current, tram) => current + (" " + tram));

                home = hm;
            }
        }
    }
}