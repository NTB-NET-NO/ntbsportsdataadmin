﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Facade.NFF.TournamentAccess;
using NTB.SportsDataAdmin.Facade.NIF.SportAccess;
using log4net;
using log4net.Config;

namespace NTB.SportsDataAdmin.Application.Models
{
    /// <summary>
    ///     This is a model that shall return the needed data to show 
    ///     Districts, Municipalities and Tournament
    /// 
    ///     It shall be used for both Soccer (sport=16) and other sports (sport!=16)
    /// </summary>
    public class SelectionViewModel
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SelectionViewModel));

        /// <summary>
        ///     The constructor
        /// </summary>
        public SelectionViewModel()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        /// <summary>
        ///     This method returns a list of stored municipalities
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        private List<Municipality> GetStoredMunicipalities(int jobId)
        {
            var municipalityRepository = new MunicipalityRepository();
            return municipalityRepository.GetMunicipalitiesByJobId(jobId);
        }

        private List<Municipality> GetSelectedMunicipalities(string selectedMunicipalities)
        {
            // Splitting the values into a string array of Ids
            string[] ids = selectedMunicipalities.Split('|');

            // Returning the list of municipalities only using the Ids.
            return ids.Select(id => new Municipality
                {
                    MunicipalityId = Convert.ToInt32(id)
                }).ToList();
        }

        private List<Tournament> GetTournaments(int seasonId, int jobId)
        {
            var tournamentRepository = new TournamentRepository();

            return tournamentRepository.GetTournamentsByJobId(jobId);
        }

        /// <summary>
        ///     Gets the districts for sports
        /// </summary>
        /// <param name="sportId">integer</param>
        /// <returns>list if districts</returns>
        private List<District> GetDistricts(int sportId)
        {
            DistrictRepository repository = new DistrictRepository();
            var districts = new List<District>(repository.GetDistrictsBySportId(sportId));

            if (!districts.Any())
            {
                if (sportId == 16)
                {
                    var facade = new TournamentFacade();
                    districts = facade.GetDistricts();
                }
                else
                {
                    
                    var facade = new SportFacade();
                    districts = facade.GetDistricts();
                }
            }
            return districts;
        }

        private List<Municipality> GetMunicipalities(int sportId)
        {
            var repository = new MunicipalityRepository();
            var municipalities = repository.GetMunicipalitiesBySportId(sportId);

            if (!municipalities.Any())
            {
                if (sportId == 16)
                {
                    var facade = new TournamentFacade();
                    municipalities = facade.GetMunicipalities();
                }
                else
                {
                    var facade = new SportFacade();
                    municipalities = facade.GetMunicipalities();
                }
            }
            return municipalities;
        }

        /// <summary>
        ///     Get the selection view
        /// </summary>
        /// <param name="sportId"></param>
        /// <param name="seasonId"></param>
        /// <param name="jobId"></param>
        /// <returns></returns>
        public SelectionView GetSelectionView(int sportId, int seasonId, int jobId)
        {
            DefineViewModel defineViewModel = new DefineViewModel();
            DefineView defineView = defineViewModel.Get(jobId);

            List<Municipality> municipalities = GetMunicipalities(sportId);
            var storedMunicipalities = GetStoredMunicipalities(jobId);
            var list = new List<Municipality>();

            foreach (Municipality municipality in municipalities)
            {
                foreach (Municipality storedMunicipality in storedMunicipalities.Where(storedMunicipality => storedMunicipality.MunicipalityId == municipality.MunicipalityId))
                {
                    municipality.Selected = true;
                }
                list.Add(municipality);
            }


            // returning the items
            return new SelectionView
                {
                    Municipalities = list,
                    Tournaments = GetTournaments(seasonId, jobId),
                    Districts = GetDistricts(sportId),
                    DefineView = defineView
                };
        }

        public SelectionView GetSelectionView(int sportId, int seasonId, int jobId, string selectedMunicipalities)
        {
            return new SelectionView
                {
                    Municipalities = GetSelectedMunicipalities(selectedMunicipalities),
                    Tournaments = GetTournaments(seasonId, jobId)
                };
        }















        //if (sportId == 16)
        //{
        //    MunicipalityRepository repository = new MunicipalityRepository
        //    {
        //        JobId = jobId,
        //        SportId = sportId
        //    };
        //    return PartialView("GetNFFMunicipalities", new List<DetailsJobView>(repository.GetAll()));

        //}

        //GeographicModels nifGeographicModels = new GeographicModels
        //{
        //    JobId = jobId,
        //    AgeCategoryId = ageCategoryId,
        //    CustomerId = customerId,
        //    SeasonId = seasonId,
        //    UseRemoteData = useRemoteData,
        //    FederationId = federationdId,
        //    SportId = sportId
        //};

        //DetailsJobView detailsJobView = nifGeographicModels.GetDetailsJobView();
    }
}