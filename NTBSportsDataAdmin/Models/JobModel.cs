﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Models
{
    public class JobModel
    {
        public void UpdateJob(int jobId, string jobName, bool schedule, int sportId, int ageCategoryDefinitionId)
        {
            // myJobs.UpdateCustomerJob(CustomerId, CustomerJobName);
            JobRepository repository = new JobRepository();
            var job = new Job
            {
                JobId = jobId,
                Schedule = schedule,
                JobName = jobName,
                AgeCategoryId = ageCategoryDefinitionId,
                SportId = sportId
            };

            repository.Update(job);

        }

        
    }
}