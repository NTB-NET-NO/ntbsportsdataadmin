﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MunicipalityModels.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Application.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace NTB.SportsDataAdmin.Application.Models
{
    /// <summary>
    /// Class that models how we are returning MunicipalitiesDataAccess
    /// </summary>
    public class MunicipalityModel : IMunicipalityModel
    {
        /// <summary>
        ///     Gets or sets the customer id.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        ///     Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        ///     Gets or sets the season id.
        /// </summary>
        public int SeasonId { get; set; }

        /// <summary>
        ///     Gets or sets the age category id.
        /// </summary>
        public int AgeCategoryId { get; set; }

        /// <summary>
        ///     Gets or sets the job id.
        /// </summary>
        public int JobId { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether use remote data.
        /// </summary>
        public bool UseRemoteData { get; set; }

        /// <summary>
        ///     Gets or sets the selected municipalities
        /// </summary>
        public string[] SelectedMunicipalities
        {
            get { return PrivateSelectedMunicipalities; }
            set { PrivateSelectedMunicipalities = value.ToString().Split(','); }
        }
        
        /// <summary>
        ///     String array of selected municipalities
        /// </summary>
        private string[] PrivateSelectedMunicipalities { get; set; }

        /// <summary>
        ///     Inserts municipalities
        /// </summary>
        /// <param name="municipalities"></param>
        /// <param name="jobId"></param>
        public void InsertMunicipalities(List<Municipality> municipalities, int jobId)
        {
            // Now we are deleting and inserting
            var repository = new CustomerMunicipalityRepository();
            var customerMunicipality = new CustomerMunicipality
                {
                    JobId = jobId
                };
            
            repository.Delete(customerMunicipality);

            // And so we are inserting
            foreach (Municipality municipality in municipalities)
            {
                customerMunicipality.MunicipalityId = municipality.MunicipalityId;
                repository.InsertOne(customerMunicipality);
            }
        }

        /// <summary>
        /// The get municipalities.
        /// </summary>
        /// <returns>
        /// The <see cref="Municipality"/>.
        /// </returns>
        public List<Municipality> GetMunicipalities()
        {
            // TODO: Refactor this so it only returns municipalities
            // TODO: create new class related to Tournaments which only returns tournaments
            // TODO: Create one method which puts the two lists together

            JobRepository repository = new JobRepository();

            MunicipalityRepository municipalityRepository = new MunicipalityRepository();

            // Creating the Tournaments Database Access object
            TournamentRepository tournaments = new TournamentRepository
                {
                    JobId = JobId, 
                    SportId = SportId
                };

            // Setting the JobId parameter

            // Getting the selected Tournaments for the customer by calling GetTournamentsForCustomer
            List<Tournament> listTournaments =
                new List<Tournament>(tournaments.GetTournamentsByJobIdAndSportId());

            // Getting the selectedMunicipalities
            List<int> listMunicipalities = new List<int>(repository.GetJobsMunicipalities(JobId).Distinct());

            // Updating the job with the age category
            repository.UpdateCustomerJobsWithAgeCategory(JobId, AgeCategoryId, SportId);

            // TODO: It could be that tournament and selectedMunicipalities needs a Id Column
            if (listTournaments.Any())
            {
                municipalityRepository.JobId = JobId;

                // return PartialView("GetSelectedMunicipalities", tournamentModel.GetMunicipalitiesAndTournaments(listTournaments)); 
                if (listMunicipalities.Any())
                {
                    return municipalityRepository.GetAll().ToList();
                    // (SportId, AgeCategoryId, listTournaments, listMunicipalities, SeasonId, JobId, UseRemoteData);


                    // return PartialView("GetMunicipalities", 
                }

                return municipalityRepository.GetAll().ToList();
            }

            return municipalityRepository.GetAll().ToList();
        }

        /// <summary>
        ///     Get the selected municipalities
        /// </summary>
        /// <param name="selectedMunicipalities"></param>
        /// <param name="jobId"></param>
        /// <returns></returns>
        public List<Municipality> GetSelectedMunicipalities(string selectedMunicipalities, int jobId)
        {
            var municipalities = new List<Municipality>();
            if (selectedMunicipalities.Contains("|"))
            {
                // We are getting the selectedMunicipalities as a string that contains | as separator
                List<string> stringMunicipalities =
                    selectedMunicipalities.Split('|').Where(x => !string.IsNullOrEmpty(x)).Distinct().ToList();

                municipalities.AddRange(stringMunicipalities.Select(municipality => new Municipality
                {
                    JobId = jobId,
                    MunicipalityId = Convert.ToInt32(municipality),
                    Selected = true
                }));
            }
            else
            {
                if (selectedMunicipalities != string.Empty)
                {
                    var selectedMunicipality = new Municipality
                    {
                        JobId = jobId,
                        MunicipalityId = Convert.ToInt32(selectedMunicipalities),
                        Selected = true
                    };
                    municipalities.Add(selectedMunicipality);
                }
            }

            return municipalities;
        }

    }
}