﻿using System.Collections.Generic;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Models
{
    public class DetailsViewModel
    {
        private readonly CustomerRepository _customersDataAccess = new CustomerRepository();

        public DetailsViewModel()
        {
            
        }

        // Getting the details for the view
        public DetailsView GetDetails(int customerId)
        {
            var view = new DetailsView();

            // Get the customer object
            Customer customer = _customersDataAccess.GetOneCustomer(customerId);
            view.Customer = customer;

            // Get the jobs for this customer
            JobRepository jobRepository = new JobRepository
            {
                CustomerId = customerId
            };

            view.Jobs = new List<Job>(jobRepository.GetAll());

            // Get the list of sports
            SportRepository sportRepository = new SportRepository();
            view.Sports = new List<Sport>(sportRepository.GetAll());

            // Get the Age Category Definitions
            AgeCategoryDefinitionRepository ageCategoryDefinitionRepository = new AgeCategoryDefinitionRepository();
            view.AgeCategoryDefinitions = new List<AgeCategoryDefinition>(ageCategoryDefinitionRepository.GetAll());

            return view;
        }
    }
}