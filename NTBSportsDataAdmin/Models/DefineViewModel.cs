﻿using System;
using System.Collections.Generic;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Models
{
    public class DefineViewModel
    {

        public DefineView Get(int jobId)
        {
            var defineView = new DefineView();

            var jobRepository = new JobRepository();
            
            var customerjob = jobRepository.GetCustomerJobByJobId(jobId);


            if (customerjob.TeamSport == 1)
            {

                var seasonRepository = new SeasonRepository();
                defineView.Seasons = seasonRepository.GetSeasonsBySportId(customerjob.SportId);
            }
            else
            {
                var year = DateTime.Today.Year-1;


                var seasons = new List<Season>();
                for (int i = year; i < year + 2; i++)
                {
                    var season = new Season
                    {
                        SeasonId = i,
                        SportId = customerjob.SportId,
                        SeasonName = "Sesong " + i,
                        SeasonActive = true

                    };
                    seasons.Add(season);
                }

                defineView.Seasons = seasons;
            }

            var viewModel = new DetailsViewModel();
            defineView.DetailsView = viewModel.GetDetails(customerjob.CustomerId);
            defineView.CustomerJob = customerjob;

            return defineView;
        }
    }
}