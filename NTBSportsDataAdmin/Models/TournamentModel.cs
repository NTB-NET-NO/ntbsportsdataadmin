﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentModels.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The tournament models.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsDataAdmin.Application.Interfaces;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Facade.NFF.TournamentAccess;
using NTB.SportsDataAdmin.Facade.NIF.SportAccess;
using NTB.SportsDataAdmin.Facade.Profixio.ProfixioFacade;
using log4net;
using log4net.Config;

namespace NTB.SportsDataAdmin.Application.Models
{
    // using Tournaments = NTBSportsDataAdmin.Models.RemoteDataAccess.Tournaments;
    
    // This shall be the main TournamentModel that we will use to model the tournament and municipality

    /// <summary>
    ///     The tournament models.
    /// </summary>
    public class TournamentModel : ITournamentModel
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof (TournamentModel));

        /// <summary>
        ///     Initializes a new instance of the <see cref="TournamentModel" /> class.
        /// </summary>
        public TournamentModel()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        // (jobId, ageCategoryId, federationId, sportId, seasonId)
        public List<Tournament> GetTournamentsForView(List<Municipality> municipalities, int jobId, int ageCategory, int orgId, int sportId, int seasonId)
        {
            var storedTournaments = GetTournamentsByJobId(jobId);

            List<string> selectedMunicipalities = municipalities.Select(municipality => municipality.MunicipalityId.ToString()).ToList();

            if (orgId == 0)
            {
                var organizationRepository = new OrganizationRepository();
                orgId = organizationRepository.GetOrganizationBySportId(sportId).OrganizationId;
            }
            Logger.Debug("We are working on results for orgid " + orgId);
            List<Tournament> tournaments;
            if (orgId == 365)
            {
                var facade = new Facade.NFF.TournamentAccess.TournamentFacade
                    {
                        AgeCategory = ageCategory
                    };
                tournaments = facade.GetTournamentByMunicipalities(selectedMunicipalities, seasonId);
            }
            else if (orgId == 2000)
            {
                var clubRepository = new ClubRepository();
                var clubs = clubRepository.GetClubsByMunicipalities(selectedMunicipalities, orgId);

                var facade = new Facade.Profixio.ProfixioFacade.TournamentFacade();

                // Only sending the club Ids
                var clubIds = new List<int>();
                clubIds = (from c in clubs select c.ClubId).ToList();
                tournaments = facade.GetTournamentByClubs(clubIds, seasonId);
            }
            else
            {
                SportFacade facade = new SportFacade();
                tournaments = facade.GetTournamentByMunicipalities(selectedMunicipalities, seasonId, orgId);
            }

            Logger.Debug("We have " + tournaments.Count + " number of tournaments after getting tournaments for selected municipalities.");

            var tournamentRepository = new TournamentRepository();
            var storedtournaments = tournamentRepository.GetTournamentsBySportId(sportId);

            Logger.Debug("We have " + storedtournaments.Count + " number of tournaments in the database for this sport.");

            List<Tournament> tournamentsNotStored = new List<Tournament>();
            foreach (Tournament t1 in tournaments)
            {
                foreach (Tournament t2 in storedtournaments)
                {
                    if (t1.TournamentId == t2.TournamentId)
                    {
                        continue;
                    }

                    tournamentsNotStored.Add(t1);
                }
            }
            //List<Tournament> tournaments1 = (from t in storedtournaments from t1 in tournaments.Where(t1 => t.TournamentId == t1.TournamentId) select t).Select(t => new Tournament
            //    {
            //        TournamentAccepted = t.TournamentAccepted, 
            //        TournamentName = t.TournamentName, 
            //        AgeCategoryDefinitionId = t.AgeCategoryDefinitionId, 
            //        AgeCategoryFilterId = t.AgeCategoryFilterId, 
            //        AgeCategoryId = t.AgeCategoryId, 
            //        DistrictId = t.DistrictId, 
            //        Division = t.Division, 
            //        GenderId = t.GenderId, 
            //        JobId = t.JobId, 
            //        MunicipalityId = t.MunicipalityId, 
            //        Push = t.Push, 
            //        SeasonId = t.SeasonId, 
            //        SportId = t.SportId, 
            //        TournamentId = t.TournamentId, 
            //        TournamentNumber = t.TournamentNumber, 
            //        TournamentSelected = t.TournamentSelected, 
            //        TournamentTypeId = t.TournamentTypeId
            //    }).ToList();

            Logger.Debug("We have " + tournamentsNotStored.Count + " number of tournaments after first filtering.");
            AgeCategoryRepository repository = new AgeCategoryRepository();
            IEnumerable<AgeCategory> categories = repository.GetAgeCategoryBySportId(sportId).Where(x => x.AgeCategoryDefinitionId == ageCategory);

            List<Tournament> returnTournament = tournamentsNotStored.Where(x => categories.Any(c => c.AgeCategoryDefinitionId == x.AgeCategoryDefinitionId)).ToList();

            Logger.Debug("We have " + returnTournament.Count + " number of tournaments after age category filtering.");

            if (returnTournament.Any())
            {
                tournaments.Clear();
                tournaments = returnTournament;
            }

            List<Tournament> items = new List<Tournament>();
            
            foreach (Tournament tournament in tournaments)
            {
                Tournament tournament1 = tournament;
                foreach (Tournament storedTournament in storedTournaments.Where(storedTournament => storedTournament.TournamentId == tournament1.TournamentId))
                {
                    tournament1.TournamentSelected = true;
                    
                }


                tournament1.SportId = sportId;
                tournament.AgeCategoryDefinitionId = ageCategory;
                items.Add(tournament1);
            }

            Logger.Debug("We have " + items.Count + " number of tournaments after last and final filtering.");

            tournamentRepository.InsertAll(items);
            
            // return !items.Any() ? tournaments : items;
            return items;
        }

        /// <summary>
        ///     The get tournaments for job.
        /// </summary>
        /// <param name="selectedTournaments">
        ///     The selected tournaments.
        /// </param>
        /// <param name="selectedMunicipalities">
        ///     The selected municipalities.
        /// </param>
        /// <param name="ageCategoryId">
        ///     The age category id.
        /// </param>
        /// <param name="seasonId">
        ///     The season id.
        /// </param>
        /// <param name="jobId">
        ///     The job id.
        /// </param>
        /// <param name="federationId">
        ///     The federation id.
        /// </param>
        /// <param name="sportId"></param>
        /// <param name="useFiks">
        ///     The use fiks.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsForJob(
            List<Tournament> selectedTournaments,
            List<Municipality> selectedMunicipalities,
            int ageCategoryId,
            int seasonId,
            int jobId,
            int sportId,
            int federationId,
            bool useFiks)
        {
            var tournamentsForJob = new List<Tournament>();

            Logger.Info("Selected MunicipalitiesDataAccess: " + selectedMunicipalities);

            try
            {
                List<Tournament> availableTournaments;

                if (sportId == 16)
                {
                    var facade = new Facade.NFF.TournamentAccess.TournamentFacade();

                    List<string> stringSelectedMunicipalites = selectedMunicipalities.Select(municipality => municipality.MunicipalityId.ToString()).ToList();

                    availableTournaments = facade.GetTournamentByMunicipalities(stringSelectedMunicipalites, seasonId);

                }
                else
                {
                    
                    SportFacade facade = new SportFacade();
                    
                    var ageCategoryRepository = new AgeCategoryRepository
                        {
                            SportId = sportId
                        };

                    List<string> listMunicipalities = selectedMunicipalities.Select(municipality => municipality.MunicipalityId.ToString()).ToList();
                    availableTournaments =
                        facade.GetTournamentByMunicipalities(listMunicipalities,  seasonId, federationId);
                }

                CheckExistingTournaments(sportId, availableTournaments);

                tournamentsForJob.AddRange(availableTournaments.Select(selectedTournament => new Tournament
                    {
                        TournamentId = selectedTournament.TournamentId, 
                        TournamentName = selectedTournament.TournamentName, 
                        AgeCategoryId = selectedTournament.AgeCategoryId, 
                        DistrictId = selectedTournament.DistrictId, 
                        GenderId = selectedTournament.GenderId, 
                        MunicipalityId = selectedTournament.MunicipalityId, 
                        SeasonId = selectedTournament.SeasonId, 
                        SportId = selectedTournament.SportId, 
                        TournamentNumber = selectedTournament.TournamentNumber
                    }).Select(tournament => AddSelectedTournament(selectedTournaments, tournament)));
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                Logger.Error(e.StackTrace);

                return null;
            }

            return tournamentsForJob.OrderByDescending(tournament => tournament.AgeCategoryId).ToList();
        }

        private static Tournament AddSelectedTournament(List<Tournament> selectedTournaments, Tournament tournament)
        {
            /** 
             * check if the tournament number is in either selected tournament or available tournament.
             * If it is we add some more parameters to the tournament object
             */

            if (selectedTournaments.Any(tournamentJob => tournament.TournamentNumber == tournamentJob.TournamentNumber))
            {
                tournament.TournamentSelected = true;
                tournament.TournamentAccepted = 1;
            }
            return tournament;
        }

        private static void CheckExistingTournaments(int sportId, List<Tournament> availableTournaments)
        {
            var tournamentRepository = new TournamentRepository
                {
                    SportId = sportId
                };

            foreach (Tournament tournament in from tournament in availableTournaments
                                              let foundTournament = tournamentRepository.Search(tournament)
                                              where foundTournament == false
                                              select tournament)
            {
                // We shall insert the tournament
                tournamentRepository.InsertOne(tournament);
            }
        }

        /// <summary>
        ///     The store tournaments.
        /// </summary>
        public List<Tournament> NormalizeTournaments(string selectedTournaments)
        {
            // Splitting the tournaments that is separated by comma
            if (string.IsNullOrEmpty(selectedTournaments))
            {
                return new List<Tournament>();
            }
            string[] splittedTournaments = selectedTournaments.Split(',');

            return splittedTournaments.Select(tournamentIds => tournamentIds.Split('/')).Select(splittedTournament => new Tournament
                {
                    TournamentId = Convert.ToInt32(splittedTournament[1]), 
                    TournamentNumber = splittedTournament[0]
                }).ToList();

        }

        public Tournament GetTournamentById(int tournamentId, int seasonId)
        {
            var facade = new SportFacade();
            return facade.GetTournamentById(tournamentId, seasonId);
        }

        /// <summary>
        ///     This method shall insert a tournament into the tournament database table
        /// </summary>
        /// <param name="tournament"></param>
        public void InsertTournament(Tournament tournament)
        {
            var repository = new TournamentRepository();
            repository.InsertOne(tournament);
        }

        /// <summary>
        ///     Insets selected tournaments into database
        /// </summary>
        /// <param name="selectedTournaments"></param>
        /// <param name="jobId"></param>
        /// <param name="ageCategoryId"></param>
        /// <param name="customerId"></param>
        /// <param name="sportId"></param>
        public void InsertSelectedTournaments(List<Tournament> selectedTournaments, int jobId, int ageCategoryId, int customerId, int sportId)
        {
            try
            {
                DeleteTournamentByJobId(jobId);

                // We are now adding 
                if (selectedTournaments.Count == 0)
                {
                    return;
                }

                // Now we are inserting information about the tournament in the database
                var customerTournamentRepository = new CustomerTournamentRepository();

                // we have to add more data to the tournament object
                List<Tournament> tournaments = selectedTournaments.Select(tournament => new Tournament
                    {
                        TournamentId = tournament.TournamentId, TournamentNumber = tournament.TournamentNumber, JobId = jobId, AgeCategoryId = ageCategoryId, SportId = sportId
                    }).ToList();

                customerTournamentRepository.InsertAll(tournaments);
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                Logger.Error(e.StackTrace);
            }
        }

        public List<Tournament> GetTournamentsByJobId(int jobId)
        {
            var repository = new TournamentRepository();
            var tournaments = repository.GetTournamentByJobId(jobId);

            return tournaments;
        }

        public List<Tournament> GetTournamentsBySeasonId(int seasonId)
        {
            var repository = new TournamentRepository();
            return new List<Tournament>(repository.GetTournamentsBySeasonId(seasonId));
        }

        public List<Tournament> GetTournamentsBySportId(int sportId)
        {
            var repository = new TournamentRepository();
            return new List<Tournament>(repository.GetTournamentsBySportId(sportId));
            
        }

        public Tournament GetTournament(int tournamentId)
        {
            var repository = new TournamentRepository();
            return repository.Get(tournamentId);
        }

        public void DeleteTournament(int tournamentId)
        {
            // We shall remove all tournaments related to this job
            var repository = new TournamentRepository();
            Tournament tournament = new Tournament
                {
                    TournamentId = tournamentId
                };
                 
            repository.Delete(tournament);
        }

        public void DeleteTournamentByJobId(int jobId)
        {
            // We shall remove all tournaments related to this job
            var repository = new TournamentRepository();
            repository.DeleteTournamentByJobId(jobId);
        }

        internal void UpdateTournamentAgeCategoryDefinitition(Dictionary<int, int> ints )
        {
            var repository = new TournamentRepository();

            foreach (KeyValuePair<int, int> kvp in ints)
            {
                repository.UpdateTournamentAgeCategoryDefinition(kvp.Key, kvp.Value);
            }
        }
    }
}