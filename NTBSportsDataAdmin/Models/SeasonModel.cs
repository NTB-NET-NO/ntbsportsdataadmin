﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Facade.NFF.TournamentAccess;
using NTB.SportsDataAdmin.Facade.NIF.SportAccess;

namespace NTB.SportsDataAdmin.Application.Models
{
    public class SeasonModel
    {
        public SeasonModel()
        {
            
        }

        public Season GetSeasonBySeasonId(int seasonId)
        {
            var repository = new SeasonRepository();
            return repository.Get(seasonId);
        }

        public IEnumerable<Season> GetSeasonsBySportId(int sportId)
        {
            var repository = new SeasonRepository();
            List<Season> seasons = repository.GetSeasonsBySportId(sportId).ToList();

            List<Season> federationSeasons = new List<Season>();
            // Now we get the list of seasons from the APIs.

            if (sportId == 16)
            {
                List<Season> s = new List<Season>();
                foreach (Season season in seasons)
                {
                    season.FederationDiscipline = 207;
                }

                var facade = new TournamentFacade();
                federationSeasons = facade.GetSeasons();
                
                foreach (Season season in federationSeasons)
                {
                    season.SportId = 16;
                    season.FederationDiscipline = 207;    
                }

            }
            else
            {
                var federationRespository = new FederationRepository();
                var federation = federationRespository.GetFederationBySportId(sportId);
                var facade = new SportFacade();
                federationSeasons = facade.GetFederationSeasons(federation.FederationId);
            }

            
            foreach (Season season in federationSeasons)
            {
                Season season1 = season;
                var foo = from s in seasons where s.SeasonId == season1.SeasonId select s;
                season1.SportId = sportId;
                if (!foo.Any())
                {
                    seasons.Add(season1);
                }
            }

            return seasons;
        }

        internal void InsertSeasons(int id, IEnumerable<Season> seasons)
        {
            SeasonRepository repository = new SeasonRepository();

            foreach (Season season in seasons)
            {
                var foundSeason = repository.GetSeason(season);

                if (foundSeason.SeasonId == season.SeasonId)
                {
                    continue;
                }
                repository.InsertOne(season);
            }

            // repository.InsertAll(seasons.ToList());
        }

        public void UpdateSeason(int id, FormCollection collection)
        {
            var repository = new SeasonRepository();
            var season = new Season
                {
                    SeasonId = id,
                    SeasonName = collection["SeasonName"],
                    SeasonActive = false,
                    SeasonStartDate = Convert.ToDateTime(collection["SeasonStartDate"]),
                    SeasonEndDate = Convert.ToDateTime(collection["SeasonEndDate"]),
                    SportId = Convert.ToInt32(collection["SportId"])
                };

            if (collection["seasonactive"] != null)
            {
                string seasonActive = collection["seasonactive"].ToLower();
                if (seasonActive == "on" || seasonActive == "true" || seasonActive == "1")
                {
                    season.SeasonActive = true;
                }
            }

            repository.Update(season);
        }

        public void DeleteSeason(Season season)
        {
            var repository = new SeasonRepository();
            repository.Delete(season);
        }

        public void ActivateSeason(int id)
        {
            var repository = new SeasonRepository();
            repository.ActivateSeason(id);
        }

        public void DeActivateSeason(int id)
        {
            var repository = new SeasonRepository();
            repository.DeActivateSeason(id);
        }


        internal void SetSeasonDisciplines(Dictionary<int, int> seasonDisciplines)
        {
            // TODO: Loop over season Disciplines
            // TODO: Create Set Season Discipline Method in Season Repository
            
            foreach (KeyValuePair<int, int> kvp in seasonDisciplines)
            {
                var seasonRepository = new SeasonRepository();
                seasonRepository.UpdateSeasonWithDiscipline(kvp.Key, kvp.Value);
            }

        }
    }
}