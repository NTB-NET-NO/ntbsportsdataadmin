// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ToolsModels.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Xml;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Facade.NFF.TournamentAccess;
using NTB.SportsDataAdmin.Facade.NIF.SportAccess;
using NTB.SportsDataAdmin.Facade.Profixio.ProfixioFacade;
using log4net;
using log4net.Config;

namespace NTB.SportsDataAdmin.Application.Models
{
    /// <summary>
    ///     This class will create different models to get data directly from the NFF database.
    ///     It will therefor use the NFF API methods
    /// </summary>
    public class ToolsModels
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof (ToolsModels));

        /// <summary>
        ///     Initializes a new instance of the <see cref="ToolsModels" /> class.
        /// </summary>
        public ToolsModels()
        {
            
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        public int SportId { get; set; }

        public int FederationId { get; set; }

        /// <summary>
        ///     The get districts.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<District> GetDistricts()
        {
            Facade.NFF.TournamentAccess.TournamentFacade facade = new Facade.NFF.TournamentAccess.TournamentFacade();
            return facade.GetDistricts();
        }

        /// <summary>
        ///     The get tournaments.
        /// </summary>
        /// <param name="districtId">
        ///     The district id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournaments(int districtId)
        {
            int seasonid = 0;
            if (GetSeason() > 0)
            {
                seasonid = GetSeason();
            }

            Facade.NFF.TournamentAccess.TournamentFacade facade = new Facade.NFF.TournamentAccess.TournamentFacade();
            return facade.GetTournamentsByDistrict(districtId, seasonid);

        }

        /// <summary>
        ///     The get seasons.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Season> GetSeasons()
        {
            var facade = new Facade.NFF.TournamentAccess.TournamentFacade();
            return facade.GetSeasons();
        }

        /// <summary>
        ///     The get quaranties.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Karantener> GetQuaranties()
        {
            int seasonid = 0;
            if (GetSeason() > 0)
            {
                seasonid = GetSeason();
            }
            var facade = new Facade.NFF.TournamentAccess.TournamentFacade();
            List<Tournament> tournaments = facade.GetTournamentsByDistrict(1, seasonid);

            foreach (Tournament tournament in tournaments)
            {
                if (tournament.TournamentName.Contains("Tippeligaen"))
                {
                }
            }

            var myKaranteneList = new List<Karantener>();

            // Match[] matches = tclient.GetMatchesByDateInterval(DateTime.Parse("2011-01-01"), DateTime.Parse("2011-04-26"), 1, true, false, false, true);
            // foreach (Match match in matches)
            // {
            // foreach (MatchEvent matchEvent in match.MatchEventList)
            // {
            // MatchEventType myMatchEventType = new MatchEventType();

            // if (myMatchEventType.EventTypeId == matchEvent.MatchEventTypeId)
            // {

            // }

            // }

            // }

            // We are not done here. 
            // TODO: Complete this method

            return myKaranteneList;
        }

        /// <summary>
        ///     The get number of matches for the selected period.
        /// </summary>
        /// <param name="dateStart">
        ///     The date Start.
        /// </param>
        /// <param name="dateEnd">
        ///     The date End.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<NumberOfMatches> GetNumberofMatches(string dateStart, string dateEnd)
        {
            var repository = new CalendarRepository();

            var startDate = Convert.ToDateTime(dateStart);
            var endDate = Convert.ToDateTime(dateEnd);

            return repository.GetMatchesByDateInterval(startDate, endDate);

        }

        private List<Tournament> SearchTournaments(List<Tournament> tournaments)
        {
            List<Tournament> tournamentsNotFound = new List<Tournament>();
            foreach (Tournament tournament in tournaments)
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
                {
                    try
                    {

                        var command = new SqlCommand("SportsData_SearchTournaments", sqlConnection);

                        command.Parameters.Add(
                            new SqlParameter("@TournamentId", tournament.TournamentId));

                        command.Parameters.Add(new SqlParameter("@DistrictId", tournament.DistrictId));

                        command.Parameters.Add(new SqlParameter("@SeasonId", tournament.SeasonId));

                        command.Parameters.Add(
                            new SqlParameter("@AgeCategoryId", tournament.AgeCategoryId));

                        command.Parameters.Add(new SqlParameter("@GenderId", tournament.GenderId));

                        command.Parameters.Add(
                            new SqlParameter("@TournamentTypeId", tournament.TournamentTypeId));

                        command.Parameters.Add(
                            new SqlParameter("@TournamentNumber", tournament.TournamentNumber));

                        command.Parameters.Add(
                            new SqlParameter("@TournamentName", tournament.TournamentName));

                        command.Parameters.Add(new SqlParameter("@Push", tournament.Push));

                        command.Parameters.Add(new SqlParameter("@SportId", tournament.SportId));

                        command.CommandType = CommandType.StoredProcedure;

                        if (sqlConnection.State == ConnectionState.Closed)
                        {
                            sqlConnection.Open();
                        }

                        SqlDataReader sqlDataReader = command.ExecuteReader();

                        if (!sqlDataReader.HasRows)
                        {
                            tournamentsNotFound.Add(tournament);
                        }
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);
                    }
                    finally
                    {
                        if (sqlConnection.State == ConnectionState.Open)
                        {
                            sqlConnection.Close();
                        }
                    }

                } 
            }

            // returning the list of not found tournaments
            return tournamentsNotFound;
        }


        public void UpdateSoccerTournamentsBySeason(int season)
        {
            var facade = new Facade.NFF.TournamentAccess.TournamentFacade();

            List<int> listIds = new List<int>();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_GetDistricts", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }


                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return;
                    }

                    while (sqlDataReader.Read())
                    {
                        // Get the tournament Id from the Database
                        int districtId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                        listIds.Add(districtId);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                foreach (int districtId in listIds)
                {
                    var tournaments = facade.GetTournamentsByDistrict(districtId, Convert.ToInt32(season));
                    SearchTournaments(tournaments);
                }

                

            }
        }

        public void UpdateProfixioTournamentBySeason(int season)
        {
            var facade = new Facade.Profixio.ProfixioFacade.TournamentFacade();

            List<Tournament> tournaments = facade.GetTournamentsBySeasonId(season);

            // We have to add a bit more information
            foreach (var tournament in tournaments)
            {
                tournament.SeasonId = season;
                tournament.SportId = SportId;
                tournament.Push = false;
                tournament.AgeCategoryId = 8;
                tournament.AgeCategoryDefinitionId = 1;
                tournament.GenderId = 1;
            }

            List<Tournament> notFoundTournaments = SearchTournaments(tournaments);

            var repository = new TournamentRepository();
            repository.InsertAll(notFoundTournaments);
        }

        public void UpdateSportTournamentsBySeason(int season)
        {
            var facade = new SportFacade();

            List<Tournament> tournaments = facade.GetTournamentsBySeasonId(season);

            List<Tournament> notFoundTournaments = SearchTournaments(tournaments);

            // not found shall be inserted
            var repository = new TournamentRepository();
            repository.InsertAll(notFoundTournaments);
        }

        /// <summary>
        ///     The update tournaments by season.
        /// </summary>
        /// <param name="collection">
        ///     The string season.
        /// </param>
        public void UpdateTournamentsBySeason(FormCollection collection)
        {
            int season = Convert.ToInt32(collection["sesong"]);

            if (SportId == 16)
            {
                // Soccer
                UpdateSoccerTournamentsBySeason(season);
            }
            else if (SportId == 2000)
            {
                // Norway Cup
                UpdateProfixioTournamentBySeason(season);
            } 
            else 
            {
                UpdateSportTournamentsBySeason(season);
            }
        }

        // This method updates the tournament databasetable. We have to use the districts first
            

        /// <summary>
        ///     The update tournaments.
        /// </summary>
        public void UpdateTournaments()
        {
            var facade = new Facade.NFF.TournamentAccess.TournamentFacade();
            TournamentRepository repository = new TournamentRepository
                {
                    SportId = SportId
                };
            List<Tournament> tournaments = new List<Tournament>(repository.GetAll());

            foreach (Tournament domainobject in tournaments)
            {
                try
                {
                    Tournament tournament = facade.GetTournament(domainobject.TournamentId);

                    domainobject.TournamentId = tournament.TournamentId;
                    domainobject.TournamentName = tournament.TournamentName;
                    domainobject.SeasonId = tournament.SeasonId;
                    domainobject.AgeCategoryId = tournament.AgeCategoryId;
                    
                    domainobject.DistrictId = tournament.DistrictId;
                    domainobject.Push = tournament.Push;
                    domainobject.GenderId = tournament.GenderId;

                    repository.Update(domainobject);
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
            }
        }

        public List<Sport> GetSports()
        {
            // Getting the sports
            SportRepository repository = new SportRepository();
            return new List<Sport>(repository.GetAll());
        }

        /// <summary>
        ///     The update sports table.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Sport> UpdateSportsTable()
        {
            
            // Creating a list that contains the Sports object
            var listOfSports = new List<Sport>();
            SportFacade facade = new SportFacade();
            var federationResult = facade.GetFederations();


            foreach (Federation federation in
                federationResult.Where(federation => federation.HasIndividualResults == 1 || federation.HasTeamResults == 1))
            {
                InsertFederation(federation);

                if (federation.FederationDisciplines == null)
                {
                    continue;
                }

                foreach (FederationDiscipline federationDiscipline in federation.FederationDisciplines)
                {
                    InsertFederationDiscipline(federationDiscipline, federation);
                }
            }

            // Getting the list of sports from the database and another method
            listOfSports.AddRange(GetSports());
            
            return listOfSports;
        }

        private static void InsertFederationDiscipline(FederationDiscipline federationDiscipline, Federation federation)
        {
            SportFacade facade = new SportFacade();
            var federationDisciplineResult = facade.GetFederationDisciplines(federation.FederationId);

            var federationDisciplinesRepository = new FederationDisciplinesRepository();

            federationDisciplinesRepository.InsertAll(federationDisciplineResult);
        }

        /// <summary>
        /// Inserting information about the federation/Sports into the database (local)
        /// </summary>
        /// <param name="federation"></param>
        private static void InsertFederation(Federation federation)
        {
            FederationRepository federationRepository = new FederationRepository();
            Federation ntbFederation = new Federation
                {
                    FederationId = federation.FederationId,
                    FederationName = federation.FederationName,
                    HasTeamResults = federation.HasTeamResults,
                    HasIndividualResults = federation.HasIndividualResults,
                    BranchCode = federation.BranchCode,
                    BranchId = federation.BranchId,
                    BranchName = federation.BranchName
                };

            federationRepository.InsertOne(ntbFederation);
        }

        /// <summary>
        ///     Get federations
        /// </summary>
        /// <returns></returns>
        private List<Federation> GetFederations()
        {
            SportFacade sportFacade = new SportFacade();
            return sportFacade.GetFederations();
        }

        /// <summary>
        ///     The list sports.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Sport> ListSports()
        {
            var sportRepository = new SportRepository();
            return new List<Sport>(sportRepository.GetAll());
        }

        /// <summary>
        ///     The delete sport.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        public void DeleteSport(int id)
        {
            Sport sport = new Sport
                {
                    Id = id
                };

            var sportRepository = new SportRepository();
            sportRepository.Delete(sport);
        }

        /// <summary>
        ///     The create municipality table.
        /// </summary>
        public void CreateRegionsTable()
        {
            var facade = new SportFacade();

            var regions = facade.GetMunicipalities();

            var municipalityRepository = new MunicipalityRepository
            {
                SportId = 23
            };

            municipalityRepository.DeleteAll();
            municipalityRepository.InsertAll(regions);
        }

        /// <summary>
        ///     The create county table.
        /// </summary>
        public void CreateCountyTable()
        {
            SportFacade facade = new SportFacade();
            List<District> regions = facade.GetDistricts();
            
            var repository = new DistrictRepository();

            // Removing all counties in database
            repository.DeleteAll();
            
            
            foreach (var region in regions)
            {
                // Just setting it to something else than 16
                region.SportId = 23;
                repository.InsertOne(region);
            }
        }

        /// <summary>
        ///     The get tournament.
        /// </summary>
        /// <param name="tournamentId">
        ///     The tournament id.
        /// </param>
        /// <returns>
        ///     The <see cref="Tournament" />.
        /// </returns>
        internal Tournament GetTournament(int tournamentId)
        {
            try
            {
                var facade = new Facade.NFF.TournamentAccess.TournamentFacade();
                return facade.GetTournament(tournamentId);
            }
            catch (Exception exception)
            {
                Logger.Info(exception.Message);
                Logger.Info(exception.StackTrace);
                return null;
            }
        }

        /// <summary>
        ///     The get season.
        /// </summary>
        /// <returns>
        ///     The <see cref="int" />.
        /// </returns>
        private int GetSeason()
        {
            var facade = new Facade.NFF.TournamentAccess.TournamentFacade();
            List<Season> seasons = facade.GetSeasons();

            return (from season in seasons where season.SeasonActive select season.SeasonId).FirstOrDefault();
        }
        
        public void PopulateNffAgeCategoryTable()
        {
            var facade = new Facade.NFF.TournamentAccess.TournamentFacade();

            List<AgeCategory> ageCategories = facade.GetAgeCategoriesTournament();

            InsertNffAgeCategoryTable(ageCategories);
        }

        /// <summary>
        ///     Populating the AgeCategoryTable with NFF data
        /// </summary>
        public void InsertNffAgeCategoryTable(List<AgeCategory> ageCategoryTournaments)
        {
            var ageCategoryRepository = new AgeCategoryRepository
                {
                    OrgId = 365,
                    SportId = 16
                };

            
            List<AgeCategory> ageCategories= new List<AgeCategory>();
            foreach (AgeCategory ageCategoryTournament in ageCategoryTournaments)
            {
                AgeCategory ageCategory = new AgeCategory
                    {
                        CategoryId = ageCategoryTournament.CategoryId,
                        CategoryName = ageCategoryTournament.CategoryName,
                        SportId = 16,
                        SportName = "Fotball"
                    };

                ageCategory.MaxAge = ageCategoryTournament.MaxAge;
                
                ageCategory.MaxAge = ageCategoryTournament.MinAge;
                
                ageCategories.Add(ageCategory);
            }

            ageCategoryRepository.InsertAll(ageCategories);
        }

        public List<AgeCategory> GetAgeCategories(int sportId)
        {
            AgeCategoryRepository ageCategoryRepository = new AgeCategoryRepository
                {
                    OrgId = 1,
                    SportId = sportId
                };

            return new List<AgeCategory>(ageCategoryRepository.GetAll());
            
        }
        
        /// <summary>
        ///     Populating the Age Category Table with data from the NIF API
        /// </summary>
        public void PopulateNifAgeCategoryTable()
        {
            var listofsports = new List<Sport>(GetSports());

            // List<int> genders = SportFacade.Utilities.NIF.Utilities.GetGenders();

            // Only use sports except soccer
            foreach (Sport sport in listofsports.Where(x => x.OrgId != 365))
            {
                int orgId = 0;
                if (sport.OrgId != null)
                {
                    orgId = (int) sport.OrgId;
                }
        

                //// Deleting from database so we don't get duplicates
                //DeleteAgeCategoryTableBySportId(activity.ActivityId);

                //// Inserting into database
                //InsertAgeCategoryTable(ageCategories);
               
            }
        }

        public void DeleteAgeCategoryTableBySportId(int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {

                var sqlCommand = new SqlCommand("SportsData_DeleteAgeCategoryBySportId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                // Closing the connection after we have finished 
                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        /// <summary>
        ///     Doing the actual insert into the database
        /// </summary>
        /// <param name="ageCategories"></param>
        public void InsertAgeCategoryTable(List<AgeCategory> ageCategories)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                foreach (AgeCategory ageCategory in ageCategories)
                {
                    if (ageCategory == null)
                    {
                        continue;
                    }

                    if (ageCategory.SportId == 16)
                    {
                        continue;
                    }
                    
                    // Deleting the categories defined by the sport id. This so we don't get multiple (similar) data
                    
                    var sqlCommand = new SqlCommand("SportsData_InsertAgeCategory", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", ageCategory.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryID", ageCategory.CategoryId));
                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryName", ageCategory.CategoryName));
                    sqlCommand.Parameters.Add(new SqlParameter("@MinAge", ageCategory.MinAge));
                    sqlCommand.Parameters.Add(new SqlParameter("@MaxAge", ageCategory.MaxAge));
                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryDefinitionId", 1));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                }

                // Closing the connection after we have finished looping the dataset
                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        /// <summary>
        /// Method to get the selected Age Category
        /// </summary>
        /// <param name="id">Age category id</param>
        public AgeCategory GetAgeCategoryById(int id)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_GetAgeCategoryById", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", id));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (!sqlDataReader.HasRows)
                {
                    return null;
                }

                // Getting the value from the database
                AgeCategory ageCategory = new AgeCategory();
                while (sqlDataReader.Read())
                {
                    ageCategory.CategoryName = sqlDataReader["AgeCategoryName"].ToString();

                    if (sqlDataReader["SportId"] != DBNull.Value)
                    {
                        ageCategory.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                    }

                    if (sqlDataReader["AgeCategoryId"] != DBNull.Value)
                    {
                        ageCategory.CategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryId"]);
                    }

                    if (sqlDataReader["MinAge"] != DBNull.Value)
                    {
                        ageCategory.MinAge = Convert.ToInt32(sqlDataReader["MinAge"]);
                    }

                    if (sqlDataReader["MaxAge"] != DBNull.Value)
                    {
                        ageCategory.MaxAge = Convert.ToInt32(sqlDataReader["MaxAge"]);
                    }

                    if (sqlDataReader["AgeCategoryDefinitionId"] != DBNull.Value)
                    {
                        ageCategory.AgeCategoryDefinitionId = Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]);
                    }
                }
                

                // Closing the connection after we have finished looping the dataset
                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return ageCategory;
            }
        }

        public void UpdateAgeCategory(FormCollection collection)
        {
            int minAge = Convert.ToInt32(collection["minage"]);
            int maxAge = Convert.ToInt32(collection["maxage"]);
            int categoryId = Convert.ToInt32(collection["categoryId"]);
            int agecategoryDefinitionId = Convert.ToInt32(collection["ageCategoryDefinition"]);

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_UpdateAgeCategoryById", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", categoryId));
                sqlCommand.Parameters.Add(new SqlParameter("@MinAge", minAge));
                sqlCommand.Parameters.Add(new SqlParameter("@MaxAge", maxAge));
                sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryDefinitionId", agecategoryDefinitionId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                // Closing the connection after we have finished looping the dataset
                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        public List<AgeCategory> GetRemoteAgeCategoriesById()
        {
            if (SportId == 16)
            {
                var ageCategories = GetAgeCategories(SportId);

                if (ageCategories.Count == 0)
                {
                    var facade = new Facade.NFF.TournamentAccess.TournamentFacade();
                    return facade.GetAgeCategoriesTournament();
                }

                return ageCategories;
            }
            else
            {
                var ageCategories = GetAgeCategories(SportId);

                if (ageCategories.Count == 0)
                {
                    // Return the list of age categories
                    SportFacade facade = new SportFacade();
                    return facade.GetAgeCategories(FederationId);
                }

                return ageCategories;
            }
        }

        public void GetTournamentsBySportId(int sportId)
        {
            List<Tournament> tournaments = new List<Tournament>();
            if (sportId == 16)
            {
                var facade = new Facade.NFF.TournamentAccess.TournamentFacade();
                List<Municipality> municipalities = facade.GetMunicipalities();
                List<string> municipalityIds = municipalities.Select(municipality => municipality.MunicipalityId.ToString()).ToList();

                Season season = facade.GetOngoingSeason();

                tournaments = facade.GetTournamentByMunicipalities(municipalityIds, season.SeasonId);
            } else if (sportId != 16)
            {
                SportFacade facade = new SportFacade();
                List<Municipality> municipalities = facade.GetMunicipalities();
                var federationRepository = new FederationRepository();
                Federation federation = federationRepository.GetFederationBySportId(sportId);


                List<Season> seasons = facade.GetFederationSeasons(federation.FederationId);
                Season ongoingSeason = (from s in seasons
                                        where s.SeasonActive
                                        select s).Single();

                List<string> municipalityIds = municipalities.Select(municipality => municipality.MunicipalityId.ToString()).ToList();

                tournaments = facade.GetTournamentByMunicipalities(municipalityIds, ongoingSeason.SeasonId, federation.FederationId);
            }
            TournamentRepository tournamentRepository = new TournamentRepository();
            tournamentRepository.InsertAll(tournaments);
        }
    }
}