﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CalendarModel.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The calendar model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Models
{
    /// <summary>
    ///     The calendar model.
    /// </summary>
    public class CalendarModel
    {
        /// <summary>
        ///     The get calendar items.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>Dictionary</cref>
        ///     </see>
        ///     .
        /// </returns>
        public Dictionary<int, List<Calendar>> GetCalendarItems()
        {
            // Creating and populating the sports list which we need for the dropdown

            var sportRepository = new SportRepository();
            var listSports = new List<Sport>(sportRepository.GetAll());
            
            List<Calendar> listCalendar = new List<Calendar>();

            Dictionary<int, List<Calendar>> dictionary = new Dictionary<int, List<Calendar>>();
            CalendarRepository calendarRepository = new CalendarRepository();
            foreach (Sport sport in listSports)
            {
                calendarRepository.SportId = sport.Id;

                dictionary.Add(sport.Id, new List<Calendar>(calendarRepository.GetAll()));
            }

            return dictionary;
        }

        /// <summary>
        ///     The get calendar items.
        /// </summary>
        /// <param name="sportId">
        ///     The sport id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Calendar> GetCalendarItems(int sportId)
        {
            CalendarRepository repository = new CalendarRepository
                {
                    SportId = sportId
                };

            return new List<Calendar>(repository.GetAll());
        }
    }
}