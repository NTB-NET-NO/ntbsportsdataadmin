﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Facade.NFF.TournamentAccess;
using NTB.SportsDataAdmin.Facade.NIF.SportAccess;

namespace NTB.SportsDataAdmin.Application.Models
{
    public class AgeCategoryDefinitionModel
    {
        public DefineAgeCategoryView GetDefineAgeCategoryViewBySportId(int sportId)
        {
            AgeCategoryDefinitionRepository repository = new AgeCategoryDefinitionRepository();
            repository.GetAll();

            var seasonRepository = new SeasonRepository();
            var seasons = new List<Season>(seasonRepository.GetSeasonsBySportId(sportId));

            if (!seasons.Any())
            {
                if (sportId == 16)
                {
                    TournamentFacade facade = new TournamentFacade();
                    seasons = facade.GetSeasons();
                }
                else
                {
                    OrganizationRepository organizationRepository = new OrganizationRepository();
                    Organization organization = organizationRepository.GetOrganizationBySportId(sportId);
                    SportFacade facade = new SportFacade();
                    seasons = facade.GetFederationSeasons(organization.OrganizationId);
                }

            }

            var defineAgeCategoryView = new DefineAgeCategoryView
                {
                    AgeCategoryDefinitions = new List<AgeCategoryDefinition>(repository.GetAll()),
                    Seasons = seasons
                };

            return defineAgeCategoryView;
        }

        public List<AgeCategoryDefinition> GetAgeCategoryDefinitions()
        {
            var repository = new AgeCategoryDefinitionRepository();
            return repository.GetAll().ToList();
        }
    }
}