﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Facade.NIF.SportAccess;

namespace NTB.SportsDataAdmin.Application.Models
{
    public class FederationModel
    {
        private readonly FederationRemoteRepository _federationRemoteRepository;

        public FederationModel()
        {
            _federationRemoteRepository = new FederationRemoteRepository();
        }

        public FederationRemoteRepository FederationRemoteRepository
        {
            get { return _federationRemoteRepository; }
        }

        public void AddFederationDisciplines(string disciplineList, int orgId)
        {
            // Creating the Federation Data Access object
            var repository = new FederationDisciplinesRepository
                {
                    OrgId = orgId
                };

            repository.DeleteAll();

            // The disciplines comes in a comma-separated string
            string[] disciplines = disciplineList.Split(',');
            var federationDisciplines = new List<FederationDiscipline>();
            foreach (string discipline in disciplines)
            {
                string[] splittedDiscipline = discipline.Split('|');
                int disciplineId = Convert.ToInt32(splittedDiscipline[0]);
                string disciplineName = splittedDiscipline[1];

                var federationDiscipline = new FederationDiscipline
                    {
                        ActivityId = disciplineId,
                        ActivityName = disciplineName
                    };

                federationDisciplines.Add(federationDiscipline);
            }

            // Now we shall add these disciplines to the database
            repository.InsertAll(federationDisciplines);
        }

        public void InsertFederationAgeCategories(int id)
        {
            var facade = new SportFacade();
            
            var ageCategories = new List<AgeCategory>(facade.GetAgeCategories(id));
                
            var sportRepository = new SportRepository();
            int sportId = sportRepository.GetSportIdFromOrgId(id);

            var ageCategoryRepository = new AgeCategoryRepository
                {
                    SportId = sportId
                };

            ageCategoryRepository.DeleteAll(sportId);

            ageCategoryRepository.InsertAll(ageCategories);
        }

        public List<AgeCategory> GetFederationAgeCategories(int id)
        {
            var repository = new SportRepository();
            int sportId = repository.GetSportIdFromOrgId(id);

            var ageCategoryRepository = new AgeCategoryRepository();
            List<AgeCategory> federationAgeCategories = ageCategoryRepository.GetAgeCategoryBySportId(sportId).ToList();

            return federationAgeCategories;
        }

        public void UpdateFederationAgeCategories(int id, FormCollection collection)
        {
            string[] categoryName = collection["categoryName[]"].Split(',');
            string[] categoryId = collection["categoryId[]"].Split(',');
            string[] minage = collection["minage[]"].Split(',');
            string[] maxage = collection["maxage[]"].Split(',');
            string[] agecategoryId = collection["ageCategory[]"].Split(',');
            string[] sportId = collection["sportId[]"].Split(',');

            int items = sportId.Length;

            for (int i = 0; i < items; i++)
            {
                int intCategoryId = Convert.ToInt32(categoryId[i]);
                int intMinAge = Convert.ToInt32(minage[i]);
                int intMaxAge = Convert.ToInt32(maxage[i]);
                int intAgeCategoryDefinitionId = Convert.ToInt32(agecategoryId[i]);
                int intSportId = Convert.ToInt32(sportId[i]);
                string stringAgeCategoryName = categoryName[i];
                var ageCategory = new AgeCategory
                    {
                        CategoryId = intCategoryId,
                        CategoryName = stringAgeCategoryName,
                        MinAge = intMinAge,
                        MaxAge = intMaxAge,
                        AgeCategoryDefinitionId = intAgeCategoryDefinitionId,
                        SportId = intSportId
                    };
                var ageCategoryRepository = new AgeCategoryRepository
                    {
                        SportId = intSportId
                    };

                ageCategoryRepository.Update(ageCategory);
            }
        }

        public void DeleteFederationAgeCategories(int id)
        {
            var ageCategoryRepository = new AgeCategoryRepository();
            ageCategoryRepository.DeleteAll(id);
        }

        
    }
}