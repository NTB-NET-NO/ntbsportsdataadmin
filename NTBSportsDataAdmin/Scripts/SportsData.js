﻿
$(document).ready(function () {
    $(".passivejobsList").hide();
    $(".changelink").click(function () {

        var changeId = $(this).attr("id");
        var newId = changeId.replace("changejob", "");

        var formRowId = $("#FormRowId" + newId).show('slow');

        return false;
    });

    $(".deletelink").click(function () {

        var $link = $(this);

        var thisid = "";
        thisid = this.rel;
        console.log("thisid: " + thisid);
        console.log($link.attr("href"));
        console.log($link.attr("title"));

        $("#" + thisid).dialog({
            resizable: false,
            height: 200,
            modal: true,
            buttons: {
                'Delete ': function () {
                    location.href = $link.attr('href');
                    $(this).dialog('close');
                },
                Cancel: function () {
                    $(this).dialog('close');
                }
            }
        });
        console.log("Done!");
        return false;
    });

    $(".activatelink").click(function () {

        var $link = $(this);

        var thisid = "";
        thisid = this.rel;
        console.log("thisid: " + thisid);
        console.log($link.attr("href"));
        console.log($link.attr("title"));

        $("#" + thisid).dialog({
            resizable: false,
            height: 200,
            modal: true,
            buttons: {
                'Aktiver ': function () {
                    location.href = $link.attr('href');
                    $(this).dialog('close');
                },
                Cancel: function () {
                    $(this).dialog('close');
                }
            }
        });
        console.log("Done!");
        return false;
    });

    $("#hidepassive").click(function () {
        $(".passivejobsList").hide('Slow');

        return false;
    });

    $("#selectedTournaments").visible = true;


    $("a.hideAll").click(function () {
        if ($(this).text() == 'Skjul alle') {
            $("div.KommuneListe").slideUp();
            $(this).text("Vis alle");
        } else {
            $("div.KommuneListe").show("slow");
            $(this).text("Skjul alle");
        }

        return false;

    });
});
