﻿using System.Collections.Generic;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Interfaces
{
    interface ITournamentDataMapper
    {
        List<Tournament> GetTournamentsByJobId(int jobId);
        List<Tournament> GetTournamentsBySportId(int sportId);
        List<Tournament> GetTournamentsBySeasonId(int seasonId);
        void UpdateTournamentAgeCategoryDefinition(int tournamentId, int ageCategoryDefintionId);
    }
}
