﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Interfaces
{
    interface IMunicipalityDataMapper
    {
        List<Municipality> GetMunicipalitiesByJobId(int jobId);
        List<Municipality> GetMunicipalitiesBySportId(int sportId);
        void StoreMunicipalities(List<Municipality> municipalities);
        void DeleteAll();

    }
}
