﻿using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Interfaces
{
    interface IOrganizationDataMapper
    {
        Organization GetOrganizationBySportId(int sportId);
    }
}
