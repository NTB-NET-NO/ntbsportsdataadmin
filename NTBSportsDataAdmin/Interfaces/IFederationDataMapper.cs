﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Interfaces
{
    interface IFederationDataMapper
    {
        Federation GetFederationBySportId(int sportId);
    }
}
