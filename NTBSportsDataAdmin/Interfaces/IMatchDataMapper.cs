﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Interfaces
{
    interface IMatchDataMapper
    {
        List<Match> GetMatchesByTournamentId(int tournamentId);
    }
}
