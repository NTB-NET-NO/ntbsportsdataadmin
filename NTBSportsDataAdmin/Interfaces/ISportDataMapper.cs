﻿using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Interfaces
{
    interface ISportDataMapper
    {
        Sport GetSportIdBySeasonId(int seasonId);
        int GetSportIdFromOrgId(int orgId);
    }
}
