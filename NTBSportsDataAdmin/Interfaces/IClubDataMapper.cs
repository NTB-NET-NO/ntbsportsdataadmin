﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Interfaces
{
    interface IClubDataMapper
    {
        List<Club> GetClubsByOrgId(int orgId);
        List<Club> GetClubsByNameAndOrgId(string clubName, int orgId);
        List<Club> GetClubsByParentOrgId(int parentOrgId);
        List<Club> GetClubsByMunicipalities(List<string> selectedMunicipalities, int parentOrgId);
        void UpdateCheckedClub(Club club);
        void UpdateClubAdvanced(Club club);
    }
}
