﻿using System.Collections.Generic;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Interfaces
{
    interface ITournamentModel
    {
        List<Tournament> NormalizeTournaments(string selectedTournaments);
        void InsertTournament(Tournament tournament);
        void InsertSelectedTournaments(List<Tournament> selectedTournaments, int jobId, int ageCategoryId, int customerId, int sportId);
        List<Tournament> GetTournamentsByJobId(int jobId);
        List<Tournament> GetTournamentsBySportId(int sportId);
        Tournament GetTournament(int tournamentId);
        void DeleteTournament(int tournamentId);
        void DeleteTournamentByJobId(int jobId);
    }
}
