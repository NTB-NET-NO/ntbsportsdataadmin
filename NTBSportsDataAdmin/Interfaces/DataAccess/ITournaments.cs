﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITournaments.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The Tournaments interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System.Collections.Generic;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Interfaces.DataAccess
{
    /// <summary>
    /// The Tournaments interface.
    /// </summary>
    public interface ITournaments
    {
        /// <summary>
        /// The get tournaments for customer.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetTournamentsForCustomer();

        /// <summary>
        /// The get tournaments in job.
        /// </summary>
        /// <returns>
        /// The <see cref="SelectedTournament"/>.
        /// </returns>
        Tournament GetTournamentsInJob();

        /// <summary>
        /// The get tournaments for job.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetTournamentsForJob();
    }
}