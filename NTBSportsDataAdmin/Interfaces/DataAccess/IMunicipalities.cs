﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMunicipalities.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The Municipalities interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Interfaces.DataAccess
{
    /// <summary>
    /// The Municipalities interface.
    /// </summary>
    public interface IMunicipalities
    {
        /// <summary>
        /// Gets or sets the job id.
        /// </summary>
        int JobId { get; set; }

        /// <summary>
        /// The get municipalities by tournaments.
        /// </summary>
        /// <param name="storedTournaments">
        /// The stored tournaments.
        /// </param>
        /// <returns>
        /// The <see cref="NTB.SportsDataAdmin.Domain.Classes.Classes.DistrictsMunicipalitiesAndTournaments"/>.
        /// </returns>
        SelectionView GetMunicipalitiesByTournaments(List<int> storedTournaments);

        /// <summary>
        /// The get municipalities by sport.
        /// </summary>
        /// <returns>
        /// The <see cref="NTB.SportsDataAdmin.Domain.Classes.Classes.DistrictsMunicipalitiesAndTournaments"/>.
        /// </returns>
        SelectionView GetMunicipalitiesBySport();

        /// <summary>
        /// The get municipalities.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Municipality> GetMunicipalities();
    }
}