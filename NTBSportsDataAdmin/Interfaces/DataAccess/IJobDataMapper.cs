﻿using System.Collections.Generic;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Interfaces.DataAccess
{
    interface IJobDataMapper
    {
        void UpdateCustomerJobsWithAgeCategory(int jobId, int ageCategory, int sportId);
        JobsModels CreateCustomerJobs(int customerId);
        List<int> GetJobsMunicipalities(int jobId);
        List<AgeCategoryDefinition> GetAgeCategoryDefinitions(int sportsId);
        void SetCustomerJob(int jobId, int customerId, int sportId, int ageCategoryId, List<Tournament> tournaments);
        bool ChangeCustomerJobStatus(int jobId);
        List<Job> ShowJobs(int customerId);
        CustomerJob GetCustomerJobByJobId(int jobId);
    }
}
