﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Interfaces
{
    interface ISeasonDataMapper
    {
        List<Season> GetSeasonsBySportId(int sportId);
        void ActivateSeason(int seasonId);
        void DeActivateSeason(int seasonId);
        void UpdateSeasonWithDiscipline(int seasonId, int disciplineId);
    }
}
