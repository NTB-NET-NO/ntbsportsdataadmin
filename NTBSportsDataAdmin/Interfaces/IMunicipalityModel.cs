﻿using System.Collections.Generic;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Interfaces
{
    interface IMunicipalityModel
    {
        void InsertMunicipalities(List<Municipality> municipalities, int jobid);
        List<Municipality> GetMunicipalities();
        List<Municipality> GetSelectedMunicipalities(string selectedMunicipalities, int jobId);
    }
}
