﻿using System;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsDataAdmin.Application.Interfaces;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Fakes
{
    public class MunicipalityRepositoryFake : IRepository<Municipality>, IDisposable
    {
        public List<Municipality> GetList { get; set; }
        public List<Municipality> SaveList { get; set; } 

        public MunicipalityRepositoryFake()
        {
            SaveList = new List<Municipality>();
        }

        public int InsertOne(Municipality domainobject)
        {
            SaveList.Add(domainobject);

            return 1;
        }

        public void InsertAll(List<Municipality> domainobject)
        {
            foreach (Municipality region in domainobject)
            {
                SaveList.Add(region);
            }
        }

        public void Update(Municipality domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Municipality domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Municipality> Search()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Municipality> GetAll()
        {
            return GetList.AsQueryable();
        }

        public Municipality Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}