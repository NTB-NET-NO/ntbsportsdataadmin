// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Tournaments.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The tournaments.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web.Mvc;
using System.Xml;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Facade.NFF.TournamentAccess;
using log4net;

namespace NTB.SportsDataAdmin.Application.Utilities
{
    // Adding supprt for log4net

    // Adding reference to the NFF services
    // using NFFTournamentService;

    /// <summary>
    /// The tournaments.
    /// </summary>
    public class Tournaments
    {
        // GetTeamsByMunicipality 

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Tournaments));

        /// <summary>
        /// The _instance.
        /// </summary>
        private static Tournaments _instance = new Tournaments();

        /// <summary>
        /// The _my sports utils.
        /// </summary>
        // private readonly SportsUtils _sportsUtils = new SportsUtils();

        private readonly TournamentFacade _tournamentFacade = new TournamentFacade();

        /// <summary>
        /// Initializes a new instance of the <see cref="Tournaments"/> class. 
        /// Prevents a default instance of the <see cref="Tournaments"/> class from being created.
        /// </summary>
        public Tournaments()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }

            // Setting up different needed remote data sets

            // Districts
            var districts = _tournamentFacade.GetDistricts();

            // Getting municipality
            var municipalities = _tournamentFacade.GetMunicipalities();

            // Getting seasons
            SeasonId = _tournamentFacade.GetOngoingSeason().SeasonId;    
        }
        
        /// <summary>
        /// Gets the instance.
        /// </summary>
        public static Tournaments Instance
        {
            get
            {
                return _instance ?? (_instance = new Tournaments());
            }
        }

        /// <summary>
        /// Gets or sets the job id.
        /// </summary>
        public int JobId { get; set; }
        
        /// <summary>
        /// Gets or sets the season id.
        /// </summary>
        public int SeasonId { get; set; }

        
        /// <summary>
        /// The validate server certificate.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="certificate">
        /// The certificate.
        /// </param>
        /// <param name="chain">
        /// The chain.
        /// </param>
        /// <param name="sslPolicyErrors">
        /// The ssl policy errors.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ValidateServerCertificate(
            object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        /// <summary>
        /// Method to return the Tournaments that are connected to the MunicipalitiesDataAccess
        /// </summary>
        /// <param name="selectedMunicipalities">
        /// string of municipalities separated by the character | (pipe)
        /// </param>
        /// <param name="ageCategoryId">
        /// int of age category id
        /// </param>
        /// <param name="seasonId">
        /// The Id for this season
        /// </param>
        /// <param name="jobId">
        /// The job Id.
        /// </param>
        /// <returns>
        /// List containing JobTournament Object
        /// </returns>
        public List<Tournament> GetTournamentByMunicipalities(
            List<Municipality> selectedMunicipalities, int ageCategoryId, int seasonId, int jobId)
        {
            // Setting up the tournament array
            Logger.Debug("selectedMunicipalities");

            // Creating a JobTournament List
            List<Tournament> listJobsTournament = new List<Tournament>();

            // Looping the string array
            Dictionary<int, List<Team>> teamInstances = new Dictionary<int, List<Team>>();
            //MunicipalitiesDataAccess municipalitiesDataAccess = new MunicipalitiesDataAccess();
            //municipalitiesDataAccess.DeleteMunicipalities(jobId);
            foreach (Municipality municipality in selectedMunicipalities)
            {
                int municipalityId = municipality.MunicipalityId;

                // Getting the teams from the municipalities and find out in which tournaments they are participating in...

                // TODO: fix so that it gets the list from the service and then check if this is in the database for this customer.
                // If it is for the customer, we shall mark it as selected.
                if (municipalityId <= 0)
                {
                    continue;
                }

                try
                {
                    // I also need to store the municipalities in the database
                    // Insert into municipality has been moved to the controller, as we do everything there
                    // Consider - we are looping there, and we are looping here. What is best practice?
                    //            could this be a speed issue?

                    // municipalitiesDataAccess.InsertMunicipality(municipalityId, jobId);
                    TournamentFacade facade = new TournamentFacade();
                    var teamResult = facade.GetTeamsByMunicipality(municipalityId, SeasonId);

                    teamInstances.Add(municipalityId, teamResult);

                    // municipalityTeams.AddRange(teams);
                    Logger.Debug("Season Id: " + seasonId.ToString());

                    // Looping the team object
                    Logger.Debug("Number of teams: " + teamResult.Count);
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
            }

            DateTime timerStart = DateTime.Now;
            Logger.Debug("Starting team getting: " + timerStart.ToShortTimeString());
            foreach (KeyValuePair<int, List<Team>> kvp in teamInstances)
            {
                List<Tournament> listTournaments = new List<Tournament>();
                foreach (Team team in kvp.Value)
                {
                    try
                    {
                        // Logging this
                        Logger.Debug("Team ID: " + team.TeamId + ", SeasonId: " + seasonId);
                        Logger.Debug("Club ID: " + team.ClubId);

                        // Now getting the tournament by team object
                        // tournaments = mySportsUtils.tclient.GetTournamentsByTeam(team.TeamId, seasonId, null);
                        DateTime timerTournament = DateTime.Now;
                        Logger.Debug("Before GetTournamentsByTeam: " + timerTournament.ToShortTimeString());

                        TournamentFacade facade = new TournamentFacade();
                        List<Tournament> tournaments = facade.GetTournamentsByTeam(team.TeamId, seasonId);

                        
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);
                    }

                    // Looping throught the tournaments that we found
                    if (listTournaments.Any())
                    {
                        List<Tournament> filteredTournaments;
                        if (ageCategoryId == 2)
                        {
                            filteredTournaments = (from myTournaments in listTournaments
                                                  where
                                                      (myTournaments.AgeCategoryId == 18)
                                                      || (myTournaments.AgeCategoryId == 21)
                                                  orderby myTournaments.Division
                                                  select myTournaments).ToList();
                        }
                        else
                        {
                            filteredTournaments = (from myTournaments in listTournaments
                                                  where
                                                      !(myTournaments.AgeCategoryId == 18
                                                        || myTournaments.AgeCategoryId == 21)
                                                  orderby myTournaments.Division
                                                  select myTournaments).ToList();
                        }

                        // KeyValuePair<int, NTB.Classes.Team> kvp1 = kvp;
                        KeyValuePair<int, List<Team>> kvp1 = kvp;
                        foreach (Tournament selectedTournament in from tournament in filteredTournaments
                                    select
                                        new Tournament
                                            {
                                                DistrictId = tournament.DistrictId, 
                                                SeasonId = tournament.SeasonId, 
                                                TournamentId = tournament.TournamentId, 
                                                TournamentName = tournament.TournamentName, 
                                                AgeCategoryId = tournament.AgeCategoryId, 
                                                AgeCategoryFilterId = ageCategoryId, 
                                                MunicipalityId = kvp1.Key, 
                                                TournamentNumber = tournament.TournamentNumber
                                            }
                                                into myJobTournament
                                                let hasTournament =
                                                    (from myTournament in listJobsTournament
                                                    where
                                                        myTournament.TournamentName
                                                        == myJobTournament.TournamentName
                                                    select myTournament).Any()
                                                where hasTournament == false
                                                select myJobTournament)
                        {
                            listJobsTournament.Add(selectedTournament);
                        }
                    }
                }
            }

            DateTime timerEnd = DateTime.Now;
            TimeSpan totalDifference = timerEnd - timerStart;

            Logger.Debug("Total time of producing tournament list: " + totalDifference.TotalSeconds);

            return listJobsTournament;
        }

        /// <summary>
        /// The create tournament file.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        public void CreateTournamentFile(int id, FormCollection collection)
        {
            int customerId = id;
            string startDate = DateTime.Today.ToShortDateString();
            string endDate = DateTime.Today.ToShortDateString();
            int foreignCustomerId = 0;

            if (collection["ttcustomerid"] != string.Empty)
            {
                foreignCustomerId = Convert.ToInt32(collection["ttcustomerid"]);
            }

            if (collection["startDate"] != string.Empty)
            {
                startDate = Convert.ToDateTime(collection["startDate"]).ToShortDateString();
            }

            if (collection["endDate"] != string.Empty)
            {
                endDate = Convert.ToDateTime(collection["endDate"]).ToShortDateString();
            }

            if (collection["tournament[]"] == null)
            {
                return;
            }
            string[] selectedTournaments = collection["tournament[]"].Split(',').Distinct().ToArray();
            
            XmlDocument xmlDocument = new XmlDocument();
            XmlDeclaration dec = xmlDocument.CreateXmlDeclaration("1.0", null, null);
            xmlDocument.AppendChild(dec);

            XmlElement xmlRoot = xmlDocument.CreateElement("SportsData");
            XmlElement xmlRootMeta = xmlDocument.CreateElement("SportsData-Meta");
            xmlRootMeta.SetAttribute("type", "customer");

            // We should consider creating a form where we can decide start and end of getting match data
            xmlRootMeta.SetAttribute("startdate", DateTime.Today.ToShortDateString());
            xmlRootMeta.SetAttribute("enddate", DateTime.Today.ToShortDateString());
            xmlRoot.AppendChild(xmlRootMeta);

            // Creating the Customer Element <Customer id="30"> 
            // Done: Should also have the TT-customerId
            XmlElement xmlCustomer = xmlDocument.CreateElement("Customer");
            xmlCustomer.SetAttribute("id", id.ToString());
            xmlCustomer.SetAttribute("foreigncustomerid", foreignCustomerId.ToString());
            xmlRoot.AppendChild(xmlCustomer);

            XmlElement xmlJob = xmlDocument.CreateElement("Job");
            xmlJob.SetAttribute("startdate", startDate);
            xmlJob.SetAttribute("enddate", endDate);
            xmlCustomer.AppendChild(xmlJob);

            XmlElement xmlTournaments = xmlDocument.CreateElement("Tournaments");

            foreach (string tournament in selectedTournaments)
            {
                
                string[] splittedTournamentString = tournament.Split('S');

                // We need to find out discipline for this tournament
                var tournamentRepository = new TournamentRepository();
                var foundTournament = tournamentRepository.Get(Convert.ToInt32(splittedTournamentString[0].Replace("T", string.Empty)));

                var seasonRepositor = new SeasonRepository();
                var disciplineId = seasonRepositor.Get(foundTournament.SeasonId).FederationDiscipline;
                

                XmlElement xmlTournament = xmlDocument.CreateElement("Tournament");
                xmlTournament.SetAttribute("id", splittedTournamentString[0].Replace("T", string.Empty));
                xmlTournament.SetAttribute("sportId", splittedTournamentString[1]);
                xmlTournament.SetAttribute("disciplineId", disciplineId.ToString());
                xmlTournaments.AppendChild(xmlTournament);
            }

            xmlCustomer.AppendChild(xmlTournaments);
            
            xmlDocument.AppendChild(xmlRoot);

            // Getting path from Config-file
            string outputFolder = ConfigurationManager.AppSettings["CustomerTriggerFolder"];

            // Checking that the folder exists (in case I forget when I put the system into production)
            DirectoryInfo directoryInfo = new DirectoryInfo(outputFolder);
            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }

            string todayDate = DateTime.Today.Year.ToString().PadLeft(2, '0')
                               + DateTime.Today.Month.ToString().PadLeft(2, '0')
                               + DateTime.Today.Day.ToString().PadLeft(2, '0');

            string todayTime = DateTime.Now.Hour.ToString().PadLeft(2, '0')
                               + DateTime.Now.Minute.ToString().PadLeft(2, '0')
                               + DateTime.Now.Second.ToString().PadLeft(2, '0');

            string filename = "NTB_SportsData_" 
                + customerId + "_" 
                + foreignCustomerId + "_"
                + todayDate 
                + "T"
                + todayTime 
                + ".xml";

            xmlDocument.Save(outputFolder + @"\" + filename);
        }
        
        #region Certificate

        /// <summary>
        /// The trust all certificate policy.
        /// </summary>
        public class TrustAllCertificatePolicy : ICertificatePolicy
        {
            /// <summary>
            /// The check validation result.
            /// </summary>
            /// <param name="sp">
            /// The sp.
            /// </param>
            /// <param name="cert">
            /// The cert.
            /// </param>
            /// <param name="req">
            /// The req.
            /// </param>
            /// <param name="problem">
            /// The problem.
            /// </param>
            /// <returns>
            /// The <see cref="bool"/>.
            /// </returns>
            public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
            {
                return true;
            }
        }

        #endregion
    }
}