﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CalendarController.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The calendar controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using NTB.SportsDataAdmin.Application.Models;
using System.Web.Mvc;

namespace NTB.SportsDataAdmin.Application.Controllers
{
    /// <summary>
    /// The calendar controller.
    /// </summary>
    public class CalendarController : Controller
    {
        // GET: /Calendar/

        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index()
        {
            var model = new SportModel();
            return View("Index", model.GetSports());
            
        }

        [HttpPost]
        public ActionResult GetEvents(FormCollection collection)
        {
            var sportId = 0;
            if (collection["sports"] != null)
            {
                sportId = Convert.ToInt32(collection["sports"]);
            }

            var model = new CalendarModel();
            return View("ListEvents", model.GetCalendarItems(sportId));
        }

        // GET: /Calendar/Details/5
        /// <summary>
        /// The get sport by id.
        /// </summary>
        /// <param name="id">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult GetSportById(int id)
        {
            CalendarModel calendarModel = new CalendarModel();
            return PartialView("GetSelectedSportsCalendar", calendarModel.GetCalendarItems(id));
        }

        /// <summary>
        /// The details.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: /Calendar/Create

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Calendar/Create

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: /Calendar/Edit/5

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: /Calendar/Edit/5

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: /Calendar/Delete/5

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: /Calendar/Delete/5

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}