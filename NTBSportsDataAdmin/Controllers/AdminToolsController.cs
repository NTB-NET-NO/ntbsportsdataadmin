﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdminToolsController.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The admin tools controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Linq;
using NTB.SportsDataAdmin.Application.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Controllers
{
    /// <summary>
    /// The admin tools controller.
    /// </summary>
    public class AdminToolsController : Controller
    {
        /// <summary>
        /// The _tools models.
        /// </summary>
        private ToolsModels _toolsModels = new ToolsModels();

        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// The update sports table.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult UpdateSportsTable()
        {
            return View(_toolsModels.UpdateSportsTable());
        }

        /// <summary>
        /// The list sports.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ListSports()
        {
            _toolsModels = new ToolsModels();

            return View(_toolsModels.ListSports());
        }

        /// <summary>
        /// The delete sport.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult DeleteSport(int id)
        {
            _toolsModels = new ToolsModels();
            _toolsModels.DeleteSport(id);
            return View("ListSports", _toolsModels.ListSports());
        }

        /// <summary>
        /// The get new tournaments.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult GetTournaments(int id)
        {
            var toolsModels = new ToolsModels();
            toolsModels.GetTournamentsBySportId(id);
            return View("ListSports", _toolsModels.ListSports());
        }

        /// <summary>
        /// The check tournament.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult CheckTournament()
        {
            return View();
        }

        /// <summary>
        /// The check tournament.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult CheckTournament(FormCollection collection)
        {
            int tournamentId = 0;
            foreach (var key in collection.AllKeys)
            {
                tournamentId = Convert.ToInt32(collection[key]);
            }

            var toolsModel = new ToolsModels();
            if (tournamentId > 0)
            {
                Tournament tournament = toolsModel.GetTournament(tournamentId);

                if (tournament != null)
                {
                    return View("TournamentInfo", tournament);
                }
            }

            return View();
        }

        /// <summary>
        /// The update current tournaments.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult UpdateCurrentTournaments()
        {
            var toolsModel = new ToolsModels();
            toolsModel.UpdateTournaments();

            return View("Index");
        }

        /// <summary>
        /// The vedlikehold.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Vedlikehold()
        {
            var toolsModel = new ToolsModels();

            return View(toolsModel.GetSeasons());
        }

        /// <summary>
        /// The update tournaments.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult UpdateTournaments(FormCollection collection)
        {
            int sportId = Convert.ToInt32(collection["sportId"]);
            var toolsModels = new ToolsModels
                {
                    SportId = sportId
                };
            
            toolsModels.UpdateTournamentsBySeason(collection);

            return RedirectToAction("index");
        }

        /// <summary>
        /// The create municipality table.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult CreateMunicipalityTable()
        {
            var toolsModels = new ToolsModels();
            
            toolsModels.CreateCountyTable();

            toolsModels.CreateRegionsTable();

            return View("Index");
        }

        /// <summary>
        /// Updating the database after doing necesarry changes 
        /// </summary>
        /// <param name="collection">Form collection</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditSelectedAgeCategory(FormCollection collection)
        {
            var toolsModels = new ToolsModels();
            toolsModels.UpdateAgeCategory(collection);

            const int sportId = 16;
            return View("ChangeAgeCategory", toolsModels.GetAgeCategories(sportId));

            //ToolsModels toolsModels = new ToolsModels();
            //return View("EditSelectedAgeCategory", toolsModels.GetAgeCategoryById(id));
        }

        /// <summary>
        /// Editing one selected Age Category
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult EditSelectedAgeCategory(int id)
        {
            var toolsModels = new ToolsModels();
            return View("EditSelectedAgeCategory", toolsModels.GetAgeCategoryById(id));
        }

        /// <summary>
        /// Creating the Age Category Table
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateAgeCategoryTable()
        {
            var toolsModels = new ToolsModels();
            toolsModels.PopulateNffAgeCategoryTable();
            toolsModels.PopulateNifAgeCategoryTable();

            return View("ChangeAgeCategory");
        }

        public ActionResult ChangeAgeCategory()
        {
            var toolsModels = new ToolsModels();

            return View("ChangeAgeCategory", toolsModels.GetAgeCategories(16));
        }

        public ActionResult SelectAgeCategorySport()
        {
            return View("SelectAgeCategorySport", _toolsModels.GetSports());
        }

        [HttpPost]
        public ActionResult GetAgeCategoriesForSport(FormCollection collection)
        {
            
            string[] formCollection = collection["sport"].Split('|');

            int sportId = Convert.ToInt32(formCollection[0]);
            int federationId = Convert.ToInt32(formCollection[1]);
            var toolsModels = new ToolsModels
                {
                    FederationId = federationId,
                    SportId = sportId
                };
            return View("GetAgeCategoriesForSport", toolsModels.GetRemoteAgeCategoriesById());
        }

        /// <summary>
        /// The create customer xml.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult CreateCustomerXml()
        {
            var customerModel = new CustomerRepository();
            List<Customer> customers = customerModel.GetAllCustomers();
            var adminToolModels = new AdminToolModel();
            adminToolModels.CreateCustomersXml(customers);
            return View("Index");
        }

        public ActionResult GetSeasons(int id)
        {
            var sportDataMapper = new SportRepository();
            var sportId = sportDataMapper.GetSportIdFromOrgId(id);

            var model = new SeasonModel();
            var seasons = new List<Season>(model.GetSeasonsBySportId(sportId));

            
            var remoteRepository = new FederationRemoteRepository();
            var orgId = id;
            List<FederationDiscipline> federationDisciplines = remoteRepository.GetFederationDisciplines(orgId);

            model.InsertSeasons(id, seasons);

            var view = new SeasonView
                {
                    SportId = id,
                    FederationDisciplines = federationDisciplines,
                    Seasons = seasons
                };
            return View(view);
        }

        [HttpPost]
        public ActionResult SetSeasonsDisciplines(int id, FormCollection collection)
        {
            string[] disciplines = collection["discipline[]"].Split(',');
            string[] seasonIds = collection["seasonid[]"].Split(',');

            int size = disciplines.Length;

            var seasonDisciplines = new Dictionary<int, int>();
            for (int i = 0; i < size; i++)
            {
                if (disciplines[i].Trim() == string.Empty)
                {
                    continue;
                }

                seasonDisciplines.Add(Convert.ToInt32(seasonIds[i]), Convert.ToInt32(disciplines[i]));
            }

            var model = new SeasonModel();
            model.SetSeasonDisciplines(seasonDisciplines);

            // Remember to change this
            return RedirectToAction("GetSeasons", new {id = id });
        }

        [HttpPost]
        public ActionResult EditSeason(int id, FormCollection collection)
        {
            var model = new SeasonModel();
            model.UpdateSeason(id, collection);
            int sportId = Convert.ToInt32(collection["SportId"]);
            return RedirectToAction("GetSeasons", new {id = sportId });
        }

        public ActionResult EditSeason(int id)
        {
            var model = new SeasonModel();
            return View(model.GetSeasonBySeasonId(id));
        }

        public ActionResult DeleteSeason(int id)
        {
            var model = new SeasonModel();
            var sportModel = new SportModel();

            Sport sport = sportModel.GetSportIdBySeasonId(id);
            
            // Now we delete the season
            model.DeleteSeason(new Season
                {
                    SeasonId = id
                });

            return RedirectToAction("GetSeasons", model.GetSeasonsBySportId(sport.Id));
        }
        public ActionResult GetFederations()
        {
            var model = new FederationModel();
            return View(model.FederationRemoteRepository.GetFederations());
        }

        public ActionResult ViewFederation(int id)
        {
            ViewBag.OrgId = id;
            var model = new FederationModel();
            return View(model.FederationRemoteRepository.GetFederation(id));
        }

        [HttpPost]
        public ActionResult AddFederationDisciplines(int id, FormCollection collection)
        {
            var federationModel = new FederationModel();
            federationModel.AddFederationDisciplines(collection["disciplines[]"], id);
            return RedirectToAction("GetFederations");
        }

        public ActionResult AddAgeCategories(int id)
        {
            var model = new FederationModel();
            model.InsertFederationAgeCategories(id);
            ViewBag.OrgId = id;
            return RedirectToAction("EditAgeCategories", "AdminTools", new {id = id});
        }

        public ActionResult EditAgeCategories(int id)
        {
            var model = new FederationModel();
            ViewBag.OrgId = id;
            return View(model.GetFederationAgeCategories(id));
        }

        [HttpPost]
        public ActionResult EditAgeCategories(int id, FormCollection collection)
        {
            var model = new FederationModel();

            model.UpdateFederationAgeCategories(id, collection);
            return RedirectToAction("GetFederations");
        }

        public ActionResult ActivateSeason(int id)
        {
            var model = new SeasonModel();
            model.ActivateSeason(id);
            var sportModel = new SportModel();

            Sport sport = sportModel.GetSportIdBySeasonId(id);

            return RedirectToAction("GetSeasons", new {id = sport.OrgId});
        }

        public ActionResult DeActivateSeason(int id)
        {
            var model = new SeasonModel();
            model.DeActivateSeason(id);
            var sportModel = new SportModel();

            Sport sport = sportModel.GetSportIdBySeasonId(id);

            return RedirectToAction("GetSeasons", new {id = sport.OrgId});
        }

        public ActionResult Tournaments()
        {
            var model = new SportModel();
            return View(model.GetSports());
        }

        public ActionResult GetNewTournaments(int id)
        {
            
            var repository = new SeasonRepository();
            return View("GetNewTournaments", repository.GetSeasonsBySportId(id));
        }

        [HttpPost]
        public ActionResult GetTournamentSeasons(FormCollection collection)
        {
            int sportId = 0;
            if (collection["sport"] != null)
            {
                sportId = Convert.ToInt32(collection["sport"]);
            }

            if (sportId == 0)
            {
                return RedirectToAction("Tournaments");
            }

            Session["SportId"] = sportId;

            var model = new SeasonModel();
            
            return View(model.GetSeasonsBySportId(sportId).ToList());
        }

        [HttpPost]
        public ActionResult GetTournaments(FormCollection collection)
        {
            int seasonId = 0;
            if (collection["seasons"] != null)
            {
                seasonId = Convert.ToInt32(collection["seasons"]);
            }

            // We should get only tournaments for this season
            
            var model = new TournamentModel();
            var ageCategoryDefinitionModel = new AgeCategoryDefinitionModel();
            var ageCategoryDefinitions = ageCategoryDefinitionModel.GetAgeCategoryDefinitions();

            var tournaments = model.GetTournamentsBySeasonId(seasonId);

            foreach (Tournament tournament in tournaments)
            {
                if (tournament.AgeCategoryDefinitionId == 1)
                {
                    if (tournament.TournamentName.ToLower().Contains("kvinner") ||
                        tournament.TournamentName.ToLower().Contains("menn")
                        )
                    {
                        tournament.AgeCategoryDefinitionId = 2;
                    }
                    else
                    {
                        tournament.AgeCategoryDefinitionId = 1;
                    }
                }
            }

            var sportId = Convert.ToInt32(Session["SportId"]);
            var tournamentListViewModel = new TournamentListViewModel
                {
                    SportId = sportId,
                    AgeCategoryDefinitions = ageCategoryDefinitions,
                    Tournaments = tournaments
                };
            return View("ListTournaments", tournamentListViewModel);
        }

        public ActionResult ActivateSport(int id)
        {
            var model = new SportModel();
            model.Activate(id);
            return RedirectToAction("GetFederations");
        }

        public ActionResult GetMatches(int id)
        {
            var model = new MatchModel();
            int sportId = Convert.ToInt32(Request.Params["sportId"]);
            return View("ListMatches", model.GetMatches(id, sportId));
        }

        [HttpPost]
        public ActionResult UpdateTournamentsWithAgeCategory(FormCollection collection)
        {
            int sportId = 0;
            if (collection["sport"] != null)
            {
                sportId = Convert.ToInt32(collection["sport"]);
            }

            string[] ageCategoryDefinitionValues = collection["alderskategori[]"].Split(',');
            string[] tournamentIds = collection["tournamentId[]"].Split(',');

            int size = ageCategoryDefinitionValues.Length;

            var ageCategoryDefintionDictionary = new Dictionary<int, int>();
            for (int i = 0; i < size; i++)
            {
                if (ageCategoryDefinitionValues[i].Trim() == string.Empty)
                {
                    continue;
                }

                ageCategoryDefintionDictionary.Add(Convert.ToInt32(tournamentIds[i]), Convert.ToInt32(ageCategoryDefinitionValues[i]));
            }


            var model = new TournamentModel();
            model.UpdateTournamentAgeCategoryDefinitition(ageCategoryDefintionDictionary);
                
            var ageCategoryDefinitionModel = new AgeCategoryDefinitionModel();
            var ageCategoryDefinitions = ageCategoryDefinitionModel.GetAgeCategoryDefinitions();

            var tournamentListViewModel = new TournamentListViewModel
            {
                SportId = sportId,
                AgeCategoryDefinitions = ageCategoryDefinitions,
                Tournaments = model.GetTournamentsBySportId(sportId)
            };
            return View("ListTournaments", tournamentListViewModel);
        }
    }
}