﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerController.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Linq;
using NTB.SportsDataAdmin.Application.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Application.Utilities;
using NTB.SportsDataAdmin.Domain.Classes;
using log4net;

namespace NTB.SportsDataAdmin.Application.Controllers
{
    /// <summary>
    /// The customer controller.
    /// </summary>
    public class CustomerController : Controller
    {
        // GET: /Customer/

        /// <summary>
        /// The _customers data access.
        /// </summary>
        private readonly CustomerRepository _customersDataAccess = new CustomerRepository();
        
        /// <summary>
        /// The _log.
        /// </summary>
        private readonly ILog _log = LogManager.GetLogger(typeof(CustomerController));

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index()
        {
            _log.Debug("ActionResult index()");
            return View(_customersDataAccess.GetAllCustomers());
        }

        // GET: /Customer/Details/5

        /// <summary>
        /// The details.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Details(int id)
        {
            Session["CustomerId"] = id;

            DetailsViewModel model = new DetailsViewModel();
            DetailsView view = model.GetDetails(id);
            
            return View(view);
        }

        // GET: /Customer/Create

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Create()
        {
            return View(new Customer());
        }

        // POST: /Customer/Create

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                Customer customer = new Customer();

                TryUpdateModel(customer);

                if (ModelState.IsValid)
                {
                    _customersDataAccess.AddOneCustomer(customer);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: /Customer/Edit/5

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Edit(int id)
        {
            return View(_customersDataAccess.GetOneCustomer(id));
        }

        // POST: /Customer/Edit/5

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                Customer customer = new Customer();

                TryUpdateModel(customer);

                if (ModelState.IsValid)
                {
                    _customersDataAccess.EditOneCustomer(customer, id);
                }

                Session.Add("CustomerId", id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: /Customer/Delete/5

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Delete(int id)
        {
            // Deactivating the customer
            var model = new CustomerRepository();
            model.DeactivateCustomer(id);

            Session.Add("CustomerId", id);

            return RedirectToAction("Index");

            // return View();
        }

        /// <summary>
        /// The activate.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Activate(int id)
        {
            // Deactivating the customer
            var model = new CustomerRepository();
            model.ActivateCustomer(id);

            Session.Add("CustomerId", id);

            return RedirectToAction("Index");

            // return View();
        }

        // POST: /Customer/Delete/5

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        /// <summary>
        /// The get selected items.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="selectedItems">
        /// The selected items.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult GetSelectedItems(int id, string selectedItems, int seasonId, FormCollection collection)
        {
            int customerId = Convert.ToInt32(Session["CustomerId"]);

            _log.Debug("selectedItems: " + selectedItems);
            _log.Debug("SportsId: " + Session["SportId"]);
            _log.Debug("JobId from form: " + collection["JobId"]);
            _log.Debug("AgeCategoryFilterId: " + collection["AgeCategoryId"]);
            _log.Debug("Id: " + id);
            _log.Debug("CustomerId: " + customerId);

            if (Convert.ToInt32(Session["CustomerId"]) == 0 && id > 0)
            {
                Session.Add("CustomerId", id);
            }



            var sportId = Convert.ToInt32(Session["SportId"]);
            int federationId = Convert.ToInt32(Session["FederationId"]);

            int ageCategoryId = Convert.ToInt32(collection["AgeCategoryId"]);
            int jobId = Convert.ToInt32(collection["Jobid"]);

            var municipalityModel = new MunicipalityModel();
            List<Municipality> selectedMunicipalities = municipalityModel.GetSelectedMunicipalities(collection["selectedItems"], jobId);

            // We are storing the selection (delete first, then insert)
            municipalityModel.InsertMunicipalities(selectedMunicipalities, jobId);

            var tournamentModel = new TournamentModel();

            ViewBag.SportId = sportId;
            ViewBag.JobId = jobId;
            ViewBag.AgeCategoryId = ageCategoryId;
            ViewBag.SeasonId = seasonId;
            ViewBag.CustomerId = id;
            return PartialView("GetSelectedItems", tournamentModel.GetTournamentsForView(selectedMunicipalities, jobId, ageCategoryId, federationId, sportId, seasonId));
        }

        /// <summary>
        /// The get age category definitions.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <param name="federationId">
        /// The federation id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult GetAgeCategoryDefinitions(int sportId, int jobId, int customerId, int federationId)
        {
            ViewBag.JobId = jobId;
            ViewBag.CustomerId = customerId;
            ViewBag.SportId = sportId;
            SportId = sportId;

            Session.Add("CustomerId", customerId);
            Session.Add("FederationId", federationId);
            Session.Add("SportId", sportId);



            AgeCategoryDefinitionModel model = new AgeCategoryDefinitionModel();

            return PartialView("getAgeCategoryDefinitions", model.GetDefineAgeCategoryViewBySportId(sportId));
        }

        /// <summary>
        /// The get job name.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult GetJobName(int id)
        {
            int jobId = Convert.ToInt32(Request.QueryString["JobId"]);
            int ageCategoryId = Convert.ToInt32(Request.QueryString["AgeCategoryId"]);
            JobRepository jobs = new JobRepository();
            ViewBag.JobId = jobId;
            ViewBag.CustomerId = id;
            ViewBag.AgeCategoryId = ageCategoryId;

            if (Session["CustomerId"].ToString() == string.Empty)
            {
                Session.Add("CustomerId", id);
            }

            return PartialView("DefineJob", jobs.GetCustomerJobByJobId(jobId));
        }

        [HttpPost]
        public ActionResult GetMunicipalities(int id, FormCollection collection)
        {
            var model = new SelectionViewModel();
            int sportId = 0;

            if ((collection["sportId"]) != null)
            {

                if (int.TryParse(collection["sportId"], out sportId))
                {
                    
                }
            }

            // We could try and move this into the if
            if (Session["SportId"] != null)
            {
                if (sportId > 0 && sportId > Convert.ToInt32(Session["SportId"]))
                {
                    Session["SportId"] = sportId;
                }
                else if (sportId == 0 && sportId != Convert.ToInt32(Session["SportId"]))
                {
                    sportId = Convert.ToInt32(Session["SportId"]);
                }

            }
            else
            {
                Session.Add("SportId", sportId);
            }

            int seasonId = 0;
            if (int.TryParse(collection["seasonId"], out seasonId))
            {
                
            }

            if (Session["SeasonId"] != null)
            {
                if (seasonId > 0 && seasonId > Convert.ToInt32(Session["SeasonId"]))
                {
                    Session["SeasonId"] = seasonId;
                }
                else if (seasonId == 0 && seasonId != Convert.ToInt32(Session["SeasonId"]))
                {
                    seasonId = Convert.ToInt32(Session["SeasonId"]);
                }
            }
            else if (Session["SeasonId"] == null && seasonId > 0)
            {
                Session.Add("SeasonId", seasonId);
            }

            int ageCategoryId = 0;
            if (int.TryParse(collection["ageCategoryId"], out ageCategoryId))
            {
                
            }

            if (Session["AgeCategoryId"] != null)
            {
                if (ageCategoryId > 0 && seasonId > Convert.ToInt32(Session["AgeCategoryId"]))
                {
                    Session["AgeCategoryId"] = ageCategoryId;
                }
                else if (ageCategoryId == 0 && ageCategoryId != Convert.ToInt32(Session["AgeCategoryId"]))
                {
                    ageCategoryId = Convert.ToInt32(Session["AgeCategoryId"]);
                }
            }
            else if (Session["AgeCategoryId"] == null && ageCategoryId > 0)
            {
                Session.Add("AgeCategoryId", ageCategoryId);
            }

            int jobId = id;


            // Adding selected season to session variable
            
            

            ViewBag.SportId = sportId;
            ViewBag.AgeCategoryId = ageCategoryId;
            ViewBag.SeasonId = seasonId;
            ViewBag.JobId = jobId;
            ViewBag.CustomerId = Session["CustomerId"];
            return View("GetMunicipalities", model.GetSelectionView(sportId, seasonId, jobId));
        }

        /// <summary>
        /// The get municipalities.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <param name="jobId">
        /// The job id.
        /// </param>
        /// <param name="ageCategoryId">
        /// The age category id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <param name="useRemoteData">
        /// The use remote data.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult GetMunicipalities(
            int sportId, int customerId, int jobId, int ageCategoryId, int seasonId, bool useRemoteData = true)
        {
            _log.Debug("CustomerId: " + customerId.ToString());
            ViewBag.CustomerId = customerId;
            ViewBag.SportId = sportId;
            ViewBag.JobId = jobId;
            ViewBag.AgeCategoryId = ageCategoryId;
            ViewBag.SeasonId = seasonId;

            if (Convert.ToInt32(Session["CustomerId"]) == 0)
            {
                Session.Add("CustomerId", customerId);
            }

            if (Convert.ToInt32(Session["AgeCategoryId"]) == 0)
            {
                Session.Add("AgeCategoryId", ageCategoryId);
            }

            if (Convert.ToInt32(Session["SeasonId"]) == 0)
            {
                Session.Add("SeasonId", seasonId);
            }
            
            var model = new SelectionViewModel();


            return View("GetMunicipalities", model.GetSelectionView(sportId, seasonId, jobId));
        }

        /// <summary>
        /// The store tournaments.
        /// </summary>
        /// <param name="id">
        /// Customer id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="RedirectResult"/>.
        /// </returns>
        [HttpPost]
        public RedirectResult StoreTournaments(int id, FormCollection collection)
        {
            // Getting values from URL and session
            int sportId = Convert.ToInt32(collection["sportId"]);
            if (sportId == 0)
            {
                sportId = Convert.ToInt32(Session["SportId"]);

                if (sportId == 0)
                {
                    sportId = SportId;
                }
            }
            int jobId = Convert.ToInt32(collection["jobId"]);
            
            // Session variables
            int customerId = Convert.ToInt32(Session["customerId"]);
            int ageCategoryId = Convert.ToInt32(Session["ageCategoryId"]);
            int seasonId = Convert.ToInt32(Session["SeasonId"]);
            _log.Debug("jobId: " + jobId);
            _log.Debug("SportsId: " + sportId);
            _log.Debug("CustomerId: " + customerId);

            var model = new TournamentModel();
            
            // Transforming the list of selected tournaments into a list of tournament objects
            var selectedTournaments = model.NormalizeTournaments(collection["selectedTournaments"]);

            if (!string.IsNullOrWhiteSpace(collection["writtentournament"]))
            {
                // Let's check if the box contains , or ;
                var tournamentString = collection["writtentournament"];
                var tournaments = new List<string>();
                
                if (tournamentString.Contains(";") )
                {
                    tournaments.AddRange(tournamentString.Split(';'));
                }
                else if (tournamentString.Contains(","))
                {
                    tournaments.AddRange(tournamentString.Split(','));
                }
                else
                {
                    tournaments.Add(tournamentString);
                }

                foreach (string t in tournaments)
                {
                    var tournamentId = Convert.ToInt32(t);
                    if (tournamentId == 0) continue;
                    var tournament = model.GetTournamentById(tournamentId, seasonId);
                    tournament.TournamentSelected = true;
                    tournament.SportId = sportId;

                    tournament.AgeCategoryId = ageCategoryId;
                    selectedTournaments.Add(tournament);

                    model.InsertTournament(tournament);
                }
            }

            // Insert the information in our database - if we have any tournaments that is
            if (selectedTournaments.Any())
            {
                model.InsertSelectedTournaments(selectedTournaments, jobId, ageCategoryId, customerId, sportId);
            }

            // Redirect the customer/user to the details web page
            return new RedirectResult("/Customer/Details/" + customerId);
        }

        /// <summary>
        /// This method lists up the different jobs - that is the names of the jobs, not what they are containing.
        /// </summary>
        /// <param name="id">
        /// The job Id we use to get jobId informasjon
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ListJobs(int id)
        {
            Session.Add("CustomerId", id);
            ViewBag.CustomerId = id;

            var jobRepository = new JobRepository
                {
                    CustomerId = id
                };

            return PartialView(new List<Job>(jobRepository.GetAll()));
        }

        /// <summary>
        /// The insert job.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult InsertJob(int id, FormCollection collection)
        {
            Session.Add("CustomerId", id);
            ViewBag.CustomerId = id;
            string customerJobName = Convert.ToString(collection["JobbNavn"]);

            int sportId = Convert.ToInt32(collection["sports"]);
            int ageCategoryId = Convert.ToInt32(collection["agecategorydefinition"]);

            bool schedule = false;
            if (collection["schedule"] != null)
            {
                if (collection["schedule"].ToLower() == "on")
                {
                    schedule = true;
                }
            }
            
            int customerId = id;

            JobRepository jobRepository = new JobRepository();
            Job job = new Job
                {
                    CustomerId = customerId,
                    JobName = customerJobName,
                    SportId = sportId,
                    AgeCategoryId = ageCategoryId,
                    Schedule = schedule
                };


            int insertId = jobRepository.InsertOne(job);

            ViewBag.JobId = insertId;

            return new RedirectResult("/Customer/Details/" + id);
        }

        /// <summary>
        /// The get age categories.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpGet]
        public ActionResult GetAgeCategories()
        {
            AgeCategoryModel model = new AgeCategoryModel();

            return PartialView("GetAgeCategories", model.GetAgeCategories());
        }

        /// <summary>
        /// The update job.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult UpdateJob(int id, FormCollection collection)
        {
            Session.Add("CustomerId", id);
            ViewBag.CustomerId = id;
            
            int jobId = Convert.ToInt32(collection["JobId"]);
            int sportId = Convert.ToInt32(collection["sports"]);
            int ageCategoryDefinition = Convert.ToInt32(collection["agecategorydefinition"]);
            
            string customerJobName = collection["JobbNavn"];
            bool schedule = Convert.ToString(collection["schedule"])  == "on";
            
            var jobModel = new JobModel();

            jobModel.UpdateJob(jobId, customerJobName, schedule, sportId, ageCategoryDefinition);
            
            return new RedirectResult("/Customer/Details/" + id);
        }

        public ActionResult DefineJob(int id)
        {
            Session.Add("SeasonId", 0);
            
            var model = new DefineViewModel();
            return View(model.Get(id));
        }

        /// <summary>
        /// The activate job.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public ActionResult ActivateJob(int id)
        {
            ViewBag.CustomerId = id;

            int intJobId = Convert.ToInt32(Request.QueryString["JobId"]);

            var repository = new JobRepository
                {
                    JobId = intJobId
                };
            repository.Activate();
            
            return new RedirectResult("/Customer/Details/" + id);
        }

        /// <summary>
        /// The delete job.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult DeleteJob(int id)
        {
            ViewBag.CustomerId = id;

            Job job = new Job
                {
                    JobId = Convert.ToInt32(Request.QueryString["JobId"])
                };
            JobRepository repository = new JobRepository();

            repository.Delete(job);
            
            return new RedirectResult("/Customer/Details/" + id);
        }

        /// <summary>
        /// The create job.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult CreateJob(int id)
        {
            int jobId = 0;
            if (HttpContext.Request.QueryString["JobId"] != null)
            {
                jobId = Convert.ToInt32(Request.QueryString["JobId"]);
            }

            ViewBag.CustomerId = id;
            ViewBag.JobId = jobId;

            return PartialView();
        }

        /// <summary>
        /// The view job.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ViewJob(int id)
        {
            JobRepository repository = new JobRepository();
            return View(repository.CreateCustomerJobs(id));
        }

        /// <summary>
        /// The show jobs.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ShowJobs(int id)
        {
            ViewBag.CustomerName = CustomerModel.GetCustomer(id).Name;
            ViewBag.CustomerId = CustomerModel.GetCustomer(id).CustomerId;
            
            JobRepository repository = new JobRepository();
            return View(repository.ShowJobs(id));
        }

        /// <summary>
        /// The send results.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult SendResults(int id)
        {
            //// Creating the customer Model object
            // CustomerModel customerModel = new CustomerModel();

            //// Getting the Customer Tournaments
            // List<JobTournament> jobTournaments = customerModel.GetCustomerTournaments(id);

            //// Returning the view
            ViewBag.CustomerId = id;

            CustomerModel customerModel = new CustomerModel();
            int customerForeignId = customerModel.GetCustomerForeignId(id);
            ViewBag.CustomerForeignId = customerForeignId;
            
            JobRepository repository = new JobRepository();
            return View("SendResult", repository.ShowJobs(id));
        }

        /// <summary>
        /// This controller is used to create a maintenance file for all customers
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult SendAllResults()
        {
            return View("SendAllResults");
        }

        /// <summary>
        /// The send all results.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult SendAllResults(FormCollection collection)
        {
            string startDate = collection["startDate"];
            string endDate = collection["endDate"];
            CustomerModel customerModel = new CustomerModel();
            customerModel.GetTournamentsForAllCustomers(startDate, endDate);

            // Sending the user to the list of customers
            return RedirectToAction("Index");
        }

        /// <summary>
        /// The send results.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult SendResult(int id, FormCollection collection)
        {
            Tournaments tournaments = new Tournaments();
            tournaments.CreateTournamentFile(id, collection);
            
            // Sending the user to the list of customers
            return RedirectToAction("Index");
        }

        /// <summary>
        /// The show teams.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ShowTeams(int id)
        {
            //// Returning the view
            ViewBag.CustomerId = id;

            // Sending the user to the list of customers
            return RedirectToAction("Index");
        }
    }
}