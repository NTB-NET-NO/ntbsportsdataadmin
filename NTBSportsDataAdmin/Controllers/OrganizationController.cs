﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net.Repository.Hierarchy;
using NTB.SportsDataAdmin.Application.Models;
using NTB.SportsDataAdmin.Application.Repositories;
using NTB.SportsDataAdmin.Domain.Classes;
using log4net;


namespace NTB.SportsDataAdmin.Application.Controllers
{
    public class OrganizationController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(OrganizationController));
        //
        // GET: /Organization/

        public ActionResult Index()
        {
            var organizationModel = new OrganizationModel();
            return View(organizationModel.GetOrganizations());
        }

        // GET: /Crganization/CreateOrganizationSeason/2000
        public ActionResult CreateOrganizationSeason(int id)
        {
            var model = new OrganizationModel();
            return View(model.GetCreateSeasonView(id));
        }

        //
        // POST: /Organization/CreateSeason
        [HttpPost]
        public ActionResult CreateSeason(int id, FormCollection collection)
        {
            var model = new OrganizationModel();
            model.CreateSeason(id, collection);
            return RedirectToAction("Details/" + id);
        }

        //
        // GET: /Organization/Details/5
        public ActionResult Details(int id)
        {
            OrganizationModel model = new OrganizationModel();
            Session["OrgId"] = id;

            return View(model.GetOrganizationDetailsById(id));
        }

        //
        // GET: /Organization/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Organization/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                var model = new OrganizationModel();
                model.InsertOrganization(collection);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Organization/Edit/5
 
        public ActionResult Edit(int id)
        {
            var model = new OrganizationModel();
            var organization = model.GetOrganizationById(id);
            return View(organization);
        }

        //
        // POST: /Organization/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                var model = new OrganizationModel();
                model.UpdateOrganization(collection);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Organization/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Organization/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult CreateOrganizationSport(int id)
        {
            var repository = new SportRepository();
            ViewBag.OrgId = id;
            return View(repository.GetAll().ToList());
        }

        [HttpPost]
        public ActionResult CreateSport(int id, FormCollection collection)
        {
            var model = new OrganizationModel();
            model.CreateSport(id, collection);
            return RedirectToAction("Details/"+id);
        }

        public ActionResult GetClubs(int id)
        {
            /*
             * Consider some sort of factory pattern here.
             * So to choose which facade is to be used to get the clubs
             * This in case we are to add more outside NIF/NFF event organizors into admin
             * 
             */
            int orgId = Convert.ToInt32(Session["OrgId"]);

            if (orgId <= 0) return RedirectToAction("SelectSeason/" + id);

            var orgModel = new OrganizationModel();
            var clubs = orgModel.GetOrganizationClubs(id, orgId);

            var newClubs = orgModel.FindNewClubs(clubs);

            orgModel.InsertClubs(newClubs);

            return RedirectToAction("SelectSeason/" + id);

        }

        public ActionResult ShowClubs(int id)
        {
            var model = new OrganizationModel();
            return View(model.GetClubsByParentOrgId(id, true));
        }

        [HttpPost]
        public ActionResult CheckClubs (int id, FormCollection collection)
        {
            var model = new OrganizationModel();

            model.UpdateCheckedClubs(id, collection);

            return RedirectToAction("ShowClubs/" + id);
        }

        [HttpPost]
        public ActionResult UpdateClubAdvanced(int id, FormCollection collection)
        {
            var clubId = id; // Just so we know what type of Id we are working with
            var parentOrgid = Convert.ToInt32(Session["OrgId"]); // Getting the organization we are working with

            var model = new OrganizationModel();
            model.UpdateClubAdvanced(clubId, parentOrgid, collection);

            return RedirectToAction("CheckClubAdvanced/" + parentOrgid);
        }

        /// <summary>
        ///     Check Club Advanced is a manual process where the user have to find the right municipality, district and even country.
        /// </summary>
        /// <param name="id">Organization Id we are working with</param>
        /// <returns>Check Club Advanced View</returns>
        public ActionResult CheckClubAdvanced(int id)
        {
            
            if (Session["ConnectedOrgId"].ToString() == string.Empty)
            {
                return RedirectToAction("ShowClubs/" + id);
            }

            Session["OrgId"] = id;
            var model = new OrganizationModel();

            var connectedOrgId = Convert.ToInt32(Session["ConnectedOrgId"]);

            return View(model.GetAdvancedCheckClubView(id, connectedOrgId));

        }

        [HttpPost]
        public ActionResult ConnectClubs(int id, FormCollection collection)
        {
            var orgId = id; // Just so we know which type of Id we are working on
            var fromOrgId = Convert.ToInt32(collection["selectOrg"]);
            Session["ConnectedOrgId"] = fromOrgId;

            var model = new OrganizationModel();
            Dictionary<Club, List<Club>> foundClubs = model.FindClubs(orgId, fromOrgId);

            ViewBag.OrgId = id;
            return View("CheckClubs", foundClubs);
            // return RedirectToAction("SelectSeason/" + id);
        }

        public ActionResult ConnectClubs(int id)
        {
            var orgModel = new OrganizationModel();
            int orgId = 0;
            if (Session["OrgId"].ToString() != string.Empty)
            {
                orgId = Convert.ToInt32(Session["OrgId"]);
            }
            orgModel.OrgId = orgId;
            var org = orgModel.GetOrganizationById(orgId);

            var orgs = orgModel.GetOrganizations();

            var viewModel = new ConnectClubsViewModel
            {
                Organization = org,
                Organizations = orgs
            };

            return View(viewModel);
        }

        public ActionResult SelectSeason(int id)
        {
            OrganizationModel model = new OrganizationModel();
            int orgId = Convert.ToInt32(Session["OrgId"]);
            return View(model.GetSelectedSeasonDetailBySeasonid(id, orgId));
            
        }
    }
}
