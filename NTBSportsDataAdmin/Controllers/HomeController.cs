﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HomeController.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The home controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System.Web.Mvc;
using NTB.SportsDataAdmin.Application.Models;

namespace NTB.SportsDataAdmin.Application.Controllers
{
    /// <summary>
    /// The home controller.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index()
        {
            ViewBag.Message = "Velkommen til SportsData Admin!";

            CustomerModel customerModel = new CustomerModel();
            return View(customerModel.GetNumberOfCustomerJobs());
        }

        /// <summary>
        /// The about.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult About()
        {
            return View();
        }
    }
}