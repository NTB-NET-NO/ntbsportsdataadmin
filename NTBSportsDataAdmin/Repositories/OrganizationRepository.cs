using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsDataAdmin.Application.Interfaces;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Repositories
{
    public class OrganizationRepository : IRepository<Organization>, IDisposable, IOrganizationDataMapper
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(OrganizationRepository));
        
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int InsertOne(Organization domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {

                    var sqlCommand = new SqlCommand("SportsData_InsertSport", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };


                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@OrgId", domainobject.OrganizationId));
                    sqlCommand.Parameters.Add(new SqlParameter("@OrgName", domainobject.OrganizationName));
                    sqlCommand.Parameters.Add(new SqlParameter("@OrgNameShort", domainobject.OrganizationNameShort));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.OrganizationNameShort));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportName", domainobject.Sport));
                    sqlCommand.Parameters.Add(new SqlParameter("@TeamSport", domainobject.TeamSport));
                    sqlCommand.Parameters.Add(new SqlParameter("@SingleSport", domainobject.SingleSport));

                    sqlCommand.ExecuteNonQuery();


                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return 1;
        }

        public void InsertAll(List<Organization> domainobject)
        {
            foreach (var org in domainobject)
            {
                InsertOne(org);
            }
        }

        public void Update(Organization domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {

                    var sqlCommand = new SqlCommand("SportsData_UpdateOrganizationById", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };


                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@OrgId", domainobject.OrganizationId));
                    sqlCommand.Parameters.Add(new SqlParameter("@OrgName", domainobject.OrganizationName));
                    sqlCommand.Parameters.Add(new SqlParameter("@OrgNameShort", domainobject.OrganizationNameShort));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@Sport", domainobject.Sport));
                    sqlCommand.Parameters.Add(new SqlParameter("@SingleSport", domainobject.SingleSport));
                    sqlCommand.Parameters.Add(new SqlParameter("@TeamSport", domainobject.TeamSport));

                    sqlCommand.ExecuteNonQuery();


                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void Delete(Organization domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Organization> GetAll()
        {
            var organizations = new List<Organization>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {

                    var sqlCommand = new SqlCommand("SportsData_GetOrganizations", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };


                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    while (sqlDataReader.Read())
                    {

                        var organization = new Organization();
                        if (sqlDataReader["OrgId"] != DBNull.Value)
                        {
                            organization.OrganizationId = Convert.ToInt32(sqlDataReader["OrgId"].ToString());
                        }
                        organization.OrganizationName = sqlDataReader["OrgName"].ToString();
                        organization.OrganizationNameShort = sqlDataReader["OrgNameShort"].ToString();

                        organizations.Add(organization);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            return organizations.AsQueryable();
        
        }

        public Organization Get(int id)
        {
            var organization = new Organization();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {

                    var sqlCommand = new SqlCommand("SportsData_GetOrganizationsById", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    sqlCommand.Parameters.Add(new SqlParameter("@OrgId", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    while (sqlDataReader.Read())
                    {

                        
                        if (sqlDataReader["OrgId"] != DBNull.Value)
                        {
                            organization.OrganizationId = Convert.ToInt32(sqlDataReader["OrgId"].ToString());
                        }

                        if (sqlDataReader["SingleSport"] != DBNull.Value)
                        {
                            organization.SingleSport = Convert.ToInt32(sqlDataReader["SingleSport"].ToString());
                        }

                        if (sqlDataReader["TeamSport"] != DBNull.Value)
                        {
                            organization.TeamSport = Convert.ToInt32(sqlDataReader["TeamSport"].ToString());
                        }

                        if (sqlDataReader["SportId"] != DBNull.Value)
                        {
                            organization.SportId = Convert.ToInt32(sqlDataReader["SportId"].ToString());
                        }

                        organization.Sport = sqlDataReader["Sport"].ToString();
                        organization.OrganizationName = sqlDataReader["OrgName"].ToString();
                        organization.OrganizationNameShort = sqlDataReader["OrgNameShort"].ToString();

                        
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            return organization;
        }

        public Organization GetOrganizationBySportId(int sportId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_GetOrganizationBySportId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@sportId", sportId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return null;
                }

                while (sqlDataReader.Read())
                {
                    var organization = new Organization();

                    if (sqlDataReader["OrgId"] != DBNull.Value)
                    {
                        organization.OrganizationId = Convert.ToInt32(sqlDataReader["OrgId"].ToString());
                    }

                    if (sqlDataReader["SingleSport"] != DBNull.Value)
                    {
                        organization.SingleSport = Convert.ToInt32(sqlDataReader["SingleSport"].ToString());
                    }

                    if (sqlDataReader["TeamSport"] != DBNull.Value)
                    {
                        organization.TeamSport = Convert.ToInt32(sqlDataReader["TeamSport"].ToString());
                    }

                    if (sqlDataReader["SportId"] != DBNull.Value)
                    {
                        organization.SportId = Convert.ToInt32(sqlDataReader["SportId"].ToString());
                    }

                    organization.Sport = sqlDataReader["Sport"].ToString();
                    organization.OrganizationName = sqlDataReader["OrgName"].ToString();

                    return organization;
                }
            }
            return null;
        }
    }
}