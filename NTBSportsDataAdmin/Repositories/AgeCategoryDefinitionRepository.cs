using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsDataAdmin.Application.Interfaces;
using NTB.SportsDataAdmin.Domain.Classes;
using log4net;

namespace NTB.SportsDataAdmin.Application.Repositories
{
    public class AgeCategoryDefinitionRepository : IRepository<AgeCategoryDefinition>, IDisposable

    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AgeCategoryDefinitionRepository));

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int InsertOne(AgeCategoryDefinition domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<AgeCategoryDefinition> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(AgeCategoryDefinition domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(AgeCategoryDefinition domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<AgeCategoryDefinition> GetAll()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[SportsData_GetAgeCategoryDefinitions]", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var ageCategoryDefinitions = new List<AgeCategoryDefinition>();
                    while (sqlDataReader.Read())
                    {
                        var ageCategoryDefinition = new AgeCategoryDefinition
                            {
                                DefinitionId = Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]),
                                DefinitionName = sqlDataReader["AgeCategoryDefinition"].ToString()
                            };
                        ageCategoryDefinitions.Add(ageCategoryDefinition);
                    }
                    return ageCategoryDefinitions.AsQueryable();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    return new List<AgeCategoryDefinition>().AsQueryable();
                }
                finally
                {

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public AgeCategoryDefinition Get(int id)
        {
            return GetAll().Single(s => s.DefinitionId == id);
        }
    }
}