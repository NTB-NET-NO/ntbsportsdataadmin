using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsDataAdmin.Application.Interfaces;
using NTB.SportsDataAdmin.Domain.Classes;
using log4net;
using log4net.Config;

namespace NTB.SportsDataAdmin.Application.Repositories
{
    public class DistrictRepository : IRepository<District>, IDisposable, IDistrictDataMapper
    {
        public DistrictRepository()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        public int SportId { get; set; }

        internal static readonly ILog Logger = LogManager.GetLogger(typeof(DistrictRepository));

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int InsertOne(District domainobject)
        {
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // running the command
                    var sqlCommand = new SqlCommand("SportsData_InsertCountyTable", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@CountyId", domainobject.DistrictId));

                    sqlCommand.Parameters.Add(new SqlParameter("@CountyName", domainobject.DistrictName));

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Parameters.Clear();
                    sqlCommand.CommandText = "SELECT @@IDENTITY";
                    sqlCommand.CommandType = CommandType.Text;

                    int insertId = Convert.ToInt32(sqlCommand.ExecuteScalar());

                    sqlCommand.Dispose();

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }

                    return insertId;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return 0;
            }
        }

        public void InsertAll(List<District> domainobject)
        {
            foreach (District district in domainobject)
            {
                InsertOne(district);
            }
        }

        public void Update(District domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(District domainobject)
        {
            throw new NotImplementedException();
        }

        public void DeleteAll()
        {
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    var sqlCommand = new SqlCommand("SportsData_TruncateCountiesTable", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        public IQueryable<District> GetAll()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                // DONE: Move code to SportsData Stored Procedures
                var sqlCommand = new SqlCommand("SportsData_GetDistricts", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", DBNull.Value));

                sqlConnection.Open();
                var districts = new List<District>();
                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var district = new District
                                {
                                    DistrictId = (int) sqlDataReader["DistrictId"],
                                    DistrictName = sqlDataReader["DistrictName"].ToString()
                                };

                            districts.Add(district);
                        }

                        sqlDataReader.Close();
                    }

                    sqlConnection.Close();
                }

                return districts.AsQueryable();
            }
        }

        public District Get(int id)
        {
            return GetAll().Single(d => d.DistrictId == id);
        }

        public List<District> GetDistrictsBySportId(int sportId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                // DONE: Move code to SportsData Stored Procedures
                var sqlCommand = new SqlCommand("SportsData_GetDistricts", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                sqlConnection.Open();
                var districts = new List<District>();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        var district = new District
                            {
                                DistrictId = (int) sqlDataReader["DistrictId"],
                                DistrictName = sqlDataReader["DistrictName"].ToString()
                            };

                        districts.Add(district);
                    }

                    sqlDataReader.Close();
                }

                sqlConnection.Close();
                

                return districts;
            }
        }
    }
}