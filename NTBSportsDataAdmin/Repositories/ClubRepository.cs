﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClubRepository.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The Club Repository which is used to get data from the database.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common.CommandTrees;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsDataAdmin.Application.Interfaces;
using NTB.SportsDataAdmin.Domain.Classes;
using log4net;

namespace NTB.SportsDataAdmin.Application.Repositories
{
    public class ClubRepository : IRepository<Club>, IDisposable, IClubDataMapper
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ClubRepository));

        public int InsertOne(Club domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    // @Done: Move this code to the database
                    var sqlCommand = new SqlCommand("SportsData_InsertClub", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@ClubId", domainobject.ClubId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ParentOrgId", domainobject.ParentOrgId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ClubName", domainobject.ClubName));
                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));
                    sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityId", domainobject.MunicipalityId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();

                    }

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return 1;
        }

        public void InsertAll(List<Club> domainobject)
        {
            foreach (Club club in domainobject)
            {
                InsertOne(club);
            }
        }

        public void Update(Club domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Club domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Club> GetAll()
        {
            throw new NotImplementedException();
        }

        public Club Get(int id)
        {
            var club = new Club();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    // @Done: Move this code to the database
                    var sqlCommand = new SqlCommand("SportsData_GetAllEvents", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@ClubId", id));
                    
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();

                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return club;
                    }

                    while (sqlDataReader.Read())
                    {
                        club.ClubId = Convert.ToInt32(sqlDataReader["Id"]);
                        club.ClubName = sqlDataReader["Name"].ToString();

                        club.MunicipalityId = Convert.ToInt32(sqlDataReader["MunicipalityId"]);
                        club.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                        club.ParentOrgId = Convert.ToInt32(sqlDataReader["OrgId"]);
                    }

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }

                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return club;
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<Club> GetClubsByOrgId(int orgId)
        {
            var clubs = new List<Club>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    // @Done: Move this code to the database
                    var sqlCommand = new SqlCommand("SportsData_GetClubsByOrgId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@OrgId", orgId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();

                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return clubs;
                    }

                    while (sqlDataReader.Read())
                    {
                        var club = new Club();
                        club.ClubId = Convert.ToInt32(sqlDataReader["Id"]);
                        club.ClubName = sqlDataReader["Name"].ToString();

                        club.MunicipalityId = 0;
                        if (sqlDataReader["MunicipalityId"] != DBNull.Value)
                        {
                            club.MunicipalityId = Convert.ToInt32(sqlDataReader["MunicipalityId"]);
                        }

                        if (sqlDataReader["MunicipalityName"] != DBNull.Value)
                        {
                            club.MunicipalityName = sqlDataReader["MunicipalityId"].ToString();
                        }

                        club.DistrictId = 0;
                        if (sqlDataReader["DistrictId"] !=  DBNull.Value)
                        {
                            club.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                        }

                        if (sqlDataReader["DistrictName"] != DBNull.Value)
                        {
                            club.DistrictName = sqlDataReader["DistrictName"].ToString();
                        }

                        club.ParentOrgId = Convert.ToInt32(sqlDataReader["ParentOrgId"]);

                        clubs.Add(club);
                    }

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }

                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return clubs;
            }
        }

        public List<Club> GetClubsByNameAndOrgId(string clubName, int orgId)
        {
            var clubs = new List<Club>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    // @Done: Move this code to the database
                    var sqlCommand = new SqlCommand("[SportsData_GetClubByNameAndOrgId]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@OrgId", orgId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ClubName", clubName));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();

                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return clubs;
                    }

                    while (sqlDataReader.Read())
                    {
                        var club = new Club();
                        club.ClubId = Convert.ToInt32(sqlDataReader["Id"]);
                        club.ClubName = sqlDataReader["Name"].ToString();

                        club.MunicipalityId = 0;
                        if (sqlDataReader["MunicipalityId"] != DBNull.Value)
                        {
                            club.MunicipalityId = Convert.ToInt32(sqlDataReader["MunicipalityId"]);
                        }

                        club.DistrictId = 0;
                        if (sqlDataReader["DistrictId"] != DBNull.Value)
                        {
                            club.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                        }

                        club.ParentOrgId = Convert.ToInt32(sqlDataReader["ParentOrgId"]);

                        clubs.Add(club);
                    }

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }

                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return clubs;
            }
        }

        public List<Club> GetClubsByParentOrgId(int parentOrgId)
        {
            var clubs = new List<Club>();

            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    // @Done: Move this code to the database
                    var sqlCommand = new SqlCommand("[SportsData_GetClubByParentOrgId]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@ParentOrgId", parentOrgId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();

                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return clubs;
                    }

                    while (sqlDataReader.Read())
                    {
                        var club = new Club();
                        club.ClubId = Convert.ToInt32(sqlDataReader["Id"]);
                        club.ClubName = sqlDataReader["Name"].ToString();

                        club.MunicipalityId = 0;
                        if (sqlDataReader["MunicipalityId"] != DBNull.Value)
                        {
                            club.MunicipalityId = Convert.ToInt32(sqlDataReader["MunicipalityId"]);
                        }

                        club.DistrictId = 0;
                        if (sqlDataReader["DistrictId"] != DBNull.Value)
                        {
                            club.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                        }

                        club.ParentOrgId = Convert.ToInt32(sqlDataReader["ParentOrgId"]);

                        clubs.Add(club);
                    }

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }

                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return clubs;
        }

        private List<Club> GetClubsByMunicipality(int municipalityId, int parentOrgid)
        {
            List<Club> clubs = new List<Club>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    // @Done: Move this code to the database
                    var sqlCommand = new SqlCommand("SportsData_GetClubsByMunicipality", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityId", municipalityId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ParentOrgId", parentOrgid));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return clubs;
                    }

                    while (sqlDataReader.Read())
                    {
                        var club = new Club
                        {
                            ClubId = Convert.ToInt32(sqlDataReader["ClubId"]),
                            ClubName = sqlDataReader["ClubName"].ToString(),
                            DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]),
                            MunicipalityId = Convert.ToInt32(sqlDataReader["MunicipalityId"])
                        };

                        clubs.Add(club);
                    }

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return clubs;
        }

        public List<Club> GetClubsByMunicipalities(List<string> selectedMunicipalities, int parentOrgId)
        {
            
            List<Club> clubs = new List<Club>();

            foreach (string municipality in selectedMunicipalities)
            {
                var municipalityId = Convert.ToInt32(municipality);
                clubs.AddRange(GetClubsByMunicipality(municipalityId, parentOrgId));
            }

            return clubs;
        }


        public void UpdateCheckedClub(Club club)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    // @Done: Move this code to the database
                    var sqlCommand = new SqlCommand("SportsData_UpdatedCheckedClub", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@ClubId", club.ClubId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ParentOrgId", club.ParentOrgId));
                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", club.DistrictId));
                    sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityId", club.MunicipalityId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void UpdateClubAdvanced(Club club)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    // @Done: Move this code to the database
                    var sqlCommand = new SqlCommand("SportsData_UpdatedCheckedClub", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@ClubId", club.ClubId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ParentOrgId", club.ParentOrgId));
                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", club.DistrictId));
                    sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityId", club.MunicipalityId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}