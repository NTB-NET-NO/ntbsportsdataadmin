using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsDataAdmin.Application.Interfaces;
using NTB.SportsDataAdmin.Domain.Classes;
using log4net;

namespace NTB.SportsDataAdmin.Application.Repositories
{
    public class CustomerTournamentRepository : IRepository<Tournament>, IDisposable
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof (CustomerTournamentRepository));

        /// <summary>
        ///     Gets or sets the job id
        /// </summary>
        public int JobId { get; set; }

        /// <summary>
        ///     Gets or sets the sport id
        /// </summary>
        public int SportId { get; set; }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int InsertOne(Tournament domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[SportsData_CustomerJob_Tournament_Insert]", sqlConnection);
                    sqlCommand.Parameters.Add(new SqlParameter("@JobId", domainobject.JobId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentNumber", domainobject.TournamentNumber));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.CommandText = "SELECT @@IDENTITY";
                    sqlCommand.CommandType = CommandType.Text;

                    sqlCommand.Dispose();

                    Logger.Info("Inserted successfully");

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return 1;
            }
        }

        public void InsertAll(List<Tournament> domainobject)
        {
            foreach (Tournament tournament in domainobject)
            {
                InsertOne(tournament);
            }
        }

        public void Update(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Tournament> GetAll()
        {
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
                {
                    var sqlCommand = new SqlCommand("SportsData_GetCustomerTournaments", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@JobId", JobId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", SportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Connection.Open();

                    sqlCommand.Prepare();

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

                    var tournaments = new List<Tournament>();

                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var tournament = new Tournament
                                {
                                    TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]),
                                    TournamentNumber = sqlDataReader["TournamentNumber"].ToString(),
                                    TournamentAccepted = Convert.ToInt32(sqlDataReader["TournamentAccepted"])
                                };
                            tournaments.Add(tournament);
                        }
                    }

                    sqlDataReader.Close();
                    return tournaments.AsQueryable();
                }

                // start by finding out if the client has any jobs related to it
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                Logger.Error(e.StackTrace);

                return null;
            }
        }

        public Tournament Get(int id)
        {
            return GetAll().Single(s => s.TournamentId == id);
        }
    }
}