using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsDataAdmin.Application.Interfaces;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Repositories
{
    public class MediaHouseRepository : IRepository<MediaHouse>, IDisposable
    {
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int InsertOne(MediaHouse domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<MediaHouse> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(MediaHouse domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(MediaHouse domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<MediaHouse> GetAll()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_GetMediaHouses", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                var mediaHouses = new List<MediaHouse>();

                while (sqlDataReader.Read())
                {
                    var mediaHouse = new MediaHouse
                        {
                            MediaHouseId = Convert.ToInt32(sqlDataReader["MediaHouseId"]),
                            MediaHouseName = sqlDataReader["MediaHouse"].ToString()
                        };
                    mediaHouses.Add(mediaHouse);
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return mediaHouses.AsQueryable();
            }
        }

        public MediaHouse Get(int id)
        {
            return GetAll().Single(s => s.MediaHouseId == id);
        }
    }
}