// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Calendars.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The calendars.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsDataAdmin.Application.Interfaces;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Repositories
{
    /// <summary>
    ///     The calendars.
    /// </summary>
    public class CalendarRepository : IRepository<Calendar>, IDisposable, ICalendarDataMapper
    {
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? SportId { get; set; }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int InsertOne(Calendar domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Calendar> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Calendar domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Calendar domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Calendar> GetAll()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                // @Done: Move this code to the database
                var sqlCommand = new SqlCommand("SportsData_GetAllEvents", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@ActivityId", SportId));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", StartDate));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", EndDate));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                var calendars = new List<Calendar>();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return calendars.AsQueryable();
                }

                while (sqlDataReader.Read())
                {
                    var calendar = new Calendar
                        {
                            CalendarId = (int) sqlDataReader["EventId"],
                            Name = sqlDataReader["Name"].ToString(),
                            StartDateTime = Convert.ToDateTime(sqlDataReader["DateStart"]),
                            SportId = Convert.ToInt32(sqlDataReader["ActivityId"])
                        };

                    calendars.Add(calendar);
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return calendars.AsQueryable();
            }
        }

        public Calendar Get(int id)
        {
            return GetAll().Single(s => s.CalendarId == id);
        }

        /// <summary>
        ///     Gets list of calendar events
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>Returns a list of matches based on start and end date</returns>
        public List<NumberOfMatches> GetMatchesByDateInterval(DateTime startDate, DateTime endDate)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                // @Done: Move this code to the database
                var sqlCommand = new SqlCommand("SportsData_GetNumberOfMatchesByDateInterval", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", startDate));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", endDate));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                var numberOfMatches = new List<NumberOfMatches>();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return numberOfMatches;
                }

                while (sqlDataReader.Read())
                {
                    var calendar = new NumberOfMatches
                    {
                        MatchDate = Convert.ToDateTime(sqlDataReader["Date"]),
                        TotalMatches = Convert.ToInt32(sqlDataReader["Matches"]),
                        SportId = Convert.ToInt32(sqlDataReader["SportId"])
                    };

                    numberOfMatches.Add(calendar);
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return numberOfMatches;
            }
        }
    }
}