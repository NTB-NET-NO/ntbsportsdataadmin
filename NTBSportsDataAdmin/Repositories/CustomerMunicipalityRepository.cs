using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsDataAdmin.Application.Interfaces;
using NTB.SportsDataAdmin.Domain.Classes;
using log4net;

namespace NTB.SportsDataAdmin.Application.Repositories
{
    public class CustomerMunicipalityRepository : IRepository<CustomerMunicipality>, IDisposable
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof (TournamentRepository));

        public int JobId { get; set; }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int InsertOne(CustomerMunicipality domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                // @DONE: Move SQL-query to database (Stored Procedure)
                var sqlCommand = new SqlCommand("SportsData_CustomerJob_Municipality_Insert", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@JobId", domainobject.JobId));

                sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityId", domainobject.MunicipalityId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                //sqlCommand.CommandText = "SELECT @@IDENTITY";
                //sqlCommand.CommandType = CommandType.Text;

                //int insertId = Convert.ToInt32(sqlCommand.ExecuteScalar());

                sqlCommand.Dispose();

                Logger.Info("Inserted successfully");

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlCommand.Connection.Close();
                }

                return 1;
            }
        }


        public void InsertAll(List<CustomerMunicipality> domainobject)
        {
            if (!domainobject.Any())
            {
                throw new Exception("Object contains no items");
            }

            foreach (CustomerMunicipality customerMunicipality in domainobject)
            {
                InsertOne(customerMunicipality);
            }
        }

        public void Update(CustomerMunicipality domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(CustomerMunicipality domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                // @DONE: Move SQL-query to database (Stored Procedure)
                var sqlCommand = new SqlCommand("SportsData_CustomerJob_Municipality_Delete", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@JobId", domainobject.JobId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        public IQueryable<CustomerMunicipality> GetAll()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_GetCustomerMunicipalities", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    throw new Exception("Query returned no rows!");
                }

                var customerMunicipalities = new List<CustomerMunicipality>();
                while (sqlDataReader.Read())
                {
                    var customerMunicipality = new CustomerMunicipality
                        {
                            // TournamentId, DistrictId, SeasonId, SportId, TournamentName, Push, AgeCategoryId, GenderId, TournamentTypeId, TournamentNumber
                            MunicipalityId = (int) sqlDataReader["MunicipalityId"],
                            JobId = (int) sqlDataReader["JobId"]
                        };

                    customerMunicipalities.Add(customerMunicipality);
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return customerMunicipalities.AsQueryable();
            }
        }

        public CustomerMunicipality Get(int id)
        {
            var customerMunicipalities = new List<CustomerMunicipality>(GetAll());

            // Linqing the return object
            return (from c in customerMunicipalities
                    where c.MunicipalityId == id && c.JobId == JobId
                    select c).Single();
        }
    }
}