using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsDataAdmin.Application.Interfaces;
using NTB.SportsDataAdmin.Application.Interfaces.DataAccess;
using NTB.SportsDataAdmin.Application.Models;
using NTB.SportsDataAdmin.Domain.Classes;
using log4net;

namespace NTB.SportsDataAdmin.Application.Repositories
{
    public class JobRepository : IRepository<Job>, IDisposable, IJobDataMapper
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof (JobRepository));

        /// <summary>
        ///     Gets or sets the job id.
        /// </summary>
        public int JobId { get; set; }

        /// <summary>
        ///     Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        ///     Gets or sets the customer id.
        /// </summary>
        public int CustomerId { get; set; }

        public void Dispose()
        {
            throw new NotImplementedException();
        }


        public int InsertOne(Job domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_CustomerJobs_Insert", sqlConnection);
                sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", domainobject.CustomerId));
                sqlCommand.Parameters.Add(new SqlParameter("@SportsId", domainobject.SportId));
                sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", domainobject.AgeCategoryId));
                sqlCommand.Parameters.Add(new SqlParameter("@JobName", domainobject.JobName));


                int scheduleId = 0;
                if (domainobject.Schedule)
                {
                    scheduleId = 1;
                }
                
                sqlCommand.Parameters.Add(new SqlParameter("@Schedule", scheduleId));

            sqlCommand.CommandType = CommandType.StoredProcedure;
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }
                sqlCommand.Prepare();
                sqlCommand.ExecuteNonQuery();

                sqlCommand.Parameters.Clear();
                sqlCommand.CommandText = "SELECT @@IDENTITY";
                sqlCommand.CommandType = CommandType.Text;

                int insertId = Convert.ToInt32(sqlCommand.ExecuteScalar());

                sqlCommand.Dispose();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return insertId;
            }
        }

        public void InsertAll(List<Job> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Job domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_CustomerJobs_Update", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@jobId", domainobject.JobId));
                sqlCommand.Parameters.Add(new SqlParameter("@JobName", domainobject.JobName));
                sqlCommand.Parameters.Add(new SqlParameter("@Schedule", domainobject.Schedule));
                sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", domainobject.AgeCategoryId));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void Delete(Job domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_CustomerJobs_Delete", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@JobId", domainobject.JobId));
                sqlCommand.CommandType = CommandType.StoredProcedure;

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        /// <summary>
        ///     Gets all the jobs for selected customer
        /// </summary>
        /// <returns>Iqueryable object containing all the jobs for the customer</returns>
        public IQueryable<Job> GetAll()
        {
            var jobs = new List<Job>();

            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                // Running the command
                var sqlCommand = new SqlCommand("SportsData_GetCustomerJobs", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();
                sqlCommand.Prepare();
                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var job = new Job
                                {
                                    JobId = Convert.ToInt32(sqlDataReader["JobId"]),
                                    CustomerId = Convert.ToInt32(sqlDataReader["CustomerId"])
                                };

                            if (sqlDataReader["SportsId"] != DBNull.Value)
                            {
                                job.SportId = Convert.ToInt32(sqlDataReader["SportsId"]);
                            }

                            job.JobName = (string) sqlDataReader["JobName"];

                            if (sqlDataReader["JobStatus"] != DBNull.Value)
                            {
                                job.Status = Convert.ToInt32(sqlDataReader["JobStatus"]);
                            }

                            if (sqlDataReader["AgeCategory"] != DBNull.Value)
                            {
                                job.AgeCategoryId = Convert.ToInt32(sqlDataReader["AgeCategory"]);
                            }

                            if (sqlDataReader["Schedule"] != DBNull.Value)
                            {
                                job.Schedule = Convert.ToBoolean(sqlDataReader["Schedule"]);
                            }

                            jobs.Add(job);
                        }
                    }
                }
            }

            return jobs.AsQueryable();
        }

        /// <summary>
        ///     Returns a single job from the dataset
        /// </summary>
        /// <param name="id">job id to get</param>
        /// <returns>job object containing the job</returns>
        public Job Get(int id)
        {
            // Returning one selected job
            return GetAll().Single(s => s.JobId == id);
        }

        public void Activate()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_CustomerJobs_Activate", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@JobId", JobId));
                sqlCommand.CommandType = CommandType.StoredProcedure;

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                // Running the query
                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void UpdateCustomerJobsWithAgeCategory(int jobId, int ageCategory, int sportId)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
                {
                    // Running the command
                    SqlCommand sqlCommand = new SqlCommand("SportsData_CustomerJobs_UpdateWithAgeCategory", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@JobId", jobId));

                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategory", ageCategory));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlConnection.Open();

                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        public JobsModels CreateCustomerJobs(int customerId)
        {
            var municipalityModel = new MunicipalityModel();
            var municipalities = municipalityModel.GetMunicipalities();

            // Getting the list of Sport that the user can select from
            SportModel sportModel = new SportModel();

            List<Sport> sports = new List<Sport>(sportModel.GetSports());


            // Getting the list of Districts that the user can select from
            DistrictModel districtModel = new DistrictModel();

            List<District> districts = new List<District>(districtModel.GetDistricts());


            // Now getting the customer
            Customer customer = new Customer();
            CustomerRepository cdb = new CustomerRepository();

            var customerResult = cdb.GetOneCustomer(customerId);

            customer.Name = customerResult.Name;
            customer.Address = customerResult.Address;
            customer.Email = customerResult.Email;
            customer.CustomerId = customerId;

            List<Customer> customers = new List<Customer> {customer};

            JobsModels jobs = new JobsModels
            {
                Municipalities = municipalities,
                Sports = sports,
                Districts = districts,
                Customers = customers
            };

            return jobs;
        }

        public List<int> GetJobsMunicipalities(int jobId)
        {
            List<int> municipalities = new List<int>();
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                // Running the command
                SqlCommand sqlCommand = new SqlCommand("SportsData_GetMunicipalityId", sqlConnection);
                sqlCommand.Parameters.Add(new SqlParameter("@JobId", SqlDbType.Int, 11));
                sqlCommand.CommandType = CommandType.StoredProcedure;

                // Adding the CustomerId
                sqlCommand.Parameters["@JobId"].Value = jobId;

                sqlConnection.Open();
                sqlCommand.Prepare();
                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            municipalities.Add(Convert.ToInt32(sqlDataReader["MunicipalityId"]));
                        }
                    }
                }
            }

            return municipalities;
        }

        public List<AgeCategoryDefinition> GetAgeCategoryDefinitions(int sportsId)
        {
            List<AgeCategoryDefinition> definitions = new List<AgeCategoryDefinition>();
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                // SportsData_GetAgeCategoryDefinitions
                // Running the command
                SqlCommand sqlCommand = new SqlCommand("SportsData_GetAgeCategoryDefinitions", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                // Open the connection
                sqlConnection.Open();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            AgeCategoryDefinition definition = new AgeCategoryDefinition
                            {
                                DefinitionId = Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]),
                                DefinitionName = sqlDataReader["AgeCategoryDefinition"].ToString()
                            };
                            definitions.Add(definition);
                        }
                    }
                }
            }

            return definitions;
        }

        public bool ChangeCustomerJobStatus(int jobId)
        {
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("SportsData_CustomerJobs_ChangeStatus", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@JobId", jobId));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                sqlCommand.Prepare();

                int deactivated = sqlCommand.ExecuteNonQuery();

                if (deactivated == 1)
                {
                    sqlCommand.Parameters.Clear();
                    sqlCommand.Dispose();

                    sqlConnection.Close();

                    return true;
                }

                sqlCommand.Parameters.Clear();
                sqlCommand.Dispose();

                sqlConnection.Close();
            }

            return false;
        }

        public void SetCustomerJob(int jobId, int customerId, int sportId, int ageCategoryId, List<Tournament> tournaments)
        {
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("SportsData_CustomerJob_Definitions_Update", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@JobId", jobId));
                sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", customerId));
                sqlCommand.Parameters.Add(new SqlParameter("@SportsId", sportId));
                sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", ageCategoryId));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();
                sqlCommand.Prepare();
                sqlCommand.ExecuteNonQuery();

                // Adding the MunicipalityId
                // Running the command
                foreach (Tournament tournamentJob in tournaments)
                {
                    using (var cmd = new SqlCommand("SportsData_CustomerJob_Tournament_Insert", sqlConnection))
                    {
                        cmd.Parameters.Add(new SqlParameter("@JobId", jobId));
                        cmd.Parameters.Add(new SqlParameter("@TournamentId", tournamentJob.TournamentId));
                        cmd.Parameters.Add(new SqlParameter("@TournamentNumber", tournamentJob.TournamentNumber));

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public List<Job> ShowJobs(int customerId)
        {
            List<Job> jobs = new List<Job>();
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("SportsData_GetTournamentsByCustomerId", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", customerId));
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        Job job = new Job();
                        job.GenderId = Convert.ToInt32(sqlDataReader["GenderId"]);
                        job.JobName = sqlDataReader["JobName"].ToString();
                        job.TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);
                        job.TournamentName = sqlDataReader["TournamentName"].ToString();
                        job.SportId = Convert.ToInt32(sqlDataReader["Sportid"].ToString());

                        if (sqlDataReader["Push"] != DBNull.Value)
                        {
                            job.Push = Convert.ToInt16(sqlDataReader["Push"]);
                        }
                        

                        jobs.Add(job);
                    }
                }
            }

            return jobs;
        }

        public CustomerJob GetCustomerJobByJobId(int jobId)
        {
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("SportsData_GetCustomerJobsByJobId", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@JobId", jobId));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return null;
                }
                CustomerJob customerJob = new CustomerJob();

                while (sqlDataReader.Read())
                {
                    customerJob.JobId = Convert.ToInt32(sqlDataReader["JobId"]);
                    customerJob.CustomerId = Convert.ToInt32(sqlDataReader["CustomerId"]);
                    customerJob.JobName = sqlDataReader["JobName"].ToString();
                    customerJob.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                    customerJob.TeamSport = Convert.ToInt32(sqlDataReader["TeamSport"]);
                    customerJob.SingleSport = Convert.ToInt32(sqlDataReader["SingleSport"]);
                 
                    if (sqlDataReader["AgeCategoryId"] != DBNull.Value)
                    {
                        customerJob.AgeCategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryId"]);
                    }

                    if (sqlDataReader["Schedule"] != DBNull.Value)
                    {
                        customerJob.Schedule = Convert.ToInt32(sqlDataReader["Schedule"]);
                    }

                }

                return customerJob;
            }
        }
    }
}