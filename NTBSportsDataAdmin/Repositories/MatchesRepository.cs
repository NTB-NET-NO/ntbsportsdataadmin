using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsDataAdmin.Application.Interfaces;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Repositories
{
    public class MatchesRepository : IRepository<Match>, IDisposable, IMatchDataMapper
    {
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int InsertOne(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Match> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Match> GetAll()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_GetMatches", sqlConnection);

                sqlCommand.CommandType = CommandType.StoredProcedure;

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                var matches = new List<Match>();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();


                if (sqlDataReader.Read())
                {
                    var match = new Match
                        {
                            MatchId = (int) sqlDataReader["MatchId"],
                            SportId = (int) sqlDataReader["SportId"],
                            HomeTeam = sqlDataReader["HomeTeam"].ToString(),
                            AwayTeam = sqlDataReader["AwayTeam"].ToString(),
                            Date = Convert.ToDateTime(sqlDataReader["Date"].ToString()),
                            TournamentId = (int) sqlDataReader["TournamentId"]
                        };
                    matches.Add(match);
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return matches.AsQueryable();
            }
        }

        public Match Get(int id)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_GetMatches", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("MatchId", id));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                var match = new Match();
                if (sqlDataReader.Read())
                {
                    match.MatchId = (int) sqlDataReader["MatchId"];
                    match.SportId = (int) sqlDataReader["SportId"];
                    match.HomeTeam = sqlDataReader["HomeTeam"].ToString();
                    match.AwayTeam = sqlDataReader["AwayTeam"].ToString();
                    match.Date = Convert.ToDateTime(sqlDataReader["Date"].ToString());
                    match.TournamentId = (int) sqlDataReader["TournamentId"];
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return match;
            }
        }

        public List<Match> GetMatchesByTournamentId(int tournamentId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_GetMatchesByTournamentId", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("Tournamentid", tournamentId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                List<Match> matches = new List<Match>();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        var match = new Match();
                        match.MatchId = (int)sqlDataReader["MatchId"];
                        match.SportId = (int)sqlDataReader["SportId"];
                        match.HomeTeam = sqlDataReader["HomeTeam"].ToString();
                        match.AwayTeam = sqlDataReader["AwayTeam"].ToString();
                        match.Date = Convert.ToDateTime(sqlDataReader["Date"].ToString());
                        match.TournamentId = (int)sqlDataReader["TournamentId"];

                        matches.Add(match);
                    }
                }
                
                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return matches;
            }
        }
    }
}