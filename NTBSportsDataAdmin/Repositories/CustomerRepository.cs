// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customers.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The customers.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using NTB.SportsDataAdmin.Domain.Classes;
using log4net;

namespace NTB.SportsDataAdmin.Application.Repositories
{
    /// <summary>
    ///     The customers.
    /// </summary>
    public class CustomerRepository : IEnumerable<Customer>
    {
        #region Private Memberes

        /// <summary>
        ///     The _customers.
        /// </summary>
        private readonly List<Customer> _customers = new List<Customer>();

        #endregion

        /// <summary>
        ///     The get enumerator.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerator" />.
        /// </returns>
// ReSharper disable FunctionRecursiveOnAllPaths
        public IEnumerator<Customer> GetEnumerator()
// ReSharper restore FunctionRecursiveOnAllPaths
        {
            return GetEnumerator();
        }

        private static readonly ILog Logger = LogManager.GetLogger(typeof (CustomerRepository));

        /// <summary>
        ///     The get enumerator.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerator" />.
        /// </returns>
// ReSharper disable RedundantNameQualifier
        IEnumerator IEnumerable.GetEnumerator()
// ReSharper restore RedundantNameQualifier
        {
            return GetEnumerator();
        }

        /// <summary>
        ///     The edit one customer.
        /// </summary>
        /// <param name="customer">
        ///     The customer.
        /// </param>
        /// <param name="customerId">
        ///     The customer id.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public bool EditOneCustomer(Customer customer, int customerId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_Customer_Update", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerName", customer.Name));

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerEmail", customer.Email));

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerAddress", customer.Address));

                    sqlCommand.Parameters.Add(new SqlParameter("@PostalCode", customer.PostalCode));

                    sqlCommand.Parameters.Add(new SqlParameter("@City", customer.City));

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                    sqlCommand.Parameters.Add(new SqlParameter("@TTCustomerId", customer.TtCustomerId));

                    sqlCommand.Parameters.Add(new SqlParameter("@MediaHouseId", customer.MediaHouseId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlConnection.Open();

                    sqlCommand.ExecuteNonQuery();

                    sqlConnection.Close();

                    return true;
                }
                catch (Exception exception)
                {
                    Logger.Info(exception.Message);
                    Logger.Info(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Info(exception.InnerException.Message);
                        Logger.Info(exception.InnerException.StackTrace);
    
                    }
                    return false;
                }
            }
        }

        /// <summary>
        ///     The add one customer.
        /// </summary>
        /// <param name="customer">
        ///     The customer.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public bool AddOneCustomer(Customer customer)
        {
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
                {
                    var sqlCommand = new SqlCommand("SportsData_Customer_Insert", sqlConnection);
                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerName", customer.Name));

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerEmail", customer.Email));

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerAddress", customer.Address));

                    sqlCommand.Parameters.Add(new SqlParameter("@PostalCode", customer.PostalCode));

                    sqlCommand.Parameters.Add(new SqlParameter("@City", customer.City));

                    sqlCommand.Parameters.Add(new SqlParameter("@TTCustomerId", customer.TtCustomerId));

                    sqlCommand.Parameters.Add(new SqlParameter("@MediaHouseId", customer.MediaHouseId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlConnection.Open();

                    sqlCommand.ExecuteNonQuery();

                    sqlConnection.Close();

                    return true;
                }
            }
            catch (Exception exception)
            {
                Logger.Info(exception.Message);
                Logger.Info(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    Logger.Info(exception.InnerException.Message);
                    Logger.Info(exception.InnerException.StackTrace);

                }
                return false;
            }
        }

        /// <summary>
        ///     The deactivate customer.
        /// </summary>
        /// <param name="customerId">
        ///     The customer id.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public bool DeactivateCustomer(int customerId)
        {
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
                {
                    var sqlCommand = new SqlCommand("SportsData_Customer_Deactivate", sqlConnection);
                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlConnection.Open();

                    sqlCommand.ExecuteNonQuery();

                    sqlConnection.Close();

                    return true;
                }
            }
            catch (Exception exception)
            {
                Logger.Info(exception.Message);
                Logger.Info(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    Logger.Info(exception.InnerException.Message);
                    Logger.Info(exception.InnerException.StackTrace);

                }
                return false;
            }
        }

        /// <summary>
        ///     The activate customer.
        /// </summary>
        /// <param name="customerId">
        ///     The customer id.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public bool ActivateCustomer(int customerId)
        {
            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
                {
                    var sqlCommand = new SqlCommand("SportsData_Customer_Activate", sqlConnection);
                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlConnection.Open();

                    int rows = sqlCommand.ExecuteNonQuery();

                    sqlConnection.Close();

                    return true;
                }
            }
            catch (Exception exception)
            {
                Logger.Info(exception.Message);
                Logger.Info(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    Logger.Info(exception.InnerException.Message);
                    Logger.Info(exception.InnerException.StackTrace);

                }

                return false;
            }
        }

        /// <summary>
        ///     GetOneCustomer returns information related to the selected customer.
        /// </summary>
        /// <param name="customerId">
        ///     Integer value holding the id of the customer we are to return information about
        /// </param>
        /// <returns>
        ///     Returns a Customer Object
        /// </returns>
        public Customer GetOneCustomer(int customerId)
        {
            var customer = new Customer();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_Customer_Get", sqlConnection);
                sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    while (sqlDataReader.Read())
                    {
                        customer.CustomerId = Convert.ToInt32(sqlDataReader["CustomerId"]);
                        customer.Name = sqlDataReader["CustomerName"].ToString(); // myReader.GetName(1);
                        customer.Address = sqlDataReader["CustomerAddress"].ToString();
                        customer.Email = sqlDataReader["CustomerEmail"].ToString();
                        customer.PostalCode = sqlDataReader["PostalCode"].ToString();
                        customer.City = sqlDataReader["City"].ToString();
                        if (sqlDataReader["MediaHouseId"] != DBNull.Value)
                        {
                            customer.MediaHouseId = Convert.ToInt32(sqlDataReader["MediaHouseId"]);
                        }

                        if (sqlDataReader["TTCustomerId"] != DBNull.Value)
                        {
                            customer.TtCustomerId = Convert.ToInt32(sqlDataReader["TTCustomerId"]);
                        }
                    }

                    sqlDataReader.Close();
                }
                sqlConnection.Close();
            }

            return customer;
        }

        /// <summary>
        ///     The get all customers.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Customer> GetAllCustomers()
        {
            using (
                var myConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                // @Done: Move this code to the database
                var myCommand = new SqlCommand("SportsData_Customer_Get", myConnection);

                // Sending a null parameter just so that we are sure it has a null value
                myCommand.Parameters.Add(new SqlParameter("@CustomerId", DBNull.Value));
                myCommand.CommandType = CommandType.StoredProcedure;

                myConnection.Open();

                using (SqlDataReader myReader = myCommand.ExecuteReader())
                {
                    while (myReader.Read())
                    {
                        var myCustomer = new Customer
                            {
                                CustomerId = Convert.ToInt32(myReader["CustomerId"]),
                                Name = myReader["CustomerName"].ToString(),
                                Address = myReader["CustomerAddress"].ToString(),
                                Email = myReader["CustomerEmail"].ToString(),
                                City = myReader["City"].ToString(),
                                PostalCode = myReader["PostalCode"].ToString(),
                                Active = Convert.ToInt32(myReader["Activated"]),
                            };

                        if (myReader["TTCustomerId"] != DBNull.Value)
                        {
                            myCustomer.TtCustomerId = Convert.ToInt32(myReader["TTCustomerId"]);
                        }

                        _customers.Add(myCustomer);
                    }

                    myReader.Close();
                }

                myConnection.Close();
            }

            return _customers;
        }

        public List<Customer> GetCustomersByTournamentId(int tournamentId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {

                    Logger.Debug("TournamentId: " + tournamentId);

                    // Now we are to get a list of customers for this Tournament
                    var sqlCustomerCommand = new SqlCommand(
                        "SportsData_GetCustomerByTournamentId", sqlConnection);
                    sqlCustomerCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    sqlCustomerCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlCustomerCommand.Connection.State == ConnectionState.Closed)
                    {
                        sqlCustomerCommand.Connection.Open();
                    }

                    SqlDataReader dataReader = sqlCustomerCommand.ExecuteReader(CommandBehavior.CloseConnection);

                    var customers = new List<Customer>();
                    if (!dataReader.HasRows)
                    {
                        return new List<Customer>();
                    }

                    while (dataReader.Read())
                    {
                        var customer = new Customer();
                        if (dataReader["CustomerId"] != DBNull.Value)
                        {
                            customer.CustomerId = Convert.ToInt32(dataReader["CustomerId"].ToString());
                        }

                        if (dataReader["TTCustomerId"] != DBNull.Value)
                        {
                            customer.TtCustomerId = Convert.ToInt32(dataReader["TTCustomerId"].ToString());
                        }

                        // Adding to customers-list
                        customers.Add(customer);
                    }

                    return customers;

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

            }
            return null;
        }
    }
}