using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsDataAdmin.Application.Interfaces;
using NTB.SportsDataAdmin.Domain.Classes;
using log4net;

namespace NTB.SportsDataAdmin.Application.Repositories
{
    public class AgeCategoryRepository : IRepository<AgeCategory>, IDisposable, IAgeCategoryDataMapper
    {
        public int SportId { get; set; }

        public int OrgId { get; set; }

        internal static readonly ILog Logger = LogManager.GetLogger(typeof(AgeCategoryRepository));

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        private bool GetAgeCategoryById(int ageCategoryId)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {

                    var sqlCommand = new SqlCommand("SportsData_GetAgeCategoryById", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", ageCategoryId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    if (sqlDataReader.HasRows)
                    {
                        if (sqlConnection.State == ConnectionState.Open)
                        {
                            sqlConnection.Close();
                        }

                        return true;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);

                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            return false;
        }

        public int InsertOne(AgeCategory domainobject)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                if (GetAgeCategoryById(domainobject.CategoryId))
                {
                    return 0;
                }

                var sqlCommand = new SqlCommand("SportsData_InsertFederationAgeCategories", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@CategoryId", domainobject.CategoryId));
                sqlCommand.Parameters.Add(new SqlParameter("@MaxAge", domainobject.MaxAge));
                sqlCommand.Parameters.Add(new SqlParameter("@MinAge", domainobject.MinAge));
                sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryDefinitionId",
                                                           domainobject.AgeCategoryDefinitionId));
                sqlCommand.Parameters.Add(new SqlParameter("@CategoryName", domainobject.CategoryName));
                sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                sqlCommand.Parameters.Clear();
                sqlCommand.CommandText = "SELECT @@IDENTITY";
                sqlCommand.CommandType = CommandType.Text;

                object o = sqlCommand.ExecuteScalar();
                int insertId = 0;
                if (o != DBNull.Value)
                {
                    int id1 = 0;
                    if (Int32.TryParse(o.ToString(), out id1))
                    {
                        insertId = id1 > 0 ? id1 : 0;
                    }
                }

                sqlCommand.Dispose();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return insertId;
            }
        }

        public void InsertAll(List<AgeCategory> domainobject)
        {
            foreach (AgeCategory ageCategory in domainobject)
            {
                InsertOne(ageCategory);
            }
        }

        public void Update(AgeCategory domainobject)
        {
            
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_UpdateFederationAgeCategories", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    sqlCommand.Parameters.Add(new SqlParameter("@CategoryId", domainobject.CategoryId));
                    sqlCommand.Parameters.Add(new SqlParameter("@MaxAge", domainobject.MaxAge));
                    sqlCommand.Parameters.Add(new SqlParameter("@MinAge", domainobject.MinAge));
                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryDefinitionId",
                                                               domainobject.AgeCategoryDefinitionId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);

                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void Delete(AgeCategory domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<AgeCategory> GetAll()
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_GetFederationAgeCategories", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    // This one shall return all, so we send null as parameter to the SP.
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", DBNull.Value));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    var ageCategories = new List<AgeCategory>();

                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var ageCategory = new AgeCategory
                                {
                                    CategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryID"]),
                                    CategoryName = sqlDataReader["AgeCategoryName"].ToString(),
                                    MinAge = Convert.ToInt32(sqlDataReader["MinAge"]),
                                    MaxAge = Convert.ToInt32(sqlDataReader["MaxAge"]),
                                    AgeCategoryDefinitionId = Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]),
                                    SportId = Convert.ToInt32(sqlDataReader["SportId"])
                                };

                            ageCategories.Add(ageCategory);
                        }
                    }

                    return ageCategories.AsQueryable();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);

                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return null;
        }


        public AgeCategory Get(int id)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[SportsData_GetAgeCategoryById]", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    var ageCategory = new AgeCategory();

                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            ageCategory.CategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryID"]);
                            ageCategory.CategoryName = sqlDataReader["AgeCategoryName"].ToString();
                            ageCategory.MinAge = Convert.ToInt32(sqlDataReader["MinAge"]);
                            ageCategory.MaxAge = Convert.ToInt32(sqlDataReader["MaxAge"]);
                            ageCategory.AgeCategoryDefinitionId =
                                Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]);
                            ageCategory.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        }
                    }

                    return ageCategory;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);

                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            return null;
        }

        public void DeleteAll(int sportId)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_DeleteFederationAgeCategories", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);

                    }
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

            }
        }

        public List<AgeCategory> GetAgeCategoryBySportId(int sportId)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand("SportsData_GetFederationAgeCategories", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                var ageCategories = new List<AgeCategory>();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        var ageCategory = new AgeCategory
                        {
                            CategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryID"]),
                            CategoryName = sqlDataReader["AgeCategoryName"].ToString(),
                            MinAge = Convert.ToInt32(sqlDataReader["MinAge"]),
                            MaxAge = Convert.ToInt32(sqlDataReader["MaxAge"]),
                            AgeCategoryDefinitionId = Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]),
                            SportId = Convert.ToInt32(sqlDataReader["SportId"])
                        };

                        ageCategories.Add(ageCategory);
                    }
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return ageCategories;
            }
        }
    }
}