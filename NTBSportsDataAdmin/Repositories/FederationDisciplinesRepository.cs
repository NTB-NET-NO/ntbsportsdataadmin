using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsDataAdmin.Application.Interfaces;
using NTB.SportsDataAdmin.Domain.Classes;

namespace NTB.SportsDataAdmin.Application.Repositories
{
    public class FederationDisciplinesRepository : IRepository<FederationDiscipline>, IDisposable
    {
        public int OrgId { get; set; }

        public void Dispose()
        {
            throw new NotImplementedException();
        }


        public int InsertOne(FederationDiscipline domainobject)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand("SportsData_InsertFederationDisciplines", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@OrgId", OrgId));

                sqlCommand.Parameters.Add(new SqlParameter("@DisciplineId", Convert.ToInt32(domainobject.ActivityId)));

                sqlCommand.Parameters.Add(new SqlParameter("@DisciplineName", domainobject.ActivityName));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                // Running the SQL Query
                sqlCommand.ExecuteNonQuery();

                sqlCommand.Parameters.Clear();
                
                sqlCommand.Dispose();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return 1;
            }
        }

        public void InsertAll(List<FederationDiscipline> domainobject)
        {
            foreach (FederationDiscipline discipline in domainobject)
            {
                InsertOne(discipline);
            }
        }

        public void Update(FederationDiscipline domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(FederationDiscipline domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<FederationDiscipline> GetAll()
        {
            throw new NotImplementedException();
        }

        public FederationDiscipline Get(int id)
        {
            throw new NotImplementedException();
        }

        public void DeleteAll()
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand("SportsData_DeleteFederationDisciplines", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@OrgId", OrgId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        public IQueryable<FederationDiscipline> Search()
        {
            throw new NotImplementedException();
        }
    }
}