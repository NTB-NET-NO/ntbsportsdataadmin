﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using NTB.SportsDataAdmin.Application.Interfaces;
using NTB.SportsDataAdmin.Domain.Classes;
using log4net;

namespace NTB.SportsDataAdmin.Application.Repositories
{
    public class CountryRepository : IRepository<Country>, IDisposable, ICountryRepository
    {

        private static readonly ILog Logger = LogManager.GetLogger(typeof(CountryRepository));

        public int InsertOne(Country domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Country> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Country domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Country domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Country> GetAll()
        {
            var countries = new List<Country>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                try
                {
                    // @Done: Move this code to the database
                    var sqlCommand = new SqlCommand("SportsData_GetAllCountries", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();

                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    while (sqlDataReader.Read())
                    {
                        var country = new Country();
                        country.Id = sqlDataReader["Id"].ToString();
                        country.Name = sqlDataReader["Name"].ToString();

                        countries.Add(country);
                    }

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }

                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return countries.AsQueryable();
            }
        }

        public Country Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}