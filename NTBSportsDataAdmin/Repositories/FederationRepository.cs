using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsDataAdmin.Application.Interfaces;
using NTB.SportsDataAdmin.Domain.Classes;
using log4net;

namespace NTB.SportsDataAdmin.Application.Repositories
{
    public class FederationRepository : IRepository<Federation>, IDisposable, IFederationDataMapper
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof (FederationRepository));

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int InsertOne(Federation domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_InsertNewSportsTable", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.BranchId));

                sqlCommand.Parameters.Add(new SqlParameter("@SportName", domainobject.BranchName));

                sqlCommand.Parameters.Add(new SqlParameter("@SingleSport", domainobject.HasIndividualResults));

                sqlCommand.Parameters.Add(new SqlParameter("@TeamSport", domainobject.HasTeamResults));

                sqlCommand.Parameters.Add(new SqlParameter("@OrgId", domainobject.FederationId));

                sqlCommand.Parameters.Add(new SqlParameter("@OrgName", domainobject.FederationName));

                sqlConnection.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                int insertId = 0;

                if (!sqlDataReader.HasRows)
                {
                    return insertId;
                }

                
                while (sqlDataReader.Read())
                {
                    insertId = Convert.ToInt32(sqlDataReader["InsertId"]);
                }
                
                sqlCommand.Dispose();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return insertId;
            }
        }

        public void InsertAll(List<Federation> domainobject)
        {
            foreach (Federation federation in domainobject)
            {
                InsertOne(federation);
            }
        }

        public void Update(Federation domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Federation domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Federation> GetAll()
        {
            throw new NotImplementedException();
        }

        public Federation Get(int id)
        {
            // Get federation id
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_GetFederationById", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@FederationId", id));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                Federation federation = new Federation();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        federation.FederationId = Convert.ToInt32(sqlDataReader["OrgId"]);
                        federation.FederationName = sqlDataReader["OrgName"].ToString();
                        federation.BranchId = Convert.ToInt32(sqlDataReader["SportId"]);
                    }
                }

                return federation;
            }
        }

        public IQueryable<Federation> Search()
        {
            throw new NotImplementedException();
        }

        public Federation GetFederationBySportId(int sportId)
        {
            // []
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["SportsDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("SportsData_GetOrganizationBySportId", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                Federation federation = new Federation();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        federation.FederationId = Convert.ToInt32(sqlDataReader["OrgId"]);
                        federation.FederationName = sqlDataReader["OrgName"].ToString();
                        federation.BranchId = Convert.ToInt32(sqlDataReader["SportId"]);
                    }
                }

                return federation;
            }
        }
    }
}