﻿using System.Collections.Generic;
using NTB.SportsDataAdmin.Application.Fakes;
using NTB.SportsDataAdmin.Application.Models.NIF.Classes;
using NTB.SportsDataAdmin.Application.Models.NTB.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Linq;

namespace NTB.SportsDataAdmin.Tests
{
    
    
    /// <summary>
    ///This is a test class for RegionRepositoryTest and is intended
    ///to contain all RegionRepositoryTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RegionRepositoryTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetAll
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void GetAllTest_TestingType()
        {
            RegionRepositoryFake target = new RegionRepositoryFake(); // TODO: Initialize to an appropriate value

            List<Region> expectedList = new List<Region>()
                {
                    new Region
                    {
                        CountyId = 1,
                        RegionId = 1,
                        RegionName = "Eidsberg",
                        Selected = false
                    }
                };

            
            target.GetList = new List<Region>(){
                new Region
                {
                    CountyId = 1,
                    RegionId = 1,
                    RegionName = "Eidsberg",
                    Selected = false
                }
            };
            IQueryable<Region> expected = expectedList.AsQueryable(); // TODO: Initialize to an appropriate value
            IQueryable<Region> actual = target.GetAll();
            Assert.IsInstanceOfType(actual, typeof (IQueryable)); // .AreEqual(expected, actual);
            // Assert.Inconclusive("Verify the correctness of this test method.");
        }

        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void GetAllTest_TestingNotNull()
        {
            RegionRepositoryFake target = new RegionRepositoryFake(); // TODO: Initialize to an appropriate value

            List<Region> expectedList = new List<Region>()
                {
                    new Region
                    {
                        CountyId = 1,
                        RegionId = 1,
                        RegionName = "Eidsberg",
                        Selected = false
                    }
                };


            target.GetList = new List<Region>(){
                new Region
                {
                    CountyId = 1,
                    RegionId = 1,
                    RegionName = "Eidsberg",
                    Selected = false
                }
            };
            IQueryable<Region> expected = expectedList.AsQueryable(); // TODO: Initialize to an appropriate value
            IQueryable<Region> actual = target.GetAll();

            Assert.IsNotNull(actual);
            // Assert.IsInstanceOfType(actual, typeof(IQueryable)); // .AreEqual(expected, actual);
            // Assert.Inconclusive("Verify the correctness of this test method.");
        }

        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void GetAllTest_TestingContent()
        {
            RegionRepositoryFake target = new RegionRepositoryFake(); // TODO: Initialize to an appropriate value

            List<Region> expectedList = new List<Region>()
                {
                    new Region
                    {
                        CountyId = 1,
                        RegionId = 1,
                        RegionName = "Eidsberg",
                        Selected = false
                    }
                };


            target.GetList = new List<Region>(){
                new Region
                {
                    CountyId = 1,
                    RegionId = 1,
                    RegionName = "Eidsberg",
                    Selected = false
                }
            };
            int expected = 1; // TODO: Initialize to an appropriate value
            int actual = target.GetAll().Count();

            Assert.AreEqual(expected,actual);
            // Assert.IsInstanceOfType(actual, typeof(IQueryable)); // .AreEqual(expected, actual);
            // Assert.Inconclusive("Verify the correctness of this test method.");
        }
        /*
         * RegionsDataAccess target = new RegionsDataAccess(); // TODO: Initialize to an appropriate value
            int jobId = 189; // TODO: Initialize to an appropriate value
            
            int expected = 434 ; // TODO: Initialize to an appropriate value
            int actual = target.GetRegions(jobId).Count;
            Assert.AreEqual(expected, actual);
         */

        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void GetAllTest_LiveTest_GetNumberOfRegions()
        {
            RegionRepository target = new RegionRepository {JobId = 189}; // TODO: Initialize to an appropriate value

            const int expected = 434; // TODO: Initialize to an appropriate value
            int actual = target.GetAll().Count();

            Assert.AreEqual(expected, actual);
            // Assert.IsInstanceOfType(actual, typeof(IQueryable)); // .AreEqual(expected, actual);
            // Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Get
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void GetTest_TestingLiveData_ReturningOne()
        {
            RegionRepository target = new RegionRepository(); // TODO: Initialize to an appropriate value
            int id = 500030; // TODO: Initialize to an appropriate value
            int expected = 500030;
            int actual = target.Get(id).RegionId;
            Assert.AreEqual(expected, actual);
            
        }
    }
}
