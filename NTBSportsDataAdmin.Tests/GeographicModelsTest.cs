﻿using NTB.SportsDataAdmin.Application.Models.NIF;
using NTB.SportsDataAdmin.Application.Models.NIF.Classes;
using NTB.SportsDataAdmin.Application.Models.NTB.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Collections.Generic;

namespace NTB.SportsDataAdmin.Tests
{
    
    
    /// <summary>
    ///This is a test class for GeographicModelsTest and is intended
    ///to contain all GeographicModelsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class GeographicModelsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GeographicModels Constructor
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void GeographicModelsConstructorTest()
        {
            GeographicModels target = new GeographicModels();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for GetDetailsJobView
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void GetDetailsJobViewTest()
        {
            GeographicModels target = new GeographicModels(); // TODO: Initialize to an appropriate value
            DetailsJobView expected = null; // TODO: Initialize to an appropriate value
            DetailsJobView actual;
            actual = target.GetDetailsJobView();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for AgeCategoryId
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void AgeCategoryIdTest()
        {
            GeographicModels target = new GeographicModels(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.AgeCategoryId = expected;
            actual = target.AgeCategoryId;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CustomerId
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void CustomerIdTest()
        {
            GeographicModels target = new GeographicModels(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.CustomerId = expected;
            actual = target.CustomerId;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for FederationId
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void FederationIdTest()
        {
            GeographicModels target = new GeographicModels(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.FederationId = expected;
            actual = target.FederationId;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for JobId
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void JobIdTest()
        {
            GeographicModels target = new GeographicModels(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.JobId = expected;
            actual = target.JobId;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for SeasonId
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void SeasonIdTest()
        {
            GeographicModels target = new GeographicModels(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.SeasonId = expected;
            actual = target.SeasonId;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for SportId
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void SportIdTest()
        {
            GeographicModels target = new GeographicModels(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.SportId = expected;
            actual = target.SportId;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for UseRemoteData
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void UseRemoteDataTest()
        {
            GeographicModels target = new GeographicModels(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            target.UseRemoteData = expected;
            bool actual = target.UseRemoteData;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetSelectedMunicipalities
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        [DeploymentItem("NTBSportsDataAdmin.dll")]
        public void GetSelectedMunicipalitiesTest()
        {
            GeographicModels_Accessor target = new GeographicModels_Accessor(); // TODO: Initialize to an appropriate value

            RegionRepository repository = new RegionRepository
                {
                    JobId = 189
                };
            List<Region> regions = new List<Region>(repository.GetAll()); // TODO: Initialize to an appropriate value
            const int expected = 415; // TODO: Initialize to an appropriate value
            int actual = target.GetSelectedMunicipalities(regions).Count;
            Assert.AreEqual(expected, actual);
        }
    }
}
