﻿using NTB.SportsDataAdmin.Application.Models.NTB.Classes;
using NTB.SportsDataAdmin.Application.Models.NTB.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Linq;

namespace NTB.SportsDataAdmin.Tests
{
    
    
    /// <summary>
    ///This is a test class for CalendarRepositoryTest and is intended
    ///to contain all CalendarRepositoryTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CalendarRepositoryTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetAll
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void GetAllTest_Soccer()
        {
            CalendarRepository target = new CalendarRepository
                {
                    SportId = 16
                }; // TODO: Initialize to an appropriate value
            const int expected = 91469;
            int actual = target.GetAll().Count();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetAll
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void GetAllTest_All()
        {
            CalendarRepository target = new CalendarRepository();
            
            const int expected = 96247;
            int actual = target.GetAll().Count();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetAll
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void GetAllTest_Soccer_From_To_Date()
        {
            CalendarRepository target = new CalendarRepository
                {
                    EndDate = DateTime.Today,
                    StartDate = DateTime.Parse("2013-01-01"),
                    SportId = 16
                };

            const int expected = 79444;
            int actual = target.GetAll().Count();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetAll
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void GetAllTest_Soccer_Today()
        {
            CalendarRepository target = new CalendarRepository
            {
                EndDate = DateTime.Today,
                StartDate = DateTime.Today,
                SportId = 16
            };

            const int expected = 8;
            int actual = target.GetAll().Count();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Get
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void GetTest()
        {
            CalendarRepository target = new CalendarRepository(); // TODO: Initialize to an appropriate value
            const int id = 5714202; // TODO: Initialize to an appropriate value
            Calendar calendar = new Calendar();
            int expected = 5714202; // TODO: Initialize to an appropriate value
            int actual = target.Get(id).CalendarId;
            Assert.AreEqual(expected, actual);
            
        }

        /// <summary>
        ///A test for Get
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        public void GetTest_isNotNull()
        {
            CalendarRepository target = new CalendarRepository(); // TODO: Initialize to an appropriate value
            const int id = 5714202; // TODO: Initialize to an appropriate value
            
            Calendar actual = target.Get(id);
            Assert.IsNotNull(actual);

        }
    }
}
