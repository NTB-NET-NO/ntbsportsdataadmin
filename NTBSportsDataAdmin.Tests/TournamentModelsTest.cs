﻿using NTB.SportsDataAdmin.Application.Models.NTB.Classes;
using NTB.SportsDataAdmin.Application.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Collections.Generic;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using TestContext = Microsoft.VisualStudio.TestTools.UnitTesting.TestContext;

namespace NTB.SportsDataAdmin.Tests
{
    
    
    /// <summary>
    ///This is a test class for TournamentModelsTest and is intended
    ///to contain all TournamentModelsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class TournamentModelsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        

        

        /// <summary>
        ///A test for GetMunicipalityListForNifSports
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("%PathToWebRoot%\\NTBSportsDataAdmin\\NTBSportsDataAdmin", "/")]
        [UrlToTest("http://localhost:50842/")]
        [DeploymentItem("NTBSportsDataAdmin.dll")]
        public void GetMunicipalityListForNifSportsTest_StringEmtpy()
        {
            string selectedMunicipalities = string.Empty;
            const int jobId = 0; 
            List<Municipality> actual = TournamentModels_Accessor.GetMunicipalityListForNifSports(selectedMunicipalities, jobId);
            Assert.IsNotNull(actual);
        }
    }
}
