﻿using System.Collections.Generic;
using NTB.SportsDataAdmin.Services.Profixio.ProfixioServiceReference;

namespace NTB.SportsDataAdmin.Services.Profixio.Interfaces
{
    public interface IClubDataMapper
    {
        List<TournamentClub> GetClubsByTournament(int tournamentId);
        List<MatchGroup> GetTournamentByClubs(List<int> clubIds, int tournamentId);
    }
}
