﻿using System.Collections.Generic;
using NTB.SportsDataAdmin.Services.Profixio.ProfixioServiceReference;

namespace NTB.SportsDataAdmin.Services.Profixio.Interfaces
{
    public interface ITournamentDataMapper
    {
        List<MatchGroup> GetTournamentBySeasonId(int seasonId);
    }
}
