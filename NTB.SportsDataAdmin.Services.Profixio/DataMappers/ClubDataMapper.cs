﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsDataAdmin.Services.Profixio.Interfaces;
using NTB.SportsDataAdmin.Services.Profixio.ProfixioServiceReference;

namespace NTB.SportsDataAdmin.Services.Profixio.DataMappers
{
    public class ClubDataMapper : IClubDataMapper
    {
        public List<TournamentClub> GetClubsByTournament(int tournamentId)
        {
            var applicationKey = ConfigurationManager.AppSettings["NorwayCupApplicationKey"];

            if (applicationKey == string.Empty)
            {
                throw new Exception("Application Key is not set in configuration. Correct and try again");
            }

            var client = new ForTournamentPortClient();
            return client.getClubs(applicationKey, tournamentId, null).ToList();
        }

        public List<MatchGroup> GetTournamentByClubs(List<int> clubIds, int tournamentId)
        {
            var applicationKey = ConfigurationManager.AppSettings["NorwayCupApplicationKey"];

            if (applicationKey == string.Empty)
            {
                throw new Exception("Application Key is not set in configuration. Correct and try again");
            }

            var client = new ForTournamentPortClient();

            var teams = client.getTeams(applicationKey, tournamentId, null, null);

            var matchClasses = client.getMatchClasses(applicationKey, tournamentId.ToString());

            var foundTeams = new List<TournamentTeam>();
            foreach (int clubId in clubIds)
            {
                foundTeams.AddRange(teams.Where(c=>c.clubid==clubId));
            }

            var matchGroups = new List<MatchGroup>();
            foreach (var team in foundTeams)
            {
                var filteredClasses = matchClasses.SingleOrDefault(c => c.id == team.MatchClassId);

                if (filteredClasses != null)
                {
                    var group = filteredClasses.MatchGroups.SingleOrDefault(g => g.id == team.MatchGroupId);

                    if (group != null)
                    {
                        var myMatchGroup = new MatchGroup
                        {
                            id = group.id,
                            IsPlayoff = group.IsPlayoff,
                            MatchClassId = filteredClasses.id.ToString(),
                            Name = filteredClasses.Name + " G-" + group.Name,
                            PlayoffId = group.PlayoffId
                        };
                        matchGroups.Add(myMatchGroup);
                    }
                }
            }

            return matchGroups;
        }
    }
}
