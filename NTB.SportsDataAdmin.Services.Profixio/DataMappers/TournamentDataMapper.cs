﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using NTB.SportsDataAdmin.Services.Profixio.Interfaces;
using NTB.SportsDataAdmin.Services.Profixio.ProfixioServiceReference;

namespace NTB.SportsDataAdmin.Services.Profixio.DataMappers
{
    public class TournamentDataMapper : ITournamentDataMapper
    {
        public List<MatchGroup> GetTournamentBySeasonId(int seasonId)
        {
            var applicationKey = ConfigurationManager.AppSettings["NorwayCupApplicationKey"];

            var client = new ForTournamentPortClient();

            var matchClasses = client.getMatchClasses(applicationKey, seasonId.ToString());

            
            return (from matchClass in matchClasses
                from @group in matchClass.MatchGroups
                select new MatchGroup
                {
                    id = @group.id, IsPlayoff = @group.IsPlayoff, MatchClassId = @group.MatchClassId, Name = matchClass.Name + " G-" + @group.Name, PlayoffId = @group.PlayoffId
                }).ToList();
        }
    }
}
