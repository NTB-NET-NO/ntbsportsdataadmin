namespace NTB.SportsDataAdmin.Domain.Classes
{
    public class Tournament
    {
        public int TournamentId { get; set; }

        public int JobId { get; set; }

        public string TournamentName { get; set; }

        public string TournamentNumber { get; set; }

        public int TournamentTypeId { get; set; }

        public int AgeCategoryId { get; set; }

        public int DistrictId { get; set; }

        public int GenderId { get; set; }

        public int SportId { get; set; }

        public int SeasonId { get; set; }

        public bool Push { get; set; }

        public int TournamentAccepted { get; set; }

        public bool TournamentSelected { get; set; }

        public int AgeCategoryFilterId { get; set; }

        public int MunicipalityId { get; set; }

        public int Division { get; set; }

        public int AgeCategoryDefinitionId { get; set; }

        public int DisciplineId { get; set; }
    }
}