// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentTools.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the ToolsTournament type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace NTB.SportsDataAdmin.Domain.Classes
{
    /// <summary>
    /// The tools tournament.
    /// </summary>
    public class TournamentTools
    {
        /// <summary>
        /// Gets or sets the tournament age categories.
        /// </summary>
        public List<AgeCategory> TournamentAgeCategories { get; set; }

        /// <summary>
        /// Gets or sets the date start.
        /// </summary>
        public DateTime DateStart { get; set; }

        /// <summary>
        /// Gets or sets the date end.
        /// </summary>
        public DateTime DateEnd { get; set; }
        
        /// <summary>
        /// Gets or sets the matches.
        /// </summary>
        public List<Match> Matches { get; set; }

        /// <summary>
        /// Gets or sets the districts.
        /// </summary>
        public List<District> Districts { get; set; }
    }
}