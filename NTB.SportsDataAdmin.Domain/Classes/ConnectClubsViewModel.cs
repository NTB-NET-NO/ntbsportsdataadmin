﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsDataAdmin.Domain.Classes
{
    public class ConnectClubsViewModel
    {
        /// <summary>
        /// Gets or sets Organization
        /// </summary>
        public Organization Organization { get; set; }

        /// <summary>
        /// Gets or sets List of Organizations
        /// </summary>
        public List<Organization> Organizations { get; set; }
    }
}
