namespace NTB.SportsDataAdmin.Domain.Classes
{
    public class Team
    {
        public int AgeCategoryId { get; set; }
        public int ClubId { get; set; }
        public int GenderId { get; set; }
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public string TeamNameInTournament { get; set; }
        
        
    }
}