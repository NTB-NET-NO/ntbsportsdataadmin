﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsDataAdmin.Domain.Classes
{
    public class Player
    {
        public int PlayerId { get; set; }

        public string PlayerName { get; set; }
    }
}
