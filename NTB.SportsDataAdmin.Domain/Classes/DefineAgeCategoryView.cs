﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsDataAdmin.Domain.Classes
{
    public class DefineAgeCategoryView
    {
        public List<AgeCategoryDefinition> AgeCategoryDefinitions { get; set; }

        public List<Season> Seasons { get; set; }
    }
}
