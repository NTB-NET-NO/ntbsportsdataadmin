﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsDataAdmin.Domain.Classes
{
    public class DetailsView
    {
        public Customer Customer { get; set; }
        public List<Sport> Sports { get; set; }
        public List<AgeCategoryDefinition> AgeCategoryDefinitions { get; set; }
        public List<Job> Jobs { get; set; }
    }
}
