﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsDataAdmin.Domain.Classes
{
    public class OrganizationDetail
    {
        /// <summary>
        /// Gets or sets organization
        /// </summary>
        public Organization Org { get; set; }

        /// <summary>
        /// Gets or sets seasons for organization
        /// </summary>
        public List<Season> OrgSeasons { get; set; }

        /// <summary>
        /// Gets or sets a list of clubs
        /// </summary>
        public List<Club> Clubs { get; set; }
    }
}
