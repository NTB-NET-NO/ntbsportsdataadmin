﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsDataAdmin.Domain.Classes
{
    public class DefineView
    {
        public DetailsView DetailsView { get; set; }
        public CustomerJob CustomerJob { get; set; }
        public IEnumerable<Season> Seasons { get; set; }
    }
}
