// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Match.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The matches.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System;

namespace NTB.SportsDataAdmin.Domain.Classes
{
    /// <summary>
    /// The matches.
    /// </summary>
    public class Match
    {
        /// <summary>
        /// Gets or sets the match id.
        /// </summary>
        public int MatchId { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the home team.
        /// </summary>
        public string HomeTeam { get; set; }

        /// <summary>
        /// Gets or sets the away team.
        /// </summary>
        public string AwayTeam { get; set; }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the tournament id.
        /// </summary>
        public int TournamentId { get; set; }

        /// <summary>
        ///     Gets or sets the Tournament name
        /// </summary>
        public string TournamentName { get; set; }

        /// <summary>
        ///     Gets or sets the tournament age category id
        /// </summary>
        public int TournamentAgeCategoryId { get; set; }
        
    }
}