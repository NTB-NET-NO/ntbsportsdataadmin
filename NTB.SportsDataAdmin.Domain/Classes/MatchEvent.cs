﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsDataAdmin.Domain.Classes
{
    public class MatchEvent
    {
        public int MatchEventTypeId { get; set; }
        public string MatchEventType { get; set; }
        public string MatchEventName { get; set; }
        
    }
}
