﻿using System.Collections.Generic;

namespace NTB.SportsDataAdmin.Domain.Classes
{
    public class CreateSeasonView
    {
        public int OrgId { get; set; }
        public Season Season { get; set; }

        public List<Sport> Sports { get; set; }
    }
}
