﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsDataAdmin.Domain.Classes
{
    public class SelectedSeasonDetailModel
    {

        public Season Season { get; set; }

        public Organization Organization { get; set; }

        public List<Club> Clubs { get; set; }
        
    }
}
