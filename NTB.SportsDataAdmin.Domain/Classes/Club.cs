﻿
namespace NTB.SportsDataAdmin.Domain.Classes
{
    public class Club
    {
        /// <summary>
        /// Gets or sets the ClubId
        /// </summary>
        public int ClubId { get; set; }
        
        /// <summary>
        /// Gets or sets the ClubName
        /// </summary>
        public string ClubName { get; set; }
        
        /// <summary>
        /// Gets or sets the district id
        /// </summary>
        public int DistrictId { get; set; }

        /// <summary>
        /// Gets or sets the district name
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        /// Gets or sets the MunicipalityId
        /// </summary>
        public int MunicipalityId { get; set; }

        /// <summary>
        /// Gets or sets the parent organization id
        /// </summary>
        public int ParentOrgId { get; set; }

        /// <summary>
        /// Gets or sets the municipality name
        /// </summary>
        public string MunicipalityName { get; set; }

        /// <summary>
        /// Gets or sets the country Code
        /// </summary>
        public string CountryCode { get; set; }
    }
}
