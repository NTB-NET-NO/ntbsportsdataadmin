﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsDataAdmin.Domain.Classes
{
    public class Country
    {
        /// <summary>
        /// Gets or sets the id of the country
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the country
        /// </summary>
        public string Name { get; set; }
    }
}
