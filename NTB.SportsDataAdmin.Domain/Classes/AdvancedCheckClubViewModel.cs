﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsDataAdmin.Domain.Classes
{
    public class AdvancedCheckClubViewModel
    {
        public List<Municipality> Municipalities { get; set; }

        public List<District> Districts { get; set; }

        public List<Country>  Countries { get; set; }

        public List<Club> Clubs { get; set; }
    }
}
