﻿using Glue;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Services.NIF.NIFProdService;
using NTBSportsDataAdmin.Common;

namespace NTB.SportsDataAdmin.Facade.Mappers.NIF
{
    public class DistrictMapper : BaseMapper<Region, District>
    {
        protected override void SetUpMapper(Mapping<Region, District> mapper)
        {
            mapper.Relate(x => x.RegionId, y => y.DistrictId);
            mapper.Relate(x => x.RegionName, y => y.DistrictName);
        }
    }
}
