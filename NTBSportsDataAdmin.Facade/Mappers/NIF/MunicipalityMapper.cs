﻿using Glue;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Services.NIF.NIFProdService;
using NTBSportsDataAdmin.Common;

namespace NTB.SportsDataAdmin.Facade.Mappers.NIF
{
    public class MunicipalityMapper : BaseMapper<Region, Municipality>
    {
        protected override void SetUpMapper(Mapping<Region, Municipality> mapper)
        {
            mapper.Relate(x => x.ParentRegionId, y => y.DistrictId);
            mapper.Relate(x => x.RegionId, y => y.MunicipalityId);
            mapper.Relate(x => x.RegionName, y => y.MuniciaplityName);
        }
    }
}
