using Glue;
using NTB.SportsDataAdmin.Domain.Classes;
using NTBSportsDataAdmin.Common;

namespace NTB.SportsDataAdmin.Facade.Mappers.NIF
{
    public class SportMapper : BaseMapper<FederationDiscipline, Sport>
    {
        protected override void SetUpMapper(Mapping<FederationDiscipline, Sport> mapper)
        {
            mapper.Relate(x => x.ActivityName, y=>y.Name);
            mapper.Relate(x => x.ActivityId, y => y.Id);
        }
    }
}


