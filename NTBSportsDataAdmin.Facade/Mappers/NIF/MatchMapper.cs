﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Glue;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Services.NIF.NIFProdService;
using NTBSportsDataAdmin.Common;

namespace NTB.SportsDataAdmin.Facade.Mappers.NIF
{
    public class MatchMapper : BaseMapper<TournamentMatchExtended, Match>
    {
        protected override void SetUpMapper(Mapping<TournamentMatchExtended, Match> mapper)
        {
            mapper.Relate(x => x.Awayteam, y => y.AwayTeam);
            mapper.Relate(x => x.Hometeam, y => y.HomeTeam);
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.MatchDate, y => y.Date);
        }
    }
}
