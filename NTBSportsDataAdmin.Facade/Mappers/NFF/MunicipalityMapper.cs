using Glue;
using NTB.SportsDataAdmin.Services.NFF.NFFProdService;
using NTBSportsDataAdmin.Common;

namespace NTB.SportsDataAdmin.Facade.Mappers.NFF
{
    class MunicipalityMapper : BaseMapper<Municipality, Domain.Classes.Municipality>
    {
        protected override void SetUpMapper(Mapping<Municipality, Domain.Classes.Municipality> mapper)
        {
            mapper.Relate(x => x.Id, y=> y.MunicipalityId);
            mapper.Relate(x => x.Name, y => y.MuniciaplityName);
        }
    }
}