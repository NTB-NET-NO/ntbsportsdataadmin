﻿using Glue;
using NTB.SportsDataAdmin.Services.NFF.NFFProdService;
using NTBSportsDataAdmin.Common;

namespace NTB.SportsDataAdmin.Facade.Mappers.NFF
{
    public class ClubMapper : BaseMapper<Club, Domain.Classes.Club>
    {
        protected override void SetUpMapper(Mapping<Club, Domain.Classes.Club> mapper)
        {
            mapper.Relate(x => x.ClubId, y => y.ClubId);
            mapper.Relate(x => x.ClubName, y => y.ClubName);
            mapper.Relate(x => x.DistrictId, y => y.DistrictId);
            mapper.Relate(x => x.MunicipalityId, y=> y.MunicipalityId);
        }
    }

}
