using Glue;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Services.NFF.NFFProdService;
using NTBSportsDataAdmin.Common;

namespace NTB.SportsDataAdmin.Facade.Mappers.NFF
{
    public class AgeCategoryMapper : BaseMapper<AgeCategoryTournament, AgeCategory>
    {
        protected override void SetUpMapper(Mapping<AgeCategoryTournament, AgeCategory> mapper)
        {
            mapper.Relate(x => x.AgeCategoryId, y => y.CategoryId);
            mapper.Relate(x => x.AgeCategoryName, y => y.CategoryName);
            mapper.Relate(x => x.MinAge, y => y.MinAge);
            mapper.Relate(x => x.MaxAge, y => y.MaxAge);
        }
    }
}