﻿using Glue;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Services.NIF.NIFProdService;
using NTB.SportsDataAdmin.Services.Profixio.ProfixioServiceReference;
using NTBSportsDataAdmin.Common;

namespace NTB.SportsDataAdmin.Facade.Mappers.Profixio
{
    public class ClubMapper : BaseMapper<TournamentClub, Club>
    {
        protected override void SetUpMapper(Mapping<TournamentClub, Club> mapper)
        {
            mapper.Relate(x => x.id, y => y.ClubId);
            mapper.Relate(x => x.name, y => y.ClubName);
        }
    }
}
