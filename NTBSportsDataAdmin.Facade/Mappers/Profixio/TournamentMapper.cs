﻿using Glue;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Services.NIF.NIFProdService;
using NTB.SportsDataAdmin.Services.Profixio.ProfixioServiceReference;
using NTBSportsDataAdmin.Common;

namespace NTB.SportsDataAdmin.Facade.Mappers.Profixio
{
    public class TournamentMapper : BaseMapper<MatchGroup, Domain.Classes.Tournament>
    {
        protected override void SetUpMapper(Mapping<MatchGroup, Domain.Classes.Tournament> mapper)
        {
            mapper.Relate(x => x.id, y => y.TournamentId);
            mapper.Relate(x => x.Name, y => y.TournamentName);
            mapper.Relate(x => x.MatchClassId, y=>y.TournamentNumber);
        }
    }
}