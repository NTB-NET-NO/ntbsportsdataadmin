﻿using System.Collections.Generic;
using NTB.SportsDataAdmin.Services.NIF.NIFProdService;

namespace NTB.SportsDataAdmin.Facade.NIF.TournamentInterface
{
    public interface ITournamentFacade
    {
        // Returns list
        List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId);
        List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId);
        List<Tournament> GetTournamentsByTeam(int teamId, int seasonId);
        List<Tournament> GetTournamentsBySeasonId(int seasonId);
    }
}
