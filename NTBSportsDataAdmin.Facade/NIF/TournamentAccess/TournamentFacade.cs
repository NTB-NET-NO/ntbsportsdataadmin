﻿using System.Collections.Generic;
using NTB.SportsDataAdmin.Facade.NIF.TournamentInterface;
using NTB.SportsDataAdmin.Services.NIF.NIFProdService;

namespace NTB.SportsDataAdmin.Facade.NIF.TournamentAccess
{
    public class TournamentFacade : ITournamentFacade
    {
        public TournamentFacade()
        {
        }

        public List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId)
        {
            throw new System.NotImplementedException();
        }

        public List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId)
        {
            throw new System.NotImplementedException();
        }

        public List<Tournament> GetTournamentsByTeam(int teamId, int seasonId)
        {
            throw new System.NotImplementedException();
        }

        public List<Tournament> GetTournamentsBySeasonId(int seasonId)
        {
            return new List<Tournament>();
        }
    }
}
