﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsDataAdmin.Facade.Profixio.Interfaces
{
    interface ITournamentFacade
    {
        List<Domain.Classes.Club> GetClubsByTournament(int tournamentId);
        List<Domain.Classes.Tournament> GetTournamentByClubs(List<int> clubIds, int tournamentId);
        List<Domain.Classes.Tournament> GetTournamentsBySeasonId(int seasonId);
    }
}
