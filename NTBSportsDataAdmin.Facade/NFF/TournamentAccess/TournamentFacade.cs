﻿using System;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsDataAdmin.Domain.Classes;
using NTB.SportsDataAdmin.Facade.Mappers.NFF;
using NTB.SportsDataAdmin.Facade.NFF.TournamentInterface;
using NTB.SportsDataAdmin.Services.NFF.Interfaces;
using NTB.SportsDataAdmin.Services.NFF.Repositories;
using NTB.SportsDataAdmin.Services.NFF.NFFProdService;
using Club = NTB.SportsDataAdmin.Domain.Classes.Club;
using Tournament = NTB.SportsDataAdmin.Domain.Classes.Tournament;

namespace NTB.SportsDataAdmin.Facade.NFF.TournamentAccess
{

    public class TournamentFacade : ITournamentFacade
    {
        private readonly ITournamentDataMapper _tournamentDataMapper;
        private readonly IDistrictDataMapper _districtDataMapper;
        private readonly IMunicipalityDataMapper _municipalityDataMapper;
        private readonly ISeasonDataMapper _seasonDataMapper;
        private readonly IAgeCategoryDataMapper _ageCategoryDataMapper;
        private readonly IMatchDataMapper _matchDataMapper;
        private readonly IOrgDataMapper _orgDataMapper;

        // Using the repository interface
        private readonly IRepository<Services.NFF.NFFProdService.Municipality> _municipalityRepository;
        private readonly ITeamDataMapper _teamDataMapper;

        public TournamentFacade()
        {
            _tournamentDataMapper = new TournamentRepository();
            _districtDataMapper = new DistrictRepository();
            _municipalityDataMapper = new MunicipalityRepository();
            _seasonDataMapper = new SeasonRepository();
            _ageCategoryDataMapper = new AgeCategoryRepository();
            _teamDataMapper = new TeamRepository();
            _matchDataMapper = new MatchRepository();
            _orgDataMapper = new OrgRepository();


            // Repository code
            _municipalityRepository = new MunicipalityRepository();
        }

        public List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId)
        {
            _tournamentDataMapper.AgeCategoryId = AgeCategory;
            var result = _tournamentDataMapper.GetTournamentByMunicipalities(municipalities, seasonId);

            var mapper = new TournamentMapper();
            if (result == null)
            {
                result = new List<Services.NFF.NFFProdService.Tournament>();
            }

            return result.Select(row => mapper.Map(row, new Tournament())).ToList();

            
        }


        public List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId)
        {
            var result = _tournamentDataMapper.GetTournamentsByDistrict(districtId, seasonId);

            TournamentMapper mapper = new TournamentMapper();

            return result.Select(row => mapper.Map(row, new Domain.Classes.Tournament())).ToList();
        }

        public List<Domain.Classes.Tournament> GetTournamentsByTeam(int teamId, int seasonId)
        {
            var result = _tournamentDataMapper.GetTournamentsByTeam(teamId, seasonId);

            TournamentMapper mapper = new TournamentMapper();

            return result.Select(row => mapper.Map(row, new Domain.Classes.Tournament())).ToList();
        }


        public List<Domain.Classes.District> GetDistricts()
        {
            var districts = _districtDataMapper.GetDistricts();

            var districtMapper = new DistrictMapper();

            return districts.Select(row => districtMapper.Map(row, new Domain.Classes.District())).ToList();
        }

        public Domain.Classes.District GetDistrict(int id)
        {
            var result = _districtDataMapper.GetDistrict(id);

            var districtMapper = new DistrictMapper();

            return districtMapper.Map(result, new Domain.Classes.District());
        }

        public List<Domain.Classes.Municipality> GetMunicipalities()
        {
            var result = _municipalityRepository.GetAll();

            var municipalityMapper = new MunicipalityMapper();

            return result.Select(row => municipalityMapper.Map(row, new Domain.Classes.Municipality())).ToList();
        }

        public List<Domain.Classes.Municipality> GetMunicipalitiesbyDistrict(int districtId)
        {
            var results = _municipalityDataMapper.GetMunicipalitiesbyDistrict(districtId);

            var municipalityMapper = new MunicipalityMapper();

            return results.Select(row => municipalityMapper.Map(row, new Domain.Classes.Municipality())).ToList();
        }

        public List<Domain.Classes.Tournament> GetClubsByTournament(int tournamentId, int seasonId)
        {
            throw new NotImplementedException();
        }

        public List<Domain.Classes.Season> GetSeasons()
        {
            SeasonMapper mapper = new SeasonMapper();

            var result = _seasonDataMapper.GetSeasons();

            return result.Select(row => mapper.Map(row, new Domain.Classes.Season())).ToList();
        }

        public Domain.Classes.Season GetSeason(int id)
        {
            var result = _seasonDataMapper.GetSeason(id);

            var mapper = new SeasonMapper();

            return mapper.Map(result, new Domain.Classes.Season());
        }

        public Domain.Classes.Season GetOngoingSeason()
        {
            var result = _seasonDataMapper.GetOngoingSeason();

            var mapper = new SeasonMapper();

            return mapper.Map(result, new Domain.Classes.Season());
        }


        public List<AgeCategory> GetAgeCategoriesTournament()
        {
            var mapper = new AgeCategoryMapper();
            var result = _ageCategoryDataMapper.GetAgeCategoriesTournament();

            return result.Select(row => mapper.Map(row, new AgeCategory())).ToList();
        }

        public List<Domain.Classes.Match> GetMatchesByTournament(int tournamentId, bool includeSquad,
                                                                 bool includeReferees, bool includeResults,
                                                                 bool includeEvents)
        {
            var mapper = new MatchMapper();

            var result = _matchDataMapper.GetMatchesByTournament(tournamentId, includeSquad, includeReferees,
                                                                 includeResults, includeEvents);
            return result.Select(row => mapper.Map(row, new Domain.Classes.Match())).ToList();

        }

        public List<Club> GetClubs()
        {
            var mapper = new ClubMapper();
            var result = _orgDataMapper.GetClubs();

            return result.Select(row => mapper.Map(row, new Domain.Classes.Club())).ToList();
        }

        public AgeCategory GetAgeCategoryTournament()
        {
            throw new NotImplementedException();
        }

        public List<Domain.Classes.Team> GetTeamsByMunicipality(int municipalityId, int seasonId)
        {
            throw new NotImplementedException();
        }

        public Tournament GetTournament(int tournamentId)
        {
            var result = _tournamentDataMapper.GetTournament(tournamentId);

            var mapper = new TournamentMapper();

            return mapper.Map(result, new Tournament());
        }

        public int AgeCategory { get; set; }
    }
}