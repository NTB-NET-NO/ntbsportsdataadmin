﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataAdmin.Services.NIF.NIFProdService;

namespace NTB.SportsDataAdmin.Services.NIF.Utilities
{
    class TournamentSortComparer : IComparer<Tournament>
    {
        public int Compare(Tournament x, Tournament y)
        {
            return String.Compare(x.TournamentName, y.TournamentName, StringComparison.OrdinalIgnoreCase);
        }
    }
}
