using System;
using System.Collections.Generic;
using NTB.SportsDataAdmin.Services.NIF.NIFProdService;

namespace NTB.SportsDataAdmin.Services.NIF.Utilities
{
    /// <summary>
    ///     A static class of utility methods that we can easily access from around the application
    /// </summary>
    public static class Utilities
    {
        /// <summary>
        ///     Method to return the list of genders from the NIF 
        /// </summary>
        /// <returns></returns>
        public static List<int> GetGenders()
        {
            List<int> genders = new List<int>
                    {
                        Convert.ToInt32(Gender.All),
                        Convert.ToInt32(Gender.Male), 
                        Convert.ToInt32(Gender.Female),
                        Convert.ToInt32(Gender.Unknown)
                    };

            return genders;
        }
    }
}