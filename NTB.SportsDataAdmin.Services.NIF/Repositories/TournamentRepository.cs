﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using NTB.SportsDataAdmin.Services.NIF.Interfaces;
using NTB.SportsDataAdmin.Services.NIF.NIFProdService;
using NTB.SportsDataAdmin.Services.NIF.Utilities;
using log4net;

namespace NTB.SportsDataAdmin.Services.NIF.Repositories
{
    public class TournamentRepository : IRepository<Tournament>, IDisposable , ITournamentDataMapper
    {
        /// <summary>
        /// The tournament service client.
        /// </summary>
        private readonly TournamentServiceClient _tournamentServiceClient = new TournamentServiceClient();

        /// <summary>
        /// The organization service client.
        /// </summary>
        private readonly OrgServiceClient _orgServiceClient = new OrgServiceClient();

        private readonly TeamServiceClient _teamServiceClient = new TeamServiceClient();
        
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentRepository));

        public TournamentRepository()
        {
            
            if (_tournamentServiceClient.ClientCredentials != null)
            {
                _tournamentServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
                _tournamentServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];
            }

            if (_teamServiceClient.ClientCredentials != null)
            {
                _teamServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
                _teamServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];
            }
            if (_orgServiceClient.ClientCredentials == null) return;
            _orgServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"];
            _orgServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];
        }
        
        public int InsertOne(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Tournament> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Tournament> GetAll()
        {
            throw new NotImplementedException();
        }

        public Tournament Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        //public List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId, int orgId)
        //{
        //    var distinctTournaments = new List<Tournament>();

        //    Logger.Debug("SeasonId: " + seasonId);

        //    Logger.Debug("Federation id: " + orgId);

        //    var request = new SeasonIdRequest
        //    {
        //        SeasonId = seasonId
        //    };
        //    var response = _tournamentServiceClient.GetSeasonTournaments(request);

        //    if (!response.Success)
        //    {
        //        throw new Exception(response.ErrorMessage);
        //    }
        //    var tournaments = response.Tournaments;

        //    // Now get teams participating in the tournament
        //    foreach (var tournament in tournaments)
        //    {
        //        var tournamentIdRequest = new TournamentIdRequest
        //        {
        //            TournamentId = tournament.TournamentId
        //        };

        //        var teamResponse = _tournamentServiceClient.GetTournamentTeams(tournamentIdRequest);

        //        if (!teamResponse.Success)
        //        {
        //            continue;
        //        }

        //        // now that we have the team in this tournament we could check against a few things
        //        foreach (var team in teamResponse.TournamentMatchTeam)
        //        {
                    
        //            var searchRequest = new SearchOrgAdvancedRequest
        //            {
        //                SearchParams = searchParams;
        //            };
        //            var foo = _orgServiceClient.SearchOrgAdvanced(searchRequest);
        //        }
        //    }
            
        //}
            
            
        /// <summary>
        ///     A method to return the tournaments based on municipalities
        /// </summary>
        /// <param name="municipalities">string of ids</param>
        /// <param name="seasonId">season id</param>
        /// <param name="orgId">federation id</param>
        /// <returns>list of tournaments</returns>
        public List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId, int orgId)
        {
            // todo: Consider using first team based on municipalities, then find out in which tournament this team is involved in

            var distinctTournaments = new List<Tournament>();
            
            

            Logger.Debug("SeasonId: " + seasonId);

            Logger.Debug("Federation id: " + orgId);

            foreach (string municipality in municipalities)
            {

                Logger.Debug("Municipality: " + municipality);
                var tournamentRegionsSearchParams = new TournamentRegionsSearchParams
                    {
                        LocalCouncilIds = municipality,
                        SeasonId = seasonId,
                        FederationId = orgId
                    };
                var tournamentRegionsSearchRequest = new TournamentRegionsSearchRequest
                    {
                        SearchParams = tournamentRegionsSearchParams
                    };
                var searchResult =
                    _tournamentServiceClient.SearchTournamentByRegions(tournamentRegionsSearchRequest);

                if (!searchResult.Success)
                {
                    Logger.Error("The query caused an error!");
                    return null;
                }

                IEnumerable<Tournament> tournaments = searchResult.Tournaments;

                var tournamentComparer = new TournamentComparer();
                foreach (Tournament tournament in tournaments.Where(tournament => !distinctTournaments.Contains(tournament, tournamentComparer)))
                {
                    distinctTournaments.Add(tournament);
                }
            }
            
            distinctTournaments.Sort((a, b) =>
                {
                    int result = String.Compare(a.TournamentName, b.TournamentName, StringComparison.Ordinal);

                    return result;
                });

            return distinctTournaments;
        }

        public List<Tournament> GetTournamentByMunicipalities(List<Region> selectedRegions, List<ClassCode> ageCategories, int seasonId, int jobId, int orgId)
        {
            var selectedTournaments = new List<Tournament>();

            
            foreach (Region region in selectedRegions)
            {
                Logger.Debug("RegionName: " + region.RegionName);
                var tournamentRegionsSearchParams = new TournamentRegionsSearchParams
                {
                    LocalCouncilIds = region.RegionId.ToString(CultureInfo.InvariantCulture),
                    SeasonId = seasonId
                };
                var tournamentRegionsSearchRequest = new TournamentRegionsSearchRequest
                {
                    
                    SearchParams = tournamentRegionsSearchParams
                };
                var searchResult = _tournamentServiceClient.SearchTournamentByRegions(tournamentRegionsSearchRequest);

                if (!searchResult.Success) continue;

                foreach (Tournament tournament in searchResult.Tournaments)
                {
                    foreach (ClassCode classCode in ageCategories)
                    {
                        if (classCode.ClassId != tournament.ClassCodeId)
                        {
                            throw new Exception("Age Category not found in database!");
                        }

                        var selectedTournament = new Tournament
                            {
                                TournamentId = tournament.TournamentId,
                                TournamentName = tournament.TournamentName,
                                TournamentNo = tournament.TournamentNo,
                                ClassCodeId = tournament.ClassCodeId
                            };


                        // check if the current tournament is in the selectedtournament list
                        var tournament1 = tournament;
                        var tournaments = from a in selectedTournaments
                                          where
                                              (a.TournamentId == tournament1.TournamentId ||
                                               a.TournamentNo == tournament1.TournamentNo)
                                          select a;

                        if (!tournaments.Any())
                        {
                            selectedTournaments.Add(selectedTournament);
                        }
                    }
                }
            }
            return selectedTournaments;
        }

        /// <summary>
        ///     Method to get this seasons tournaments
        /// </summary>
        /// <param name="seasonId"></param>
        /// <returns></returns>
        public List<Tournament> GetTournamentsBySeasonId(int seasonId)
        {
            var request = new SeasonIdRequest(seasonId);

            var response = _tournamentServiceClient.GetSeasonTournaments(request);

            if (!response.Success)
            {
                throw new Exception("We've received an error from the NIF API: " + response.ErrorMessage + ", code: " + response.ErrorCode);
            }

            return response.Tournaments.ToList();
        }

        public Tournament GetTournamentById(int tournamentId, int seasonId)
        {
            var request = new SeasonIdRequest(seasonId);
            var response = _tournamentServiceClient.GetSeasonTournaments(request);

            if (!response.Success)
            {
                throw new Exception("We've received an error from the NIF API: " + response.ErrorMessage + ", code: " + response.ErrorCode);
            }

            var tournament = (from t in response.Tournaments
                where t.TournamentId == tournamentId
                select t).SingleOrDefault();
            return tournament;
        }
    }
}
