﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsDataAdmin.Services.NIF.Interfaces;
using NTB.SportsDataAdmin.Services.NIF.NIFProdService;

namespace NTB.SportsDataAdmin.Services.NIF.Repositories
{
    public class MatchRepository : IRepository<TournamentMatch>, IDisposable, IMatchDataMapper 
    {
        public int InsertOne(TournamentMatch domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<TournamentMatch> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(TournamentMatch domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(TournamentMatch domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<TournamentMatch> GetAll()
        {
            throw new NotImplementedException();
        }

        public TournamentMatch Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<TournamentMatchExtended> GetMatchesByTournamentIdAndSportId(int tournamentId, int sportId)
        {
            TournamentServiceClient tournamentServiceClient = new TournamentServiceClient();

            if (tournamentServiceClient.ClientCredentials == null)
            {
                return new List<TournamentMatchExtended>();
            }

            tournamentServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServicesUsername"]; 
            tournamentServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicesPassword"];

            // Creating the database fetch request
            var request = new TournamentMatchRequest
                {
                    TournamentId = tournamentId
                };

            var response = tournamentServiceClient.GetTournamentMatches(request);

            return !response.Success ? new List<TournamentMatchExtended>() : response.TournamentMatch.ToList();
        }
    }
}
