﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataAdmin.Services.NIF.NIFProdService;

namespace NTB.SportsDataAdmin.Services.NIF.Interfaces
{
    public interface ISeasonDataMapper
    {
        List<Season> GetFederationSeasonByOrgId(int orgId);
    }
}
