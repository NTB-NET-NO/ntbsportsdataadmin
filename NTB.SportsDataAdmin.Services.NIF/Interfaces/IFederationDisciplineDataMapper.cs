﻿using System.Collections.Generic;
using NTB.SportsDataAdmin.Services.NIF.NIFProdService;

namespace NTB.SportsDataAdmin.Services.NIF.Interfaces
{
    public interface IFederationDisciplineDataMapper
    {
        List<FederationDiscipline> GetFederationDisciplines(int orgId);
        FederationDiscipline GetFederationDiscipline(int id);
    }
}
