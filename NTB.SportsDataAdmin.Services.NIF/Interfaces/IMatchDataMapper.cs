﻿using System.Collections.Generic;
using NTB.SportsDataAdmin.Services.NIF.NIFProdService;

namespace NTB.SportsDataAdmin.Services.NIF.Interfaces
{
    public interface IMatchDataMapper
    {
        List<TournamentMatchExtended> GetMatchesByTournamentIdAndSportId(int tournamentId, int sportId);
    }
}
