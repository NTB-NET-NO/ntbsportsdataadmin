﻿using System.Collections.Generic;
using NTB.SportsDataAdmin.Services.NIF.NIFProdService;

namespace NTB.SportsDataAdmin.Services.NIF.Interfaces
{
    public interface ITournamentDataMapper
    {
        List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId, int orgId);
        List<Tournament> GetTournamentByMunicipalities(List<Region> selectedRegions, List<ClassCode> ageCategories, int seasonId, int jobId, int orgId);
        List<Tournament> GetTournamentsBySeasonId(int seasonId);

        Tournament GetTournamentById(int tournamentId, int seasonId);
    }
}
