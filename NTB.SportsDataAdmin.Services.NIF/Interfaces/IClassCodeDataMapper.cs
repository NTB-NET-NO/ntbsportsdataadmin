﻿using NTB.SportsDataAdmin.Services.NIF.NIFProdService;
using System.Collections.Generic;

namespace NTB.SportsDataAdmin.Services.NIF.Interfaces
{
    public interface IClassCodeDataMapper
    {
        List<ClassCode> GetClassCodes(int orgId);
    }
}
