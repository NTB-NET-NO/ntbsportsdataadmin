﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using NTB.SportsDataAdmin.Services.NFF.Interfaces;
using NTB.SportsDataAdmin.Services.NFF.NFFProdService;

namespace NTB.SportsDataAdmin.Services.NFF.Repositories
{
    public class OrgRepository : IOrgDataMapper
    {
        /// <summary>
        /// The service client.
        /// </summary>
        public OrganizationServiceClient ServiceClient = new OrganizationServiceClient();

        public OrgRepository()
        {
            if (ServiceClient.State == System.ServiceModel.CommunicationState.Faulted)
            {
                ServiceClient.Close();
                ServiceClient = null;
            }
            ServiceClient = new OrganizationServiceClient();
            
            if (ServiceClient.ClientCredentials != null)
            {
                ServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                ServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
            }
              
        }

        public List<Club> GetClubs()
        {
            var districts = ServiceClient.GetDistricts();

            var clubs = new List<Club>();
            foreach (District district in districts)
            {
                var remoteClubs = ServiceClient.GetClubsByDistrict(district.DistrictId, null).ToList();

                clubs.AddRange(remoteClubs);
            }
            return clubs;
        }
    }
}
