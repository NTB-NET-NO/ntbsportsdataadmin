﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsDataAdmin.Services.NFF.Interfaces;
using NTB.SportsDataAdmin.Services.NFF.NFFProdService;

namespace NTB.SportsDataAdmin.Services.NFF.Repositories
{
    public class MatchRepository : IRepository<Match>, IDisposable, IMatchDataMapper
    {
        public TournamentServiceClient ServiceClient = new TournamentServiceClient();

        public MatchRepository()
        {
            if (ServiceClient.State == System.ServiceModel.CommunicationState.Faulted)
            {
                ServiceClient.Close();
                ServiceClient = null;

                ServiceClient = new TournamentServiceClient();
                if (ServiceClient.ClientCredentials != null)
                {
                    ServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                    ServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
                }
            }  
        }

        public int InsertOne(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Match> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Match> GetAll()
        {
            throw new NotImplementedException();
        }

        public Match Get(int id)
        {
            return ServiceClient.GetMatch(id, true, true, true, true);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
        
        public List<Match> GetMatchesByTournament(int tournamentId, bool includeSquad, bool includeReferees, bool includeResults,
                                           bool includeEvents)
        {
            if (ServiceClient.State == System.ServiceModel.CommunicationState.Faulted)
            {
                ServiceClient.Close();
                ServiceClient = null;

                ServiceClient = new TournamentServiceClient();
               
            }

            if (ServiceClient.ClientCredentials != null)
            {
                ServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                ServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
            }

            return ServiceClient.GetMatchesByTournament(tournamentId, includeSquad, includeReferees, includeResults,
                                                        includeEvents, null).ToList();
        }
    }
}
