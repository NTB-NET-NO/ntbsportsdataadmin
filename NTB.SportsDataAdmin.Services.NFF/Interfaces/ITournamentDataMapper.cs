﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataAdmin.Services.NFF.NFFProdService;

namespace NTB.SportsDataAdmin.Services.NFF.Interfaces
{
    public interface ITournamentDataMapper
    {
        List<Tournament> GetTournamentByMunicipalities(List<string> municipalities, int seasonId);
        List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId);
        List<Tournament> GetTournamentsByTeam(int teamId, int seasonId);
        List<Tournament> GetClubsByTournament(int tournamentId, int seasonId);
        List<AgeCategoryTournament> GetAgeCategoriesTournament();
        List<Team> GetTeamsByMunicipality(int municipalityId);
        Tournament GetTournament(int tournamentId);

        int AgeCategoryId { get; set; }
    }
}
