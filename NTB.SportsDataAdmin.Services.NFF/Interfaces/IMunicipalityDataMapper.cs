﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataAdmin.Services.NFF.NFFProdService;

namespace NTB.SportsDataAdmin.Services.NFF.Interfaces
{
    public interface IMunicipalityDataMapper
    {
        List<Municipality> GetMunicipalitiesbyDistrict(int districtId);
    }
}
