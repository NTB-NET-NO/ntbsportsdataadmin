﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataAdmin.Services.NFF.NFFProdService;

namespace NTB.SportsDataAdmin.Services.NFF.Interfaces
{
    public interface IMatchDataMapper
    {
        List<Match> GetMatchesByTournament(int tournamentId, bool includeSquad, bool includeReferees,
                                           bool includeResults, bool includeEvents);
    }
}
