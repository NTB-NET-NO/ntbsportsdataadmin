﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsDataAdmin.Services.NFF.NFFProdService;

namespace NTB.SportsDataAdmin.Services.NFF.Interfaces
{
    public interface IDistrictDataMapper
    {
        List<District> GetDistricts();
        District GetDistrict(int districtId);
    }
}
