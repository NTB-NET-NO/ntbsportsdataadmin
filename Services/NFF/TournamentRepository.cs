using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsDataAdmin.Services.NFFProdService;
using Services.Interfaces;
using NTB.SportsDataAdmin.Services.NFFProdService;

namespace NTB.SportsDataAdmin.Services.NFF
{
    public class TournamentRepository : IRepository<Tournament>, IDisposable 
    {
        public TournamentServiceClient ServiceClient = new TournamentServiceClient();

        public OrganizationServiceClient OrganizationServiceClient = new OrganizationServiceClient();

        public MetaServiceClient MetaServiceClient = new MetaServiceClient();

        public int SeasonId { get; set; }

        public int AgeCategoryId { get; set; }

        public int InsertOne(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(System.Collections.Generic.List<Tournament> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Tournament> GetAll()
        {
            throw new NotImplementedException();
        }

        private void CheckServiceState(object service)
        {
            if (service.GetType() == new TournamentServiceClient().GetType())
            {
                _ResetTournamentServiceClient((TournamentServiceClient)service);
            } else if (service.GetType() == new OrganizationServiceClient().GetType())
            {
                _ResetOrganizationServiceClient((OrganizationServiceClient) service);
            } else if (service.GetType() == new MetaServiceClient().GetType())
            {
                _ResetMetaServiceClient((MetaServiceClient) service);
            }

        }

        private void _ResetTournamentServiceClient(TournamentServiceClient client)
        {
            if (client.State != System.ServiceModel.CommunicationState.Faulted) return;

            client.Close();
            client = null;

            client = new TournamentServiceClient();
            if (client.ClientCredentials == null) return;

            // Setting username and password again
            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];  
        }

        private void _ResetMetaServiceClient(MetaServiceClient client)
        {
            if (client.State != System.ServiceModel.CommunicationState.Faulted) return;

            client.Close();
            client = null;

            client = new MetaServiceClient();
            if (client.ClientCredentials == null) return;

            // Setting username and password again
            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
        }

        private void _ResetOrganizationServiceClient(OrganizationServiceClient client)
        {
            if (client.State != System.ServiceModel.CommunicationState.Faulted) return;

            client.Close();
            client = null;

            client = new OrganizationServiceClient();
            if (client.ClientCredentials == null) return;

            // Setting username and password again
            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
        }



        public Tournament Get(int id)
        {
            CheckServiceState(ServiceClient);

            return ServiceClient.GetTournament(id);
        }

        public List<Tournament> GetTournamentsByDistrict(int districtId)
        {
            CheckServiceState(ServiceClient);

            return new List<Tournament>(ServiceClient.GetTournamentsByDistrict(districtId, SeasonId, null));
        }

        public List<Tournament> GetTournamentsByTeam(int teamId)
        {
            CheckServiceState(ServiceClient);

            return new List<Tournament>(ServiceClient.GetTournamentsByTeam(teamId, SeasonId, null));
        }

        public List<Tournament> GetTournamentByMunicipalities(List<string> municipalities)
        {
            CheckServiceState(OrganizationServiceClient);
            CheckServiceState(ServiceClient);
            CheckServiceState(MetaServiceClient);

            List<Team> teamList = new List<Team>();

            List<Tournament> filteredTournaments = new List<Tournament>();
            foreach (string stringMunicipalityId in municipalities)
            {
                int municipalityId = Convert.ToInt32(stringMunicipalityId);

                // Getting the teams from the municipalities and find out in which tournaments they are participating in...

                // TODO: fix so that it gets the list from the service and then check if this is in the database for this customer.
                // If it is for the customer, we shall mark it as selected.
                if (municipalityId <= 0)
                {
                    continue;
                }

                try
                {
                    // I also need to store the municipalities in the database
                    // Insert into municipality has been moved to the controller, as we do everything there
                    // Consider - we are looping there, and we are looping here. What is best practice?
                    //            could this be a speed issue?

                    // municipalitiesDataAccess.InsertMunicipality(municipalityId, jobId);

                    Team[] teams = OrganizationServiceClient.GetTeamsByMunicipality(municipalityId, null);

                    teamList.AddRange(teams);
                }
                catch (Exception exception)
                {
                    throw new Exception(exception.Message);
                }

                List<Tournament> listTournaments = new List<Tournament>();
                foreach (Team team in teamList)
                {
                    try
                    {
                        Tournament[] tournaments =
                            ServiceClient.GetTournamentsByTeam(team.TeamId, SeasonId, null);

                        listTournaments.AddRange(tournaments);
                    }
                    catch (Exception exception)
                    {
                        throw new Exception(exception.Message);
                    }
                }

                if (!listTournaments.Any())
                {
                    return null;
                }

                

                if (AgeCategoryId == 2)
                {
                    filteredTournaments = (from tournament in listTournaments
                                           where
                                               (tournament.TournamentAgeCategory == 18)
                                               || (tournament.TournamentAgeCategory == 21)
                                           orderby tournament.Division
                                           select tournament).ToList();
                }
                else
                {
                    filteredTournaments = (from tournament in listTournaments
                                           where
                                               !(tournament.TournamentAgeCategory == 18
                                                 || tournament.TournamentAgeCategory == 21)
                                           orderby tournament.Division
                                           select tournament).ToList();
                }
            }
            return filteredTournaments;
        }

        /// <summary>
        ///     Method to return Tournament Standig
        /// </summary>
        /// <param name="tournamentId"></param>
        /// <returns>List of tournament table team</returns>
        public List<TournamentTableTeam> GetTournamentStanding(int tournamentId)
        {
            if (ServiceClient.State == System.ServiceModel.CommunicationState.Faulted)
            {
                ServiceClient.Close();
                ServiceClient = null;

                ServiceClient = new TournamentServiceClient();
                if (ServiceClient.ClientCredentials != null)
                {
                    ServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                    ServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
                }
            }
            
            return new List<TournamentTableTeam>(ServiceClient.GetTournamentStanding(tournamentId, null));
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
