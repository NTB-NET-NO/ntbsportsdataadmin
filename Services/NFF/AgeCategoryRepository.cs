﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Services.Interfaces;
using NTB.SportsDataAdmin.Services.NFFProdService;

namespace NTB.SportsDataAdmin.Services.NFF
{
    class AgeCategoryRepository : IRepository<AgeCategoryTournament>, IDisposable
    {
        /// <summary>
        /// The service client.
        /// </summary>
        public MetaServiceClient ServiceClient = new MetaServiceClient();

        public int InsertOne(AgeCategoryTournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<AgeCategoryTournament> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(AgeCategoryTournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(AgeCategoryTournament domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<AgeCategoryTournament> GetAll()
        {
            throw new NotImplementedException();
        }

        public AgeCategoryTournament Get(int id)
        {
            throw new NotImplementedException();
        }

        public List<AgeCategoryTournament> GetAgeCategoriesTournament()
        {
            if (ServiceClient.State == System.ServiceModel.CommunicationState.Faulted)
            {
                ServiceClient.Close();
                ServiceClient = null;

                ServiceClient = new MetaServiceClient();
                if (ServiceClient.ClientCredentials != null)
                {
                    ServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                    ServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
                }
            }

            return new List<AgeCategoryTournament>(ServiceClient.GetAgeCategoriesTournament());
        }

        public List<AgeCategoryTeam> GetAgeCategoriesTeam()
        {
            if (ServiceClient.State == System.ServiceModel.CommunicationState.Faulted)
            {
                ServiceClient.Close();
                ServiceClient = null;

                ServiceClient = new MetaServiceClient();
                if (ServiceClient.ClientCredentials != null)
                {
                    ServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                    ServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
                }
            }

            return new List<AgeCategoryTeam>(ServiceClient.GetAgeCategoriesTeam());
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
