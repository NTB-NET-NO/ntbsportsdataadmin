﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using NTB.SportsDataAdmin.Services.NFFProdService;
using Services.Interfaces;

namespace Services.NFF
{
    class MatchRepository : IRepository<Match>, IDisposable
    {
        public TournamentServiceClient ServiceClient = new TournamentServiceClient();

        public int InsertOne(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Match> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Match> GetAll()
        {
            throw new NotImplementedException();
        }

        public Match Get(int id)
        {
            if (ServiceClient.State == System.ServiceModel.CommunicationState.Faulted)
            {
                ServiceClient.Close();
                ServiceClient = null;

                ServiceClient = new TournamentServiceClient();
                if (ServiceClient.ClientCredentials != null)
                {
                    ServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServicesUsername"];
                    ServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicesPassword"];
                }
            }

            return ServiceClient.GetMatch(id, true, true, true, true);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        
    }
}
