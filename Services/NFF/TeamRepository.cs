﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Interfaces;
using NTB.SportsDataAdmin.Services.NFFProdService;

namespace NTB.SportsDataAdmin.Services.NFF
{
    class TeamRepository : IRepository<Team>, IDisposable 
    {
        public int InsertOne(Team domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Team> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Team domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Team domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Team> GetAll()
        {
            throw new NotImplementedException();
        }

        public Team Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
